package by.training.parser;

import by.training.model.CompositeText;
import by.training.reader.FileReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.Objects;

@RunWith(JUnit4.class)
public class ParserParagraphTest {

    private ClassLoader classLoader = getClass().getClassLoader();
    private String text;

    private ParserParagraph parserParagraph;

    @Before
    public void loadInfo() {
        ParserSentence parserSentence = new ParserSentence();
        ParserWord parserWord = new ParserWord();
        parserParagraph = new ParserParagraph();

        parserSentence.setNextParser(parserWord);
        parserParagraph.setNextParser(parserSentence);

        File file = new File(Objects.requireNonNull(classLoader.getResource("text.txt")).getFile());
        FileReader fileReader = new FileReader();

        text = fileReader.readFile(file.getAbsolutePath());
    }

    @Test
    public void parse() {
        CompositeText paragraph = parserParagraph.parse(text);

        Assert.assertEquals(paragraph.getTexts().size(), 4);
    }
}