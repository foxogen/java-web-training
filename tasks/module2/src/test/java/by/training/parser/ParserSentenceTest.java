package by.training.parser;

import by.training.model.CompositeText;
import by.training.reader.FileReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.Objects;

@RunWith(JUnit4.class)
public class ParserSentenceTest {

    private ClassLoader classLoader = getClass().getClassLoader();
    private String text;

    private ParserSentence parserSentence;

    @Before
    public void loadInfo() {
        parserSentence = new ParserSentence();
        ParserWord parserWord = new ParserWord();

        parserSentence.setNextParser(parserWord);

        File file = new File(Objects.requireNonNull(classLoader.getResource("text.txt")).getFile());
        FileReader fileReader = new FileReader();

        text = fileReader.readFile(file.getAbsolutePath());
    }

    @Test
    public void parseSentence() {
        CompositeText sentence = parserSentence.parse(text);

        Assert.assertEquals(sentence.getTexts().size(), 6);
    }
}