package by.training.parser;

import by.training.model.CompositeText;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ParserWorldTest {

    private static final String SENTENCE_EIGHTEEN_WORD = "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.";

    @Test
    public void parseWord() {
        ParserWord parserWord = new ParserWord();

        CompositeText wordLeafList = parserWord.parse(SENTENCE_EIGHTEEN_WORD);

        Assert.assertEquals(wordLeafList.getTexts().size(), 17);
    }

}