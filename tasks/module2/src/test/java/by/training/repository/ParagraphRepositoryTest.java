package by.training.repository;

import by.training.controller.TextController;
import by.training.entity.Paragraph;
import by.training.entity.Sentence;
import by.training.entity.Text;
import by.training.entity.Word;
import by.training.parser.ParserParagraph;
import by.training.parser.ParserSentence;
import by.training.parser.ParserWord;
import by.training.reader.FileReader;
import by.training.service.TextService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.Objects;

@RunWith(JUnit4.class)
public class ParagraphRepositoryTest {
    private ClassLoader classLoader = getClass().getClassLoader();

    private Repository<Text> repositoryText = new TextRepository();
    private Repository<Paragraph> repositoryParagraph = new ParagraphRepository();
    private Repository<Sentence> repositorySentence = new SentenceRepository();
    private Repository<Word> repositoryWord = new WordRepository();

    @Before
    public void loadInfo() {
        TextService service = new TextService(repositoryText, repositoryParagraph, repositorySentence, repositoryWord);

        ParserSentence parserSentence = new ParserSentence();
        ParserWord parserWord = new ParserWord();
        ParserParagraph parserParagraph = new ParserParagraph();

        parserSentence.setNextParser(parserWord);
        parserParagraph.setNextParser(parserSentence);

        File file = new File(Objects.requireNonNull(classLoader.getResource("text.txt")).getFile());
        FileReader fileReader = new FileReader();

        TextController controller = new TextController(service, parserParagraph, fileReader);
        controller.saveText(file.getAbsolutePath());
    }

    @Test
    public void paragraphRepo(){
        Paragraph paragraph = repositoryParagraph.getElementByID(1);

        Assert.assertEquals(paragraph.getParentId(), 1);
    }

}