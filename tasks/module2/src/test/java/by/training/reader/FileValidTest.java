package by.training.reader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.Objects;

@RunWith(JUnit4.class)
public class FileValidTest {

    private ClassLoader classLoader = getClass().getClassLoader();

    @Test
    public void checkFile() {
        File file = new File(Objects.requireNonNull(classLoader.getResource("empty.txt")).getFile());
        FileValid fileValid = new FileValid();

        ValidationResult validationResult = fileValid.checkFile(file.getAbsolutePath());

        Assert.assertEquals(validationResult.getErrors().size(), 1);
    }

    @Test
    public void checkFileVal() {
        File file = new File(Objects.requireNonNull(classLoader.getResource("text.txt")).getFile());
        FileValid fileValid = new FileValid();

        ValidationResult validationResult = fileValid.checkFile(file.getAbsolutePath());

        Assert.assertEquals(validationResult.isValid(), true);
    }
}