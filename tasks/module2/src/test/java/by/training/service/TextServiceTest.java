package by.training.service;

import by.training.controller.TextController;
import by.training.entity.Paragraph;
import by.training.entity.Sentence;
import by.training.entity.Text;
import by.training.entity.Word;
import by.training.model.CompositeText;
import by.training.parser.ParserParagraph;
import by.training.parser.ParserSentence;
import by.training.parser.ParserWord;
import by.training.reader.FileReader;
import by.training.repository.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;
import java.util.Objects;

@RunWith(JUnit4.class)
public class TextServiceTest {

    private ClassLoader classLoader = getClass().getClassLoader();
    private TextService service;


    @Before
    public void loadInfo(){
        Repository<Text> repositoryText = new TextRepository();
        Repository<Paragraph > repositoryParagraph = new ParagraphRepository();
        Repository< Sentence > repositorySentence = new SentenceRepository();
        Repository< Word > repositoryWord = new WordRepository();
        service = new TextService(repositoryText, repositoryParagraph, repositorySentence, repositoryWord);

        ParserSentence parserSentence = new ParserSentence();
        ParserWord parserWord = new ParserWord();
        ParserParagraph parserParagraph = new ParserParagraph();

        parserSentence.setNextParser(parserWord);
        parserParagraph.setNextParser(parserSentence);

        File file = new File(Objects.requireNonNull(classLoader.getResource("text.txt")).getFile());
        FileReader fileReader = new FileReader();

        TextController controller = new TextController(service, parserParagraph, fileReader);
        controller.saveText(file.getAbsolutePath());
    }

    @Test
    public void sortParagraph() {
        List<CompositeText> list = service.sortParagraph(1);

        Assert.assertEquals(list.get(3).getTexts().size(), 2);
    }

    @Test
    public void getAllText() {
        CompositeText compositeText = service.getAllText(1);

        Assert.assertEquals(compositeText.getTexts().get(0).getTexts().get(0).getTexts().size(), 17);
    }

    @Test
    public void secondSortParagraph(){
        List<CompositeText> list = service.sortParagraph(1);

        Assert.assertEquals(list.get(0).getTexts().size(), 1);
    }

}