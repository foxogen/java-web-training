package by.training.repository;

import by.training.entity.Word;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class WordRepository implements Repository<Word> {

    private List<Word> listWord;

    public WordRepository() {
        this.listWord = new LinkedList<>();
    }

    @Override
    public void insertRepo(Word word) {
        this.listWord.add(word);
    }

    @Override
    public Word getElementByID(long id) {
        for (Word elementOfText : listWord) {
            if (elementOfText.getId() == id) {
                return elementOfText;
            }
        }
        throw new IllegalArgumentException("Incorrect id!");
    }

    @Override
    public List<Word> getAllElementsByID(long id) {
        return listWord.stream()
                .filter(w -> w.getParentId() == id)
                .collect(Collectors.toList());
    }
}
