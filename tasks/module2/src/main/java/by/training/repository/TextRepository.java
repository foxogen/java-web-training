package by.training.repository;

import by.training.entity.Text;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TextRepository implements Repository<Text> {

    private List<Text> texts;

    public TextRepository() {
        this.texts = new LinkedList<>();
    }

    @Override
    public void insertRepo(Text text) {
        this.texts.add(text);
    }

    @Override
    public Text getElementByID(long id) {
        for (Text elementOfText : texts) {
            if (elementOfText.getId() == id) {
                return elementOfText;
            }
        }
        throw new IllegalArgumentException("Incorrect id!");
    }

    @Override
    public List<Text> getAllElementsByID(long id) {
        return texts.stream()
                .filter(t -> t.getId() == id)
                .collect(Collectors.toList());
    }
}
