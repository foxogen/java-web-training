package by.training.repository;

import java.util.List;

public interface Repository<T> {

    void insertRepo(T t);

    T getElementByID(long id);

    List<T> getAllElementsByID(long id);
}
