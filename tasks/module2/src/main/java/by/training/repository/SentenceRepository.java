package by.training.repository;

import by.training.entity.Sentence;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SentenceRepository implements Repository<Sentence> {

    private List<Sentence> sentences;

    public SentenceRepository() {
        this.sentences = new LinkedList<>();
    }

    @Override
    public void insertRepo(Sentence sentence) {
        this.sentences.add(sentence);
    }

    @Override
    public Sentence getElementByID(long id) {
        for (Sentence elementOfText : sentences) {
            if (elementOfText.getId() == id) {
                return elementOfText;
            }
        }
        throw new IllegalArgumentException("Incorrect id!");
    }

    @Override
    public List<Sentence> getAllElementsByID(long id) {
        return sentences.stream()
                .filter(s -> s.getParentId() == id)
                .collect(Collectors.toList());
    }
}
