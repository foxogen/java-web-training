package by.training.repository;

import by.training.entity.Paragraph;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ParagraphRepository implements Repository<Paragraph> {

    private List<Paragraph> paragraphs;

    public ParagraphRepository() {
        this.paragraphs = new LinkedList<>();
    }

    @Override
    public void insertRepo(Paragraph paragraph) {
        this.paragraphs.add(paragraph);
    }

    @Override
    public Paragraph getElementByID(long id) {
        for (Paragraph elementOfText : paragraphs) {
            if (elementOfText.getId() == id) {
                return elementOfText;
            }
        }
        throw new IllegalArgumentException("Incorrect id!");
    }

    @Override
    public List<Paragraph> getAllElementsByID(long id) {
        return paragraphs.stream()
                .filter(p -> p.getParentId() == id)
                .collect(Collectors.toList());
    }
}
