package by.training.controller;

import by.training.model.CompositeText;
import by.training.parser.ParserChain;
import by.training.reader.FileReader;
import by.training.service.TextService;

public class TextController {

    private TextService service;
    private ParserChain<CompositeText> parserChain;
    private FileReader fileReader;

    public TextController(TextService service, ParserChain<CompositeText> parserChain, FileReader fileReader) {
        this.service = service;
        this.parserChain = parserChain;
        this.fileReader = fileReader;
    }

    public void saveText(String path) {
        String text = fileReader.readFile(path);

        CompositeText parse = parserChain.parse(text);

        service.save(parse);
    }
}
