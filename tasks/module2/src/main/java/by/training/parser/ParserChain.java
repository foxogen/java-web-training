package by.training.parser;

public interface ParserChain<T> {

    T parse(String text);

    ParserChain<T> setNextParser(ParserChain<T> parserChain);
}
