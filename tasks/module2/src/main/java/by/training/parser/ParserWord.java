package by.training.parser;

import by.training.model.CompositeText;
import by.training.model.SentenceComposite;
import by.training.model.WordLeaf;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserWord extends AbstractTextParse {

    private static final Logger LOGGER = Logger.getLogger(ParserWord.class);
    private static final String REGEX_FROM_WORD = "([\\'\\(]+)?(\\w+|[\\-])([\\.\\,\\'\\)\\:]+)?";

    public CompositeText parse(String text) {
        Pattern pattern = Pattern.compile(REGEX_FROM_WORD);
        Matcher matcher = pattern.matcher(text);

        CompositeText sentence = new SentenceComposite();

        while (matcher.find()) {
            WordLeaf wordLeaf = new WordLeaf(matcher.group(2), matcher.group(1), matcher.group(3));
            sentence.addText(wordLeaf);
        }
        LOGGER.info("Create all word leaf for sentence!");
        return sentence;
    }

}
