package by.training.parser;

import by.training.model.CompositeText;

public abstract class AbstractTextParse implements ParserChain<CompositeText> {

    protected AbstractTextParse textParse;

    @Override
    public CompositeText parse(String text) {
        if (this.textParse != null) {
            return textParse.parse(text);
        }
        return null;
    }

    @Override
    public ParserChain<CompositeText> setNextParser(ParserChain<CompositeText> parserChain) {
        this.textParse = (AbstractTextParse) parserChain;
        return textParse;
    }
}
