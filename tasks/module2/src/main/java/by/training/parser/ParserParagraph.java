package by.training.parser;

import by.training.model.CompositeText;
import by.training.model.FinishedTextComposite;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserParagraph extends AbstractTextParse {

    private static final String REGEX_FROM_PARAGRAPH = "\t[^\t]+[\\.\\?\\!]+";
    private static final Logger LOGGER = Logger.getLogger(ParserParagraph.class);

    @Override
    public CompositeText parse(String text) {
        List<String> listString = new LinkedList<>();
        Pattern pattern = Pattern.compile(REGEX_FROM_PARAGRAPH);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            listString.add(matcher.group());
        }

        CompositeText finishedText = new FinishedTextComposite();

        listString.forEach(t -> {
            CompositeText paragraph = textParse.parse(t);
            finishedText.addText(paragraph);

        });
        LOGGER.info("Create all paragraph for text!");
        return finishedText;
    }
}
