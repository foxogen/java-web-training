package by.training.parser;

import by.training.model.CompositeText;
import by.training.model.ParagraphComposite;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserSentence extends AbstractTextParse {

    private static final String REGEX_FROM_SENTENCE = "([A-Z])([^\\.\\?!]+)([\\.\\?!]+)";
    private static final Logger LOGGER = Logger.getLogger(ParserSentence.class);

    @Override
    public CompositeText parse(String text) {
        List<String> listString = new LinkedList<>();
        Pattern pattern = Pattern.compile(REGEX_FROM_SENTENCE);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            listString.add(text.substring(matcher.start(), matcher.end()));
        }

        CompositeText paragraph = new ParagraphComposite();

        listString.forEach(t -> {
            if (setNextParser(textParse) != null) {
                CompositeText sentence = textParse.parse(t);
                paragraph.addText(sentence);
            }
        });
        LOGGER.info("Create all sentences for paragraph!");
        return paragraph;
    }
}
