package by.training.model;

public interface LeafText {
    String getText();
}
