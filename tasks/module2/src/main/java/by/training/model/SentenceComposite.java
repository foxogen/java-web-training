package by.training.model;

import java.util.LinkedList;
import java.util.List;

public class SentenceComposite implements CompositeText {

    private List<CompositeText> listWords;

    public SentenceComposite() {
        listWords = new LinkedList<>();
    }

    @Override
    public void addText(CompositeText leafText) {
        this.listWords.add(leafText);
    }

    @Override
    public List<CompositeText> getTexts() {
        return this.listWords;
    }

    @Override
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        listWords.forEach(word -> stringBuilder.append(word.getText()).append(" "));
        return stringBuilder.toString();
    }
}
