package by.training.model;

import java.util.LinkedList;
import java.util.List;

public class FinishedTextComposite implements CompositeText {

    private List<CompositeText> listParagraph;

    public FinishedTextComposite() {
        listParagraph = new LinkedList<>();
    }

    @Override
    public void addText(CompositeText leafText) {
        this.listParagraph.add(leafText);
    }

    @Override
    public List<CompositeText> getTexts() {
        return this.listParagraph;
    }

    @Override
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        listParagraph.forEach(paragraph -> stringBuilder.append(paragraph.getTexts()).append("\n"));
        return stringBuilder.toString();
    }
}
