package by.training.model;

import java.util.List;

public interface CompositeText extends LeafText {

    void addText(CompositeText leafText);

    List<CompositeText> getTexts();
}
