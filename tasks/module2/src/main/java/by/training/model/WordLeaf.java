package by.training.model;

import java.util.Collections;
import java.util.List;

public class WordLeaf implements CompositeText {

    private String word;
    private String start;
    private String ending;

    public WordLeaf(String word) {
        this.word = word;
    }

    public WordLeaf(String word, String start, String ending) {
        this.word = word;
        this.start = start;
        this.ending = ending;
    }

    @Override
    public void addText(CompositeText leafText) {}

    @Override
    public List<CompositeText> getTexts() {
        return Collections.emptyList();
    }

    @Override
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        if (start != null) {
            stringBuilder.append(start);
        }
        stringBuilder.append(word);
        if (ending != null) {
            stringBuilder.append(ending);
        }
        return stringBuilder.toString();
    }

}
