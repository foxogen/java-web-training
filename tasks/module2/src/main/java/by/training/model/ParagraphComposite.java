package by.training.model;

import java.util.LinkedList;
import java.util.List;

public class ParagraphComposite implements CompositeText {

    private List<CompositeText> listSentences;

    public ParagraphComposite(){
        listSentences = new LinkedList<>();
    }

    @Override
    public void addText(CompositeText leafText) {
        this.listSentences.add(leafText);
    }

    @Override
    public List<CompositeText> getTexts() {
        return this.listSentences;
    }

    @Override
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        listSentences.forEach(sent -> stringBuilder.append(sent.getText()));
        return stringBuilder.toString();
    }
}
