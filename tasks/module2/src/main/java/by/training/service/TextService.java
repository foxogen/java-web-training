package by.training.service;

import by.training.entity.*;
import by.training.model.*;
import by.training.repository.Repository;
import org.apache.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TextService implements Service<CompositeText> {

    private static final Logger LOGGER = Logger.getLogger(TextService.class);
    private Repository<Text> repositoryText;
    private Repository<Paragraph> repositoryParagraph;
    private Repository<Sentence> repositorySentence;
    private Repository<Word> repositoryWord;

    public TextService(Repository<Text> repositoryText, Repository<Paragraph> repositoryParagraph,
                       Repository<Sentence> repositorySentence, Repository<Word> repositoryWord) {
        this.repositoryText = repositoryText;
        this.repositoryParagraph = repositoryParagraph;
        this.repositorySentence = repositorySentence;
        this.repositoryWord = repositoryWord;
    }

    @Override
    public void save(CompositeText comText) {
        Text text = new Text(comText.getText());
        repositoryText.insertRepo(text);

        comText.getTexts().forEach(par -> {
            Paragraph paragraph = new Paragraph(par.getText(), text.getId());
            repositoryParagraph.insertRepo(paragraph);

            for (CompositeText sentText : par.getTexts()) {
                Sentence sentence = new Sentence(sentText.getText(), paragraph.getId());
                repositorySentence.insertRepo(sentence);

                for (CompositeText sentTextT : sentText.getTexts()) {
                    Word word = new Word(sentTextT.getText(), sentence.getId());
                    repositoryWord.insertRepo(word);
                }
            }

        });
        LOGGER.info("Save all text in Repository!");
    }

    @Override
    public CompositeText getAllText(long id) {
        FinishedTextComposite finishedText = new FinishedTextComposite();
        Text baseElementOfText = repositoryText.getElementByID(id);
        List<Paragraph> p = repositoryParagraph.getAllElementsByID(baseElementOfText.getId());

        for (Paragraph paragraph : p) {
            ParagraphComposite paragraphComposite = new ParagraphComposite();
            List<Sentence> s = repositorySentence.getAllElementsByID(paragraph.getId());

            for (Sentence sentence : s) {
                SentenceComposite sentenceComposite = new SentenceComposite();
                List<Word> w = repositoryWord.getAllElementsByID(sentence.getId());

                for (Word word : w) {
                    WordLeaf wordLeaf = new WordLeaf(word.getText());
                    sentenceComposite.addText(wordLeaf);
                }
                paragraphComposite.addText(sentenceComposite);
            }
            finishedText.addText(paragraphComposite);
        }
        LOGGER.info("Load all text in Repository!");
        return finishedText;
    }

    @Override
    public List<CompositeText> sortParagraph(long id) {
        return getAllText(id).getTexts().stream()
                .sorted(Comparator.comparing(p -> p.getTexts().size()))
                .collect(Collectors.toList());
    }
}
