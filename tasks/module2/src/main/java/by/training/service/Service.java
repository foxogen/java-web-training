package by.training.service;

import by.training.model.CompositeText;

import java.util.List;

public interface Service<T> {

    void save(T t);

    CompositeText getAllText(long id);

    List<T> sortParagraph(long id);
}
