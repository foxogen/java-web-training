package by.training.entity;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class Paragraph extends BaseElementOfText {

    private static final AtomicLong ID = new AtomicLong(1);
    private TypeText typeText;
    private long id;
    private long parentId;

    public Paragraph(String text, long parentId) {
        super(text);
        this.parentId = parentId;
        this.typeText = TypeText.PARAGRAPH;
        this.id = ID.getAndIncrement();
    }

    public long getId() {
        return id;
    }

    public long getParentId() {
        return parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Paragraph paragraph = (Paragraph) o;
        return id == paragraph.id &&
                parentId == paragraph.parentId &&
                typeText == paragraph.typeText;
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeText, id, parentId);
    }
}
