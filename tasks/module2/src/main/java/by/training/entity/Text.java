package by.training.entity;

import java.util.Objects;

public class Text extends BaseElementOfText {

    private TypeText typeText;
    private long id;

    public Text(String text) {
        super(text);
        this.typeText = TypeText.TEXT;
        this.id++;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Text text = (Text) o;
        return id == text.id &&
                typeText == text.typeText;
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeText, id);
    }

    @Override
    public String toString() {
        return "Text{" +
                "typeText=" + typeText +
                ", id=" + id +
                '}';
    }
}
