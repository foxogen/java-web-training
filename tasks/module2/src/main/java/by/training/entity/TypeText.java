package by.training.entity;

public enum TypeText {
    WORD,
    SENTENCE,
    PARAGRAPH,
    TEXT
}
