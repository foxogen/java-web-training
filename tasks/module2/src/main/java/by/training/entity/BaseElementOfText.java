package by.training.entity;

public abstract class BaseElementOfText {

    protected String text;

    public BaseElementOfText(String text) {
        this.text = text;
    }
    public String getText() {
        return text;
    }
}
