package by.training.reader;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class FileReader {

    private FileValid fileValid = new FileValid();
    private static final Logger LOGGER = Logger.getLogger(FileReader.class);

    public String readFile(String path) {
        byte[] bytes = null;

        ValidationResult validationResult = fileValid.checkFile(path);
        if (!validationResult.isValid()) {
            LOGGER.error(validationResult.getErrors());
            throw new IllegalArgumentException("This file not valid!");
        }
        try {
            bytes = Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            LOGGER.error("File with path: " + path + " don't read!" + e);
        }

        LOGGER.info("File with path: " + path + " read!");
        return new String(bytes);
    }
}
