package by.training.reader;

import org.apache.log4j.Logger;

import java.io.File;

public class FileValid {

    private static final Logger LOGGER = Logger.getLogger(FileValid.class);

    public ValidationResult checkFile(String path) {
        ValidationResult validationResult = new ValidationResult();
        File file = new File(path);

        if (!file.isFile()) {
            validationResult.addError("Problem with file", FileValid.class + " File not found for this path: " + path);
            return validationResult;
        }

        if (file.length() == 0) {
            validationResult.addError("Problem with file", FileValid.class + " The file on this path is empty: " + path);
            return validationResult;
        }

        LOGGER.info("File with path: " + path + " valid!");
        return new ValidationResult();
    }
}
