package by.training.controller;

import by.training.builder.DataBuilder;
import by.training.builder.DataBuilderShip;
import by.training.entity.Ship;
import by.training.validator.FileValidator;
import by.training.validator.LineDataParser;
import by.training.validator.LineDataReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;
import java.util.Objects;


@RunWith(JUnit4.class)
public class PortControllerTest {

    private PortController controller;
    private String path;

    @Before
    public void loadInfo(){
        ClassLoader classLoader = PortController.class.getClassLoader();
        path = new File(Objects.requireNonNull(classLoader.getResource("shipstest.txt")).getFile()).getAbsolutePath();

        FileValidator fileValidator = new FileValidator();
        LineDataReader lineDataReader = new LineDataReader();
        LineDataParser lineDataParser = new LineDataParser();
        DataBuilder<Ship> shipDataBuilder = new DataBuilderShip();

        controller = new PortController(fileValidator, lineDataReader, lineDataParser, shipDataBuilder);
    }

    @Test
    public void readShips() {
        List<Ship> ships = controller.readShips(path);

        Assert.assertEquals(ships.size(), 16);
    }
}