package by.training;

import by.training.builder.DataBuilder;
import by.training.builder.DataBuilderShip;
import by.training.controller.PortController;
import by.training.entity.Ship;
import by.training.validator.FileValidator;
import by.training.validator.LineDataParser;
import by.training.validator.LineDataReader;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class Main {
    public static void main(String[] args) {
        ClassLoader classLoader = PortController.class.getClassLoader();
        String path = new File(Objects.requireNonNull(classLoader.getResource("ships.txt")).getFile()).getAbsolutePath();

        FileValidator fileValidator = new FileValidator();
        LineDataReader lineDataReader = new LineDataReader();
        LineDataParser lineDataParser = new LineDataParser();
        DataBuilder<Ship> shipDataBuilder = new DataBuilderShip();

        PortController controller = new PortController(fileValidator, lineDataReader, lineDataParser, shipDataBuilder);

        List<Ship> ships = controller.readShips(path);

        ships.forEach(ship -> new Thread(ship).start());
    }
}
