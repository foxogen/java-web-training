package by.training.validator;

import java.util.*;

public class LineDataParser {

    public Map<String, String> lineParse(String line) {
        List<String> list = new ArrayList<>(Arrays.asList(line.split(",")));
        Map<String, String> resultMap = new HashMap<>();

        String[] strSplit;
        for (String s : list) {
            strSplit = s.split(":");

            resultMap.put(strSplit[0], strSplit[1]);
        }

        return resultMap;
    }
}
