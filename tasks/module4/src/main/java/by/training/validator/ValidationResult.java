package by.training.validator;

import java.util.HashMap;
import java.util.Map;

public class ValidationResult {
    private Map<String, String> errors;

    public ValidationResult() {
        this.errors = new HashMap<>();
    }

    public Map<String, String> getErrors() {
        return new HashMap<>(errors);
    }

    public void addError(String type, String message) {
        this.errors.put(type, message);
    }

    public boolean isValid() {
        return this.errors.isEmpty();
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "errors=" + errors +
                '}';
    }
}
