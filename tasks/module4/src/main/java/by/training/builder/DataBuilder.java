package by.training.builder;

import java.util.Map;

public interface DataBuilder<T> {

    T build(Map<String, String> map);
}
