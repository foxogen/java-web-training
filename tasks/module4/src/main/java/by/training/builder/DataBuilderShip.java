package by.training.builder;

import by.training.controller.PortController;
import by.training.entity.Ship;
import by.training.entity.TypeTask;

import java.util.Map;

public class DataBuilderShip implements DataBuilder<Ship> {

    @Override
    public Ship build(Map<String, String> map) {
        return new Ship(Integer.parseInt(map.get("id")),
                map.get("name"),
                Integer.parseInt(map.get("capacity")),
                Integer.parseInt(map.get("numContainers")),
                TypeTask.valueOf(map.get("TypeTask")),
                PortController.portManager);
    }
}
