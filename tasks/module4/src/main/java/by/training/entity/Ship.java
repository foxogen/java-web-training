package by.training.entity;

import by.training.service.PortService;
import org.apache.log4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;

public class Ship implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(Ship.class);
    private long id;
    private String name;
    private int capacity;
    private AtomicInteger numContainers;
    private int numBerth;
    private TypeTask task;
    private PortService service;

    public Ship(long id, String name, int capacity, int numContainers, TypeTask task, PortService service) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
        this.numContainers = new AtomicInteger(numContainers);
        this.task = task;
        this.service = service;
    }

    @Override
    public void run() {
        LOGGER.info("Ship " + this.name + " arrived at the port");
        service.mooredToBerth(this);
        service.executeTask(this);

    }

    public int getNumBerth() {
        return numBerth;
    }

    public void setNumBerth(int numBerth) {
        this.numBerth = numBerth;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public AtomicInteger getNumContainers() {
        return numContainers;
    }

    public void setNumContainers(int numContainers) {
        this.numContainers.set(numContainers);
    }

    public TypeTask getTask() {
        return task;
    }

    public void setTask(TypeTask task) {
        this.task = task;
    }
}
