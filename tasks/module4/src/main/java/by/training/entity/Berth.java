package by.training.entity;

import java.util.concurrent.atomic.AtomicBoolean;

public class Berth {

    private AtomicBoolean busy = new AtomicBoolean();

    public AtomicBoolean getBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy.set(busy);
    }
}
