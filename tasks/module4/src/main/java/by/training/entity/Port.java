package by.training.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Port {

    private static final int WAREHOUSE_CAPACITY = 500;
    private static final int BERTH_CAPACITY = 4;
    private AtomicInteger warehouse;
    private List<Berth> berths;
    private static Port port;
    private static ReentrantLock lock = new ReentrantLock(true);

    private Port() {
        this.warehouse = new AtomicInteger(WAREHOUSE_CAPACITY/2);
        this.berths = new ArrayList<>();
        for (int i = 0; i < BERTH_CAPACITY; i++) {
            berths.add(new Berth());
        }
    }

    public static Port getInstance() {
        if (port == null) {
            lock.lock();
            try {
                if (port == null)
                    port = new Port();
                return port;
            }
            finally {
                lock.unlock();
            }
        }
        return port;
    }

    public static int getBerthCapacity() {
        return BERTH_CAPACITY;
    }

    public AtomicInteger getWarehouse() {
        return warehouse;
    }

    public List<Berth> getBerths() {
        return berths;
    }

    public static int getWarehouseCapacity() {
        return WAREHOUSE_CAPACITY;
    }

    public void setWarehouse(int warehouse) {
        this.warehouse.set(warehouse);
    }
}
