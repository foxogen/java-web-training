package by.training.service;

import by.training.entity.Port;
import by.training.entity.Ship;
import org.apache.log4j.Logger;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class PortService {

    private static final Logger LOGGER = Logger.getLogger(PortService.class);
    private ReentrantLock lock = new ReentrantLock();
    private Semaphore semaphore;
    private Port port;

    public PortService(Semaphore semaphore, Port port) {
        this.semaphore = semaphore;
        this.port = port;
    }

    public void mooredToBerth(Ship ship) {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            LOGGER.error("Acquire failed");
            Thread.currentThread().interrupt();
        }
        lock.lock();
        int numBerth = 0;
        boolean check = false;
        while (!check && numBerth < port.getBerths().size()) {
            if (!port.getBerths().get(numBerth).getBusy().get()) {
                port.getBerths().get(numBerth).setBusy(true);
                ship.setNumBerth(numBerth);
                check = true;
            }
            numBerth++;
        }
        LOGGER.info("Ship " + Thread.currentThread().getName() + " occupied the berth " + numBerth);
        lock.unlock();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.error("Moored to berth ship failed");
        }
    }

    public void executeTask(Ship ship) {
        lock.lock();
        System.out.println(semaphore.getQueueLength());
        switch (ship.getTask()) {
            case UNLOADING:
                if ((ship.getNumContainers().get() + port.getWarehouse().get()) > Port.getWarehouseCapacity()) {
                    LOGGER.info("Ship " + Thread.currentThread().getName() + " waiting pls the warehouse overloaded!");
                    port.getBerths().get(ship.getNumBerth()).setBusy(false);
                    try {
                        lock.unlock();
                        semaphore.release();
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        LOGGER.error("Wait until the warehouse appears in the warehouse. " + Thread.currentThread().getName());
                    }
                    mooredToBerth(ship);

                }
                if ((Port.getWarehouseCapacity() - port.getWarehouse().get()) >= ship.getNumContainers().get()) {
                    port.setWarehouse(port.getWarehouse().get() + ship.getNumContainers().get());
                    ship.setNumContainers(0);
                    LOGGER.info("Ship " + Thread.currentThread().getName() + " unloaded!");
                    port.getBerths().get(ship.getNumBerth()).setBusy(false);
                    if (lock.isHeldByCurrentThread()) {
                        lock.unlock();
                    }
                    semaphore.release();
                }
                break;
            case LOADING:
                if (ship.getCapacity() > port.getWarehouse().get()) {
                    LOGGER.info("Ship " + Thread.currentThread().getName() + " waiting pls the warehouse empty!");
                    port.getBerths().get(ship.getNumBerth()).setBusy(false);
                    try {
                        lock.unlock();
                        semaphore.release();
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                        LOGGER.error("Wait until the warehouse appears in the warehouse. " + Thread.currentThread().getName());
                    }
                    mooredToBerth(ship);
                }
                if ((port.getWarehouse().get() - ship.getCapacity()) >= 0) {
                    ship.setNumContainers(ship.getCapacity());
                    port.setWarehouse(port.getWarehouse().get() - ship.getCapacity());
                    LOGGER.info("Ship " + Thread.currentThread().getName() + " loaded!");
                    port.getBerths().get(ship.getNumBerth()).setBusy(false);
                    if (lock.isHeldByCurrentThread()) {
                        lock.unlock();
                    }
                    semaphore.release();
                }
                break;
        }

    }

}
