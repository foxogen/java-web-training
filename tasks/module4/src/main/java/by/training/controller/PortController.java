package by.training.controller;

import by.training.builder.DataBuilder;
import by.training.entity.Port;
import by.training.entity.Ship;
import by.training.entity.TypeTask;
import by.training.service.PortService;
import by.training.validator.FileValidator;
import by.training.validator.LineDataParser;
import by.training.validator.LineDataReader;
import by.training.validator.ValidationResult;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

public class PortController {

    private FileValidator fileValidator;
    private LineDataReader lineDataReader;
    private LineDataParser lineDataParser;
    private DataBuilder<Ship> shipDataBuilder;

    private static final Logger LOGGER = Logger.getLogger(PortController.class);

    private static final Port PORT = Port.getInstance();
    private static final Semaphore SEMAPHORE = new Semaphore(Port.getBerthCapacity(), true);
    public static final PortService portManager = new PortService(SEMAPHORE, PORT);

    public PortController(FileValidator fileValidator, LineDataReader lineDataReader,
                          LineDataParser lineDataParser, DataBuilder<Ship> shipDataBuilder) {
        this.fileValidator = fileValidator;
        this.lineDataReader = lineDataReader;
        this.lineDataParser = lineDataParser;
        this.shipDataBuilder = shipDataBuilder;
    }

    public List<Ship> readShips(String path) {
        ValidationResult validationResult = fileValidator.checkFile(path);

        if (!validationResult.isValid()) {
            LOGGER.error(validationResult.getErrors());
            throw new IllegalArgumentException("This file not valid!");
        }

        List<String> list = lineDataReader.dataReader(path);

        return list.stream().map(str -> {
            Map<String, String> map = lineDataParser.lineParse(str);
            Ship ship = shipDataBuilder.build(map);
            LOGGER.info("Created new ship " + ship.getName());
            return ship;
        }).collect(Collectors.toList());
    }
}
