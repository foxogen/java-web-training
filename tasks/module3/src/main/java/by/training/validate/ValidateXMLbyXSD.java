package by.training.validate;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

public class ValidateXMLbyXSD {

    private String xsd;

    public ValidateXMLbyXSD(String xsd) {
        this.xsd = xsd;
    }

    public boolean validate(String xml) {
        try {
            SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
                    .newSchema(new StreamSource(this.xsd))
                    .newValidator()
                    .validate(new StreamSource(xml));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

