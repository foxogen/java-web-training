package by.training.command;

import by.training.entity.Papers;
import by.training.parser.DefParser;
import org.apache.log4j.Logger;

import java.util.List;

public class DOMParserCommand implements Command<Papers> {

    private static final Logger LOGGER = Logger.getLogger(DOMParserCommand.class);
    private DefParser domParser;

    public DOMParserCommand(DefParser domParser) {
        this.domParser = domParser;
    }

    @Override
    public List<Papers> build(String path) {
        try {
            return this.domParser.parse(path);
        } catch (Exception e) {
            LOGGER.error("Error with xml read!" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }
}
