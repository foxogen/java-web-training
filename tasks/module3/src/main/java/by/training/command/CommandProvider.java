package by.training.command;

import by.training.parser.TypeParser;

public interface CommandProvider<T> {

    Command<T> getCommand(TypeParser typeParser);

    void createCommand(TypeParser typeParser, Command<T> command);
}
