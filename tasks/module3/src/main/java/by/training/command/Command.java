package by.training.command;

import java.util.List;

public interface Command<T> {
    List<T> build(String path);
}
