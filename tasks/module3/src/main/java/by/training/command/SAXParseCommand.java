package by.training.command;

import by.training.entity.Papers;
import by.training.parser.DefParser;
import org.apache.log4j.Logger;

import java.util.List;

public class SAXParseCommand implements Command<Papers> {

    private static final Logger LOGGER = Logger.getLogger(SAXParseCommand.class);
    private DefParser saxPaperParser;

    public SAXParseCommand(DefParser saxPaperParser) {
        this.saxPaperParser = saxPaperParser;
    }

    @Override
    public List<Papers> build(String path) {
        try {
            return this.saxPaperParser.parse(path);
        } catch (Exception e) {
            LOGGER.error("Error with xml read!" + e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }
}
