package by.training.command;

import by.training.entity.Papers;
import by.training.parser.TypeParser;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class ImplCommandProvider implements CommandProvider<Papers> {

    private static final Logger LOGGER = Logger.getLogger(ImplCommandProvider.class);
    private Map<TypeParser, Command> commandMap;

    public ImplCommandProvider() {
        this.commandMap = new HashMap<>();
    }

    @Override
    public Command<Papers> getCommand(TypeParser typeParser) {
        return this.commandMap.get(typeParser);
    }

    @Override
    public void createCommand(TypeParser typeParser, Command<Papers> command) {
        this.commandMap.put(typeParser, command);
        LOGGER.info("Create new Parser: " + typeParser);
    }
}
