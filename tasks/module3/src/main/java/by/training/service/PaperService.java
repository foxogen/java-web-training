package by.training.service;

import by.training.entity.Papers;
import by.training.repository.Repository;

import java.util.List;
import java.util.Optional;

public class PaperService implements Service<Papers> {

    private Repository<Papers> papersRepository;

    public PaperService(Repository<Papers> papersRepository) {
        this.papersRepository = papersRepository;
    }

    @Override
    public void create(Papers papers) {
        this.papersRepository.create(papers);
    }

    @Override
    public Optional<Papers> read(long id) {
        return this.papersRepository.read(id);
    }

    @Override
    public void delete(Papers papers) {
        this.papersRepository.delete(papers);
    }

    @Override
    public List<Papers> getAll() {
        return this.papersRepository.getAll();
    }
}
