package by.training.service;

import java.util.List;
import java.util.Optional;

public interface Service<T> {

    void create(T t);

    Optional<T> read(long id);

    void delete(T t);

    List<T> getAll();
}
