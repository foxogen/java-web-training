package by.training.controller;

import by.training.command.Command;
import by.training.command.CommandProvider;
import by.training.entity.Papers;
import by.training.parser.TypeParser;
import by.training.service.Service;
import by.training.validate.ValidateXMLbyXSD;
import by.training.validate.FileValid;
import by.training.validate.ValidationResult;
import org.apache.log4j.Logger;

import java.util.List;

public class PaperController {

    private static final Logger LOGGER = Logger.getLogger(ValidateXMLbyXSD.class);
    private Service<Papers> service;
    private CommandProvider<Papers> commandProvider;
    private FileValid fileValid;
    private ValidateXMLbyXSD validateXMLbyXSD;

    public PaperController(Service<Papers> service, CommandProvider<Papers> commandProvider,
                           FileValid fileValid, ValidateXMLbyXSD validateXMLbyXSD) {
        this.service = service;
        this.commandProvider = commandProvider;
        this.fileValid = fileValid;
        this.validateXMLbyXSD = validateXMLbyXSD;
    }

    public void upload(String path, TypeParser typeParser) {
        ValidationResult validationResult = fileValid.checkFile(path);
        if (!validationResult.isValid()) {
            LOGGER.error(validationResult.getErrors());
            throw new IllegalArgumentException("This file not valid!");
        }

        if (!validateXMLbyXSD.validate(path)) {
            LOGGER.error("Xml file failed validation " + path + " !");
            throw new IllegalArgumentException("This file not valid by xsd!");
        }

        Command<Papers> command = commandProvider.getCommand(typeParser);

        List<Papers> papersList = command.build(path);

        papersList.forEach(service::create);

    }
}
