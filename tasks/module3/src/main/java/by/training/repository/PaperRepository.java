package by.training.repository;

import by.training.entity.Papers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PaperRepository implements Repository<Papers> {

    private List<Papers> papersList;

    public PaperRepository() {
        this.papersList = new ArrayList<>();
    }

    @Override
    public void create(Papers papers) {
        this.papersList.add(papers);
    }

    @Override
    public Optional<Papers> read(long id) {
        return this.papersList.stream()
                .filter(papers -> papers.getId() == id)
                .findFirst();
    }

    @Override
    public void delete(Papers papers) {
        this.papersList.remove(papers);
    }

    @Override
    public List<Papers> getAll() {
        return this.papersList;
    }
}
