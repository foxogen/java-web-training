package by.training.parser;

import by.training.entity.Papers;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.List;

public class SAXPaperParser implements DefParser {

    @Override
    public List<Papers> parse(String xml) throws Exception {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        SAXHandler saxHandler = new SAXHandler();

        SAXParser saxParser = saxParserFactory.newSAXParser();
        saxParser.parse(xml, saxHandler);

        return saxHandler.getPapersList();
    }
}
