package by.training.parser;

import by.training.entity.*;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class DOMParser implements DefParser {

    private static final Logger LOGGER = Logger.getLogger(DOMParser.class);

    @Override
    public List<Papers> parse(String xml) throws Exception {
        List<Papers> papersList = new ArrayList<>();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(xml);

        NodeList nodeList = Objects.requireNonNull(document).getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nodePaper = nodeList.item(i);

            if (nodePaper.getNodeType() == Node.ELEMENT_NODE) {
                Optional<TypePaper> type = TypePaper.fromString(nodePaper.getAttributes().getNamedItem("type").getNodeValue());
                if (!type.isPresent()) {
                    LOGGER.info("This type invalid!");
                    throw new RuntimeException();
                }

                long id = Long.parseLong(nodePaper.getAttributes().getNamedItem("id").getNodeValue());
                TypePaper typePaper = TypePaper.valueOf(nodePaper.getAttributes().getNamedItem("type").getNodeValue());

                Papers paper = createSpecPaper(typePaper, id);

                Element element = (Element) nodePaper;

                String titleName = element.getElementsByTagName("titleName").item(0).getTextContent();
                boolean monthly = Boolean.parseBoolean(element.getElementsByTagName("monthly").item(0).getTextContent());

                paper.setTitlePaper(titleName);
                paper.setMonthly(monthly);

                List<String> listChars = new ArrayList<>();

                NodeList childNodes = nodePaper.getChildNodes();
                NodeList nodeCharsList = childNodes.item(5).getChildNodes();

                for (int j = 1; j < nodeCharsList.getLength(); j++) {
                    Node nodeChars = nodeCharsList.item(j);

                    if (nodeChars.getNodeType() == Node.ELEMENT_NODE) {
                        String content = nodeChars.getTextContent();

                        listChars.add(content);
                    }
                }
                paper.setListChars(listChars);
                papersList.add(paper);
            }
        }
        return papersList;
    }
}
