package by.training.parser;

import by.training.entity.Papers;
import by.training.entity.TypePaper;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StAXPaperParser implements DefParser {

    private static final Logger LOGGER = Logger.getLogger(StAXPaperParser.class);

    @Override
    public List<Papers> parse(String xml) throws Exception {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(xml));

        List<Papers> listPapers = new ArrayList<>();
        List<String> listChars = null;
        String content = null;
        Papers paper = null;

        while (reader.hasNext()) {
            int event = reader.next();

            switch (event) {
                case XMLStreamReader.START_ELEMENT:
                    if ("paper".equals(reader.getLocalName())) {
                        Optional<TypePaper> type = TypePaper.fromString(reader.getAttributeValue(1));
                        if (!type.isPresent()) {
                            LOGGER.error("This type invalid!");
                            throw new RuntimeException();
                        }
                        long id = Integer.parseInt(reader.getAttributeValue(0));
                        TypePaper typePaper = TypePaper.valueOf(reader.getAttributeValue(1));

                        paper = createSpecPaper(typePaper, id);
                    }
                    if ("chars".equals(reader.getLocalName())) {
                        listChars = new ArrayList<>();
                        String page = reader.getAttributeValue(0);
                        listChars.add(page);
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    content = reader.getText().trim();
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    switch (reader.getLocalName()) {
                        case "titleName":
                            if (paper != null) {
                                paper.setTitlePaper(content);
                            }
                            break;
                        case "monthly":
                            if (paper != null) {
                                paper.setMonthly(Boolean.parseBoolean(content));
                            }
                            break;
                        case "color":
                        case "glossy":
                            if (listChars != null) {
                                listChars.add(content);
                            }
                            break;
                        case "paper":
                            if (paper != null) {
                                paper.setListChars(listChars);
                            }
                            listPapers.add(paper);
                            break;
                    }
            }
        }
        return listPapers;
    }
}
