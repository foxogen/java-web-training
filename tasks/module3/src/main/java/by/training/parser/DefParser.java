package by.training.parser;

import by.training.entity.*;

import java.util.List;

public interface DefParser {

    List<Papers> parse(String xml) throws Exception;

    default Papers createSpecPaper(TypePaper typePaper, long id){
        switch (typePaper) {
            case NEWSPAPER:
                return new Newspaper(typePaper, id);
            case BOOKLET:
                return new Booklet(typePaper, id);
            case MAGAZINE:
                return new Magazine(typePaper, id);
            default:
                throw new RuntimeException();
        }
    }
}
