package by.training.parser;

import by.training.entity.*;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SAXHandler extends DefaultHandler {

    private static final Logger LOGGER = Logger.getLogger(SAXHandler.class);

    private List<Papers> papersList;
    private List<String> listChars;
    private String text;
    private Papers paper;

    public SAXHandler() {
        this.papersList = new ArrayList<>();
    }

    public List<Papers> getPapersList() {
        return papersList;
    }

    @Override
    public void startElement(String uri, String localName, String tag, Attributes attributes) throws SAXException {
        switch (tag) {
            case "paper":
                Optional<TypePaper> type = TypePaper.fromString(attributes.getValue("type"));
                if (type.isPresent()) {
                    long id = Long.parseLong(attributes.getValue("id"));
                    TypePaper typePaper = TypePaper.valueOf(attributes.getValue("type"));

                    paper = createSpecPaper(typePaper, id);
                }
                break;
            case "chars":
                String page = attributes.getValue("page");
                this.listChars = new ArrayList<>();
                listChars.add(page);
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        text = String.copyValueOf(ch, start, length).trim();
    }

    @Override
    public void endElement(String uri, String localName, String tag) throws SAXException {
        switch (tag) {
            case "titleName":
                paper.setTitlePaper(text);
                break;
            case "monthly":
                paper.setMonthly(Boolean.parseBoolean(text));
                break;
            case "color":
            case "glossy":
                listChars.add(text);
                break;
            case "paper":
                paper.setListChars(listChars);
                papersList.add(paper);
                break;
        }
    }

    private Papers createSpecPaper(TypePaper typePaper, long id) {
        switch (typePaper) {
            case NEWSPAPER:
                LOGGER.info("Create new NEWSPAPER with id: " + id);
                return new Newspaper(typePaper, id);
            case BOOKLET:
                LOGGER.info("Create new BOOKLET with id: " + id);
                return new Booklet(typePaper, id);
            case MAGAZINE:
                LOGGER.info("Create new MAGAZINE with id: " + id);
                return new Magazine(typePaper, id);
            default:
                LOGGER.info("This type invalid!");
                throw new RuntimeException();
        }
    }
}
