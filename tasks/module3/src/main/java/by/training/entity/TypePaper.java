package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum TypePaper {
    NEWSPAPER, MAGAZINE, BOOKLET;

    public static Optional<TypePaper> fromString(String name) {

        return Stream.of(TypePaper.values())
                .filter(typePaper -> typePaper.name().equalsIgnoreCase(name))
                .findFirst();
    }
}
