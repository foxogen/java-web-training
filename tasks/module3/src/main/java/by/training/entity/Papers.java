package by.training.entity;

import java.util.List;
import java.util.Objects;

public  class Papers {

    private long id;
    private TypePaper typePaper;
    private String titlePaper;
    private boolean monthly;
    private List<String> listChars;

    public Papers(TypePaper typePaper, long id) {
        this.typePaper = typePaper;
        this.id = id;
    }

    public Papers(TypePaper typePaper, long id, String titlePaper, boolean monthly, List<String> listChars) {
        this.typePaper = typePaper;
        this.id = id;
        this.titlePaper = titlePaper;
        this.monthly = monthly;
        this.listChars = listChars;
    }

    public TypePaper getTypePaper() {
        return typePaper;
    }

    public void setTypePaper(TypePaper typePaper) {
        this.typePaper = typePaper;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitlePaper() {
        return titlePaper;
    }

    public void setTitlePaper(String titlePaper) {
        this.titlePaper = titlePaper;
    }

    public boolean isMonthly() {
        return monthly;
    }

    public void setMonthly(boolean monthly) {
        this.monthly = monthly;
    }

    public List<String> getListChars() {
        return listChars;
    }

    public void setListChars(List<String> listChars) {
        this.listChars = listChars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Papers papers = (Papers) o;
        return id == papers.id &&
                monthly == papers.monthly &&
                typePaper == papers.typePaper &&
                Objects.equals(titlePaper, papers.titlePaper) &&
                Objects.equals(listChars, papers.listChars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typePaper, titlePaper, monthly, listChars);
    }
}
