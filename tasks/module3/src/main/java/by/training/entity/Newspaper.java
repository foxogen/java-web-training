package by.training.entity;

import java.util.List;

public class Newspaper extends Papers {

    public Newspaper(TypePaper typePaper, long id) {
        super(typePaper, id);
    }

    public Newspaper(TypePaper typePaper, long id, String titlePaper, boolean monthly, List<String> listChars) {
        super(typePaper, id, titlePaper, monthly, listChars);
    }

}
