package by.training.repository;

import by.training.command.*;
import by.training.controller.PaperController;
import by.training.entity.Newspaper;
import by.training.entity.Papers;
import by.training.entity.TypePaper;
import by.training.parser.DOMParser;
import by.training.parser.DefParser;
import by.training.parser.TypeParser;
import by.training.service.PaperService;
import by.training.service.Service;
import by.training.validate.FileValid;
import by.training.validate.ValidateXMLbyXSD;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.*;

@RunWith(JUnit4.class)
public class PaperRepositoryTest {

    private ClassLoader classLoader = getClass().getClassLoader();
    private Repository<Papers> repository;
    private List<String> list = new ArrayList<>(Arrays.asList("No", "No"));
    private Papers papers = new Newspaper(TypePaper.NEWSPAPER, 2, "The Observer", false, list);

    @Before
    public void loadInfo() {
        CommandProvider<Papers> commandProvider = new ImplCommandProvider();

        DefParser dom = new DOMParser();
        Command<Papers> commandDOM = new DOMParserCommand(dom);
        commandProvider.createCommand(TypeParser.DOM, commandDOM);

        DefParser sax = new DOMParser();
        Command<Papers> commandSAX = new SAXParseCommand(sax);
        commandProvider.createCommand(TypeParser.SAX, commandSAX);

        repository = new PaperRepository();
        Service<Papers> service = new PaperService(repository);
        FileValid fileValid = new FileValid();

        File fileXsd = new File(Objects.requireNonNull(classLoader.getResource("shema.xsd")).getFile());
        ValidateXMLbyXSD validateXMLbyXSD = new ValidateXMLbyXSD(fileXsd.getAbsolutePath());
        PaperController controller = new PaperController(service, commandProvider, fileValid, validateXMLbyXSD);

        File fileXml = new File(Objects.requireNonNull(classLoader.getResource("papers.xml")).getFile());

        String path = fileXml.getAbsolutePath();

        controller.upload(path, TypeParser.DOM);
    }

    @Test
    public void read() {
        Optional<Papers> read = repository.read(2);

        Assert.assertEquals(read.get().getTitlePaper(), "The Observer");
    }

    @Test
    public void delete() {
        repository.delete(papers);

        List<Papers> all = repository.getAll();

        Assert.assertEquals(all.size(), 16);
    }

    @Test
    public void getAll() {
        List<Papers> all = repository.getAll();

        Assert.assertEquals(all.size(), 17);
    }
}