package by.training.controller;

import by.training.command.*;
import by.training.entity.Papers;
import by.training.parser.DOMParser;
import by.training.parser.DefParser;
import by.training.parser.StAXPaperParser;
import by.training.parser.TypeParser;
import by.training.repository.PaperRepository;
import by.training.repository.Repository;
import by.training.service.PaperService;
import by.training.service.Service;
import by.training.validate.FileValid;
import by.training.validate.ValidateXMLbyXSD;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;
import java.util.Objects;

@RunWith(JUnit4.class)
public class PaperControllerTest {

    private ClassLoader classLoader = getClass().getClassLoader();
    private PaperController controller;
    private Repository<Papers> repository;
    private String path;

    @Before
    public void loadInfo() {
        CommandProvider<Papers> commandProvider = new ImplCommandProvider();

        DefParser dom = new DOMParser();
        Command<Papers> commandDOM = new DOMParserCommand(dom);
        commandProvider.createCommand(TypeParser.DOM, commandDOM);

        DefParser sax = new DOMParser();
        Command<Papers> commandSAX = new SAXParseCommand(sax);
        commandProvider.createCommand(TypeParser.SAX, commandSAX);

        DefParser stax = new StAXPaperParser();
        Command<Papers> commandStAX = new StAXParserCommand(stax);
        commandProvider.createCommand(TypeParser.StAX, commandStAX);

        repository = new PaperRepository();
        Service<Papers> service = new PaperService(repository);
        FileValid fileValid = new FileValid();

        File fileXsd = new File(Objects.requireNonNull(classLoader.getResource("shema.xsd")).getFile());
        ValidateXMLbyXSD validateXMLbyXSD = new ValidateXMLbyXSD(fileXsd.getAbsolutePath());
        controller = new PaperController(service, commandProvider, fileValid, validateXMLbyXSD);

        File fileXml = new File(Objects.requireNonNull(classLoader.getResource("papers.xml")).getFile());

        path = fileXml.getAbsolutePath();
    }

    @Test
    public void uploadDOM() {
        controller.upload(path, TypeParser.DOM);

        List<Papers> list = repository.getAll();

        Assert.assertEquals(list.size(), 17);
    }

    @Test
    public void uploadSAX(){
        controller.upload(path, TypeParser.SAX);

        List<Papers> list = repository.getAll();

        Assert.assertEquals(list.size(), 17);
    }

    @Test
    public void uploadStAX(){
        controller.upload(path, TypeParser.StAX);

        List<Papers> list = repository.getAll();

        Assert.assertEquals(list.size(), 17);
    }

    @Test(expected = IllegalArgumentException.class)
    public void uploadError(){
        File file = new File(Objects.requireNonNull(classLoader.getResource("invalidPapers.xml")).getFile());

        String path = file.getAbsolutePath();

        controller.upload(path, TypeParser.DOM);
    }
}