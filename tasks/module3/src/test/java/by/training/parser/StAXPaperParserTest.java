package by.training.parser;

import by.training.entity.Papers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;
import java.util.Objects;

@RunWith(JUnit4.class)
public class StAXPaperParserTest {

    private ClassLoader classLoader = getClass().getClassLoader();
    private String path;

    @Before
    public void loadInfo() {
        File file = new File(Objects.requireNonNull(classLoader.getResource("papers.xml")).getFile());

        path = file.getAbsolutePath();
    }

    @Test
    public void parse() throws Exception {
        DefParser defParser = new StAXPaperParser();

        List<Papers> parse = defParser.parse(path);

        Assert.assertEquals(parse.size(), 17);
    }
}