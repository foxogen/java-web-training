package by.training.parser;

import by.training.entity.Papers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;
import java.util.Objects;

@RunWith(JUnit4.class)
public class SAXPaperParserTest {

    private ClassLoader classLoader = getClass().getClassLoader();
    private String path;
    @Before
    public void loadInfo(){
        File file = new File(Objects.requireNonNull(classLoader.getResource("papers.xml")).getFile());

        path = file.getAbsolutePath();
    }

    @Test
    public void parse() throws Exception{

        DefParser dom = new SAXPaperParser();

        List<Papers> listPapers = dom.parse(path);

        Assert.assertEquals(listPapers.size(), 17);
    }
}