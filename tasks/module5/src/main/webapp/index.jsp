<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 21.08.2019
  Time: 17:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <title>Home Page</title>
</head>
<body>

<jsp:include page="/jsp/patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>

<h1 style="text-align: center">Main Page</h1>
<jsp:include page="/jsp/patterns/_footer.jsp"/>

</body>
</html>
