<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 15.12.2019
  Time: 14:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>Orders list</title>
</head>
<body>
<jsp:include page="/jsp/patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>

<h3 class="title has-text-black" style="text-align: center"><fmt:message key="page.user_orders"/></h3>
<hr class="login-hr">
<c:forEach items="${requestScope.allOrders}" var="order">
    <div class="column is-5 is-offset-4">
        <div class="box" style="align-items: center;">
            <ul>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_export_address"/>
                            ${order.exportAddress}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_import_address"/>
                            ${order.importAddress}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_city"/>
                            ${order.city}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_import_date"/>
                            ${order.dataOrder}
                    </label>
                </li>
                <li>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_cargo_weight"/>
                            ${order.cargoWeight} <fmt:message
                                key="page.courier_service_transport_kilo"/>
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_price"/>
                            ${order.price}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_phone"/>
                            ${order.mobilePhone}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.user_order_status"/>
                            ${order.status}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.user_create_order_more_info"/>
                            ${order.moreInfo}
                    </label>
                </li>
            </ul>
            <br>
            <form action="${pageContext.request.contextPath}/ordersList/editOrder" method="get"
                  style="display: inline-block">
                <input type="hidden" name="orderId" value="${order.id}">
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                key="page.button_edit_profile"/></button>
                    </div>
                </div>
            </form>
            <form action="${pageContext.request.contextPath}/ordersList/deleteOrder" method="post"
                  style="display: inline-block">
                <input type="hidden" name="orderId" value="${order.id}">
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                key="page.admin_delete"/></button>
                    </div>
                </div>
            </form>
            <c:if test="${order.status == 'APPROVED'}">
                <form action="${pageContext.request.contextPath}/possibleCouriersList?page=1&city=${order.city}"
                      method="post"
                      style="display: inline-block">
                    <div class="field is-grouped">
                        <div class="control">
                            <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                    key="page.user_possible_couriers"/></button>
                        </div>
                    </div>
                </form>
            </c:if>
        </div>
    </div>
</c:forEach>
<form action="${pageContext.request.contextPath}/ordersList" method="post">
    <nav class="pagination is-centered" role="navigation" aria-label="pagination">
        <ul class="pagination-list">
            <c:forEach begin="1" end="${lastPage}" var="i">
                <li><a class="pagination-link"
                       href="${pageContext.request.contextPath}/ordersList?page=${i}">${i}</a>
                </li>
            </c:forEach>
        </ul>
    </nav>
</form>


<jsp:include page="/jsp/patterns/_footer.jsp"/>
</body>
</html>
