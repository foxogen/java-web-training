<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 13.12.2019
  Time: 18:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${requestScope.get('lang')}"/>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>All services</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<h3 class="title has-text-black" style="text-align: center"><fmt:message key="page.nan_menu_view_all_courier_services"/></h3>
<hr class="login-hr">
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th><fmt:message key="page.courier_service_working_time"/></th>
            <th><fmt:message key="page.courier_service_info_end_work_time"/></th>
            <th><fmt:message key="page.courier_service_working_phone"/></th>
            <th><fmt:message key="page.courier_service_transport"/></th>
            <th><fmt:message key="page.courier_service_transport_capacity"/></th>
            <th><fmt:message key="page.courier_service_info_status"/></th>
            <th><fmt:message key="page.admin_check_service"/></th>

        </tr>
        </thead>
        <c:forEach items="${requestScope.allService}" var="service">
            <tbody>
            <tr>
                <th>${service.id}</th>
                <td>${service.startTime}</td>
                <td>${service.endTime}</td>
                <td>${service.mobilePhone}</td>
                <td>${service.transport.transportName}</td>
                <td>${service.transport.transportCapacity}</td>
                <td><c:choose>
                    <c:when test="${service.serviceStatus == 'SUBMITTED'}">
                        <label class="label">
                            <fmt:message key="page.courier_service_status_submitted"/>
                        </label>
                    </c:when>
                    <c:when test="${service.serviceStatus == 'APPROVED'}">
                        <label class="label">
                            <fmt:message key="page.courier_service_status_approved"/>
                        </label>
                    </c:when>
                    <c:when test="${service.serviceStatus == 'DECLINED'}">
                        <label class="label">
                            <fmt:message key="page.courier_service_status_"/>
                        </label>
                    </c:when>
                </c:choose>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/viewAllCourierService/checkService" method="post">
                        <input type="hidden" name="serviceId" value="${service.id}">
                        <input type="hidden" name="page" value="${page}">
                        <input class="button is-small" type="submit" value="<fmt:message key="page.admin_check_service"/>"/>
                    </form>
                </td>
            </tbody>
        </c:forEach>
    </table>
    <form action="${pageContext.request.contextPath}/viewAllCourierService" method="post">
        <nav class="pagination is-centered" role="navigation" aria-label="pagination">
            <ul class="pagination-list">
                <c:forEach begin="1" end="${lastPage}" var="i">
                    <li><a class="pagination-link"
                           href="${pageContext.request.contextPath}/viewAllCourierService?page=${i}">${i}</a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </form>
</div>
<jsp:include page="patterns/_footer.jsp"/>
</body>
</html>
