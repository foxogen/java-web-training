<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 18.11.2019
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="by.training.ApplicationConstant" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${requestScope.get('lang')}"/>
<fmt:setBundle basename="i18n/courier" scope="application"/>

<html>
<head>
    <title>User info</title>
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>

<section class="hero">
    <div class="hero-body">
        <div class="container">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-black"><fmt:message key="page.nav_menu_profile"/></h3>
                <hr class="login-hr">
                <div class="box">
                    <ul>
                        <li>
                            <label class="label"><fmt:message key="page.first_name"/>
                                ${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).firstName}
                            </label>
                        </li>
                        <li>
                            <label class="label"><fmt:message key="page.last_name"/>
                                ${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).lastName}
                            </label>
                        </li>
                        <li>
                            <label class="label"><fmt:message key="page.email"/>
                                ${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).email}
                            </label>
                        </li>
                        <li>
                        <li>
                            <label class="label"><fmt:message key="page.phone"/>
                                ${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).mobilePhone}
                            </label>
                        </li>
                        <li>
                            <label class="label"><fmt:message key="page.city"/>
                                ${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).city}
                            </label>

                        </li>
                        <li>
                            <label class="label"><fmt:message key="page.address"/>
                                ${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).address}
                            </label>

                        </li>
                        <li>
                            <label class="label"><fmt:message key="page.user_role"/>
                                ${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).userRole}
                            </label>

                        </li>
                    </ul>
                    <br>
                    <form action="${pageContext.request.contextPath}/userInfo/edit">
                        <div class="field is-grouped">
                            <div class="control">
                                <button class="button is-block is-info is-large is-fullwidth"><fmt:message key="page.button_edit_profile"/></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</section>


<jsp:include page="patterns/_footer.jsp"/>
</body>
</html>
