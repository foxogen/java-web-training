<jsp:useBean id="courierService" scope="request" type="by.training.courier.CourierDto"/>
<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 13.12.2019
  Time: 18:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>Check service</title>
</head>
<body>
<jsp:include page="/jsp/patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<section class="hero">
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="column is-4 is-offset-4">
                    <div class="box">
                        <ul>
                            <li>
                                <label class="label"><fmt:message key="page.courier_service_working_time"/>
                                    <input type="hidden" name="serviceId" value="${serviceId}">
                                    ${courierService.startTime}
                                </label>
                            </li>
                            <li>
                                <label class="label"><fmt:message key="page.courier_service_info_end_work_time"/>
                                    ${courierService.endTime}
                                </label>
                            </li>
                            <li>
                                <label class="label"><fmt:message key="page.courier_service_transport"/>
                                    ${courierService.transport.transportName}
                                </label>
                            </li>
                            <li>
                                <label class="label"><fmt:message key="page.courier_service_transport_capacity"/>
                                    ${courierService.transport.transportCapacity}
                                </label>
                            </li>
                            <li>
                                <label class="label"><fmt:message key="page.courier_service_working_phone"/>
                                    ${courierService.mobilePhone}
                                </label>

                            </li>
                            <li>
                                <label class="label"><fmt:message key="page.courier_service_info_status"/>
                                    <c:choose>
                                        <c:when test="${courierService.serviceStatus == 'SUBMITTED'}">
                                            <label class="label">
                                                <fmt:message key="page.courier_service_status_submitted"/>
                                            </label>
                                        </c:when>
                                        <c:when test="${courierService.serviceStatus == 'APPROVED'}">
                                            <label class="label">
                                                <fmt:message key="page.courier_service_status_approved"/>
                                            </label>
                                        </c:when>
                                        <c:when test="${courierService.serviceStatus == 'DECLINED'}">
                                            <label class="label">
                                                <fmt:message key="page.courier_service_status_"/>
                                            </label>
                                        </c:when>
                                    </c:choose>
                                </label>

                            </li>
                            <li>
                                <label class="label"><fmt:message key="page.courier_service_about_myself"/>
                                    ${courierService.aboutMyself}
                                </label>

                            </li>
                        </ul>
                        <br>
                        <form action="${pageContext.request.contextPath}/viewAllCourierService/checkService/delete"
                              method="post" style="display: inline-block">
                            <div class="field is-grouped">
                                <div class="control">
                                    <input type="hidden" name="courierServiceId" value="${courierService.id}">
                                    <input type="hidden" name="courierServiceTransportId"
                                           value="${courierService.transport.id}">
                                    <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                            key="page.admin_delete"/></button>
                                </div>
                            </div>
                        </form>
                        <form action="${pageContext.request.contextPath}/viewAllCourierService/checkService/editStatus?status=approved"
                              method="post" style="display: inline-block">
                            <div class="field is-grouped">
                                <div class="control">
                                    <input type="hidden" name="courierServiceId" value="${courierService.id}">
                                    <input type="hidden" name="page" value="${page}">
                                    <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                            key="page.courier_service_status_approved"/></button>
                                </div>
                            </div>
                        </form>
                        <form action="${pageContext.request.contextPath}/viewAllCourierService/checkService/editStatus?status=DECLINED"
                              method="post" style="display: inline-block">
                            <div class="field is-grouped">
                                <div class="control">
                                    <input type="hidden" name="courierServiceId" value="${courierService.id}">
                                    <input type="hidden" name="page" value="${page}">
                                    <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                            key="page.courier_service_status_"/></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <jsp:include page="/jsp/patterns/_footer.jsp"/>
</body>
</html>
