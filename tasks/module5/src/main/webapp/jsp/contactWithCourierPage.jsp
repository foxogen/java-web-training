<jsp:useBean id="courier" scope="request" type="by.training.user.UserDto"/>
<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 16.12.2019
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>Contact</title>
</head>
<body>
<jsp:include page="/jsp/patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<div class="column is-5 is-offset-4">
    <h3 class="title has-text-black"><fmt:message key="page.user_possible_couriers_contact"/></h3>
    <hr class="login-hr">
    <div class="box">
        <article class="media">
            <div class="media-content">
                <div class="content">
                    <form action="${pageContext.request.contextPath}/possibleCouriersList/contact" method="post"
                          style="display: inline-block">
                        <p>
                            <strong>${courier.firstName} ${courier.lastName}</strong>
                            <br>
                            <fmt:message key="page.courier_service_working_phone"/> ${courier.mobilePhone}
                        </p>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_contac_write_message"/> </label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <textarea class="textarea" name="message"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="courierServiceId" value="${courier.id}">
                        <input type="hidden" name="city" value="${city}">
                        <input type="hidden" name="page" value="${page}">
                        <div class="field is-grouped">
                            <div class="control">
                                <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                        key="page.user_button_submite"/></button>
                            </div>
                        </div>
                    </form>
                    <form action="${pageContext.request.contextPath}/possibleCouriersList" method="get"
                          style="display: inline-block">
                        <input type="hidden" name="courierServiceId" value="${courier.id}">
                        <input type="hidden" name="city" value="${city}">
                        <input type="hidden" name="page" value="${page}">

                        <div class="field is-grouped">
                            <div class="control">
                                <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                        key="page.user_button_back"/></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </article>
    </div>
</div>
<jsp:include page="/jsp/patterns/_footer.jsp"/>
</body>
</html>
