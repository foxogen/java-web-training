<%--@elvariable id="userOrder" type="by.training.order.OrderDto"--%>
<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 15.12.2019
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="by.training.ApplicationConstant" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>Edit order</title>
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<section class="hero">
    <div class="hero-body is-medium">
        <div class="container has-text-centered">
            <div class="column is-offset-4" style="margin: auto">

                <h3 class="title has-text-black"><fmt:message key="page.button_edit_profile"/></h3>
                <hr class="login-hr">
                <form action="${pageContext.request.contextPath}/ordersList/editOrder" method="post">
                    <div class="box">
                        <div class="field">
                            <c:if test="${checkData == true}">
                                <c:forEach items="${requestScope.createOrderError}" var="error">
                                    <label class="label" style="color: red"><fmt:message key="${error}"/></label>
                                </c:forEach>
                            </c:if>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_create_order_export_address"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded ">
                                        <input class="input" type="text" name="exportAddress"
                                               value="${userOrder.exportAddress}">
                                    </p>
                                </div>
                                <div class="field-label is-normal">
                                    <label class="label"><fmt:message
                                            key="page.user_create_order_import_address"/></label>
                                </div>
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input" type="text" name="importAddress"
                                               value="${userOrder.importAddress}">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_create_order_city"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="text" name="city" value="${userOrder.city}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_create_order_import_date"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="date" name="orderDate" required="required"
                                               value="${userOrder.dataOrder}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_create_order_phone"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="text" name="contactPhone"
                                               value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).mobilePhone}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_create_order_cargo_weight"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="text" name="cargoWeight"
                                               value="${userOrder.cargoWeight}"
                                               placeholder="<fmt:message key="page.courier_service_transport_kilo"/>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_create_order_price"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="text" name="price" value="${userOrder.price}"
                                               placeholder="<fmt:message key="page.user_create_order_type_money"/>">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.user_create_order_more_info"/> </label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <textarea class="textarea" name="moreInfo">${userOrder.moreInfo}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <button class="button is-primary">
                                            <input type="hidden" name="orderId" value="${userOrder.id}">
                                            <fmt:message key="page.button_edit_profile"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<jsp:include page="patterns/_footer.jsp"/>

</body>
</html>
