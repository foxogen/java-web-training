<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 21.08.2019
  Time: 17:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<body class="main">
<div class="section">

</div>
<footer class="footer has-background-primary">
    <div class="content has-text-centered">
        <p>
            @training.by
        </p>
    </div>
</footer>
</body>

<style type="text/css">
    .main {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    .section {
        flex: 1;
    }
</style>
