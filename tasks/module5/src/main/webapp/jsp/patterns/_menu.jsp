<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 30.11.2019
  Time: 17:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
</head>
<body>
<div class="has-background-primary">
    <div class="tabs is-boxed is-centered main-menu" id="nav">
        <ul>
            <li data-target="pane-1" id="1">
                <a href="${pageContext.request.contextPath}/serviceInfo">
                    <span class="is-small"></span>
                    <span><fmt:message key="page.service_nav"/></span>
                </a>
            </li>
            <li data-target="pane-2" id="2">
                <a href="${pageContext.request.contextPath}/consumerInfo">
                    <span class="is-small"><i class="fab fa-empire"></i></span>
                    <span><fmt:message key="page.the_consumer"/></span>
                </a>
            </li>
            <li data-target="pane-3" id="3">
                <a href="${pageContext.request.contextPath}/courierInfo">
                    <span class="is-small"><i class="fab fa-superpowers"></i></span>
                    <span><fmt:message key="page.the_courier"/></span>
                </a>
            </li>
        </ul>
    </div>
</div>
</body>
</html>
