<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 16.11.2019
  Time: 18:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<jsp:include page="/jsp/patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>

<section class="hero">
    <div class="hero-body">
        <div class="container">
            <form method="post" action="${pageContext.request.contextPath}/registerUser">
                <div class="column is-4 is-offset-4">
                    <h3 class="title has-text-black"><fmt:message key="page.button_registration"/></h3>
                    <hr class="login-hr">
                    <div class="box">
                        <div class="field">
                            <c:if test="${checkLogin == true}">
                                <c:forEach items="${requestScope.registrationError}" var="error">
                                <label class="label" style="color: red"><fmt:message key="${error}"/></label>
                                </c:forEach>
                            </c:if>
                            <label class="label"><fmt:message key="page.login"/> </label>
                            <div class="control">
                                <input class="input" type="text" name="login" placeholder="<fmt:message key="page.text_input"/>" required="required">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label"><fmt:message key="page.password"/></label>
                            <div class="control">
                                <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>" name="password1"
                                       required="required">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label"><fmt:message key="page.first_name"/></label>
                            <div class="control">
                                <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>" name="firstName"
                                       value="${userHas.firstName}" required="required">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label"><fmt:message key="page.last_name"/></label>
                            <div class="control">
                                <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>" name="lastName"
                                       value="${userHas.lastName}" required="required">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label"><fmt:message key="page.email"/></label>
                            <div class="control">
                                <input class="input" type="email" placeholder="<fmt:message key="page.text_input"/>" name="email"
                                       value="${userHas.email}" required="required">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label"><fmt:message key="page.phone"/></label>
                            <div class="control">
                                <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>" name="phone"
                                       value="${userHas.mobilePhone}" required="required">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label"><fmt:message key="page.city"/></label>
                            <div class="control">
                                <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>" name="city"
                                       value="${userHas.city}" required="required">
                            </div>
                        </div>

                        <div class="field">
                            <label class="label"><fmt:message key="page.address"/></label>
                            <div class="control">
                                <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>" name="address"
                                       value="${userHas.address}">
                            </div>
                        </div>


                        <div class="field">
                            <div class="control">
                                <label class="radio">
                                    <input type="radio" name="question" value="COURIER" required="required">
                                    <fmt:message key="page.checkbox_courier"/>
                                </label>
                                <label class="radio">
                                    <input type="radio" name="question" value="USER" required="required">
                                    <fmt:message key="page.checkbox_user"/>
                                </label>
                            </div>
                        </div>

                        <div class="field is-grouped">
                            <div class="control">
                                <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                        key="page.button_submit"/>
                                    <i class="fa fa-sign-in"
                                       aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<jsp:include page="/jsp/patterns/_footer.jsp"/>
</body>
</html>
