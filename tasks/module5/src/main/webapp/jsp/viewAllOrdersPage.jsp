<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 15.12.2019
  Time: 17:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>All orders</title>
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<h3 class="title has-text-black" style="text-align: center"><fmt:message key="page.admin_view_all_orders"/></h3>
<hr class="login-hr">
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>UID</th>
            <th><fmt:message key="page.user_create_order_import_date"/></th>
            <th><fmt:message key="page.user_create_order_city"/></th>
            <th><fmt:message key="page.user_create_order_export_address"/></th>
            <th><fmt:message key="page.user_create_order_import_address"/></th>
            <th><fmt:message key="page.user_create_order_price"/></th>
            <th><fmt:message key="page.user_create_order_cargo_weight"/></th>
            <th><fmt:message key="page.user_create_order_phone"/></th>
            <th><fmt:message key="page.user_order_status"/></th>
            <th><fmt:message key="page.admin_delete"/></th>
            <th><fmt:message key="page.admin_edit_order_status"/></th>

        </tr>
        </thead>
        <c:forEach items="${requestScope.allOrders}" var="order">
            <tbody>
            <tr>
                <th>${order.id}</th>
                <td>${order.userAccountId}</td>
                <td>${order.dataOrder}</td>
                <th>${order.city}</th>
                <td>${order.exportAddress}</td>
                <td>${order.importAddress}</td>
                <td>${order.price}</td>
                <td>${order.cargoWeight}</td>
                <td>${order.mobilePhone}</td>
                <td>${order.status}</td>
                <td>
                    <form action="${pageContext.request.contextPath}/viewAllOrderUser/delete" method="post">
                        <input type="hidden" name="orderId" value="${order.id}">
                        <input type="hidden" name="page" value="${page}">
                        <input class="button is-small" type="submit" value="<fmt:message key="page.admin_delete"/>"/>
                    </form>
                </td>
                <th>
                    <div class="control">
                        <div class="select">
                            <select class="is-small"
                                    onchange="window.location.href=this.options[this.selectedIndex].value">
                                <option selected
                                        value="${pageContext.request.contextPath}/viewAllOrderUser/editStatus?status=SUBMITTED&orderId=${order.id}">
                                    <fmt:message key="page.courier_service_status_submitted"/></option>
                                <option value="${pageContext.request.contextPath}/viewAllOrderUser/editStatus?status=DECLINED&orderId=${order.id}"><fmt:message
                                        key="page.courier_service_status_"/></option>
                                <option value="${pageContext.request.contextPath}/viewAllOrderUser/editStatus?status=APPROVED&orderId=${order.id}"><fmt:message
                                        key="page.courier_service_status_approved"/></option>
                            </select>
                        </div>
                    </div>
                </th>
            </tr>
            </tbody>
        </c:forEach>
    </table>
    <form action="${pageContext.request.contextPath}/viewAllOrderUser" method="post">
        <nav class="pagination is-centered" role="navigation" aria-label="pagination">
            <ul class="pagination-list">
                <c:forEach begin="1" end="${lastPage}" var="i">
                    <li><a class="pagination-link"
                           href="${pageContext.request.contextPath}/viewAllOrderUser?page=${i}">${i}</a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </form>
</div>
<jsp:include page="patterns/_footer.jsp"/>
</body>
</html>
