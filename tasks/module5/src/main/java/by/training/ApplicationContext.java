package by.training;

import by.training.admin.*;
import by.training.command.ConsumerInfoCommand;
import by.training.command.CourierInfoCommand;
import by.training.command.ServiceInfoCommand;
import by.training.core.BeanRegistry;
import by.training.core.BeanRegistryImpl;
import by.training.courier.*;
import by.training.dao.*;
import by.training.order.*;
import by.training.user.*;
import by.training.validate.CourierServiceValidator;
import by.training.validate.OrderValidator;
import by.training.validate.RegisterUserValidator;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ApplicationContext implements BeanRegistry {

    private final static AtomicBoolean INITIALIZED = new AtomicBoolean(false);
    private final static Lock INITIALIZE_LOCK = new ReentrantLock();
    private static ApplicationContext INSTANCE;

    private BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static void initialize() {
        INITIALIZE_LOCK.lock();
        try {
            if (INSTANCE != null && INITIALIZED.get()) {
                throw new IllegalStateException("Context was already initialized");
            } else {
                ApplicationContext context = new ApplicationContext();
                context.init();
                INSTANCE = context;
                INITIALIZED.set(true);
            }

        } finally {
            INITIALIZE_LOCK.unlock();
        }
    }

    public static ApplicationContext getInstance() {

        if (INSTANCE != null && INITIALIZED.get()) {
            return INSTANCE;
        } else {
            throw new IllegalStateException("Context wasn't initialized");
        }
    }

    private void init() {
        registerDataSource();
        registerClasses();
    }

    @Override
    public void destroy() {
        ApplicationContext context = getInstance();
        DataSource dataSource = context.getBean(DataSource.class);
        dataSource.close();
        beanRegistry.destroy();
    }

    private void registerDataSource() {
        DataSource dataSource = new DataSourceImpl();
        TransactionManager transactionManager = new TransactionManagerImpl(dataSource);
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager, dataSource);
        registerBean(dataSource);
        registerBean(transactionManager);
        registerBean(connectionManager);

    }

    private void registerClasses() {
        registerBean(CourierServicesListCommand.class);
        registerBean(BlockUserCommand.class);
        registerBean(DeleteUserCommand.class);
        registerBean(EditProfileCommand.class);
        registerBean(UserInfoCommand.class);
        registerBean(ServiceInfoCommand.class);
        registerBean(ConsumerInfoCommand.class);
        registerBean(CourierInfoCommand.class);
        registerBean(ViewAllUserCommand.class);
        registerBean(RegisterUserCommand.class);
        registerBean(LoginUserCommand.class);
        registerBean(LogoutUserCommand.class);
        registerBean(UserDaoImpl.class);
        registerBean(UserServiceImpl.class);
        registerBean(UserInfoDaoImpl.class);
        registerBean(CourierServiceCommand.class);
        registerBean(CourierDaoImpl.class);
        registerBean(CourierServiceImpl.class);
        registerBean(CourierServiceValidator.class);
        registerBean(RegisterUserValidator.class);
        registerBean(RegionDaoImpl.class);
        registerBean(TransportDaoImpl.class);
        registerBean(DeleteCourierServiceCommand.class);
        registerBean(EditCourierServiceCommand.class);
        registerBean(ViewAllCourierServiceCommand.class);
        registerBean(CheckCourierServiceCommand.class);
        registerBean(DeleteCourierServiceAdminCommand.class);
        registerBean(EditStatusCourierServiceCommand.class);
        registerBean(CreateOrderCommand.class);
        registerBean(OrderValidator.class);
        registerBean(OrderDaoImpl.class);
        registerBean(OrderServiceImpl.class);
        registerBean(OrderListCommand.class);
        registerBean(DeleteOrderCommand.class);
        registerBean(EditOrderCommand.class);
        registerBean(ViewAllUserOrderCommand.class);
        registerBean(DeleteUserOrderCommand.class);
        registerBean(EditOrderStatusCommand.class);
        registerBean(PossibleCourierListCommand.class);
        registerBean(ViewOrderCommand.class);
        registerBean(ContactWithCourierCommand.class);
        registerBean(MessageDaoImpl.class);
    }

    @Override
    public <T> void registerBean(T bean) {
        this.beanRegistry.registerBean(bean);
    }

    @Override
    public <T> void registerBean(Class<T> beanClass) {
        this.beanRegistry.registerBean(beanClass);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        return this.beanRegistry.getBean(beanClass);
    }

    @Override
    public <T> T getBean(String name) {
        return this.beanRegistry.getBean(name);
    }

    @Override
    public <T> boolean removeBean(T bean) {
        return this.beanRegistry.removeBean(bean);
    }
}
