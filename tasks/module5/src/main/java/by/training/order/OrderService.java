package by.training.order;

import java.util.List;

public interface OrderService {
    boolean save(OrderDto orderDto) throws OrderServiceException;

    boolean deleteOrder(OrderDto orderDto) throws OrderServiceException;

    OrderDto getUserOrderById(long id) throws OrderServiceException;

    boolean updateOrder(OrderDto orderDto) throws OrderServiceException;

    int getCountPage(long id) throws OrderServiceException;

    int getCountPage() throws OrderServiceException;

    List<OrderDto> getOrderByPage(int page, long userId) throws OrderServiceException;

    List<OrderDto> getOrderByPage(int page) throws OrderServiceException;

    boolean editOrderStatus(OrderDto orderDto) throws OrderServiceException;
}
