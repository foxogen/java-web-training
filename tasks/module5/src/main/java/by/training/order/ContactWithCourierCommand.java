package by.training.order;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.courier.CourierService;
import by.training.courier.CourierServiceException;
import by.training.entity.Message;
import by.training.user.UserDto;
import by.training.user.UserService;
import by.training.user.UserServiceException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.CONTACT_WITH_COURIER_COMMAND;

@Log4j
@Bean(nameCommand = CONTACT_WITH_COURIER_COMMAND)
@AllArgsConstructor
public class ContactWithCourierCommand implements ServletCommand {

    private UserService service;
    private CourierService courierService;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                String serviceId = req.getParameter("courierServiceId");
                String city = req.getParameter("city");
                String page = req.getParameter("page");
                System.out.println(serviceId + city + page);
                String message = req.getParameter("message");
                Message mes = new Message();
                mes.setCourierServiceId(Long.parseLong(serviceId));
                mes.setMessage(message);

                courierService.saveMessage(mes);

                resp.sendRedirect(req.getContextPath() + "/possibleCouriersList?city=" + city);
            } else {
                String city = req.getParameter("city");
                String serviceId = req.getParameter("courierServiceId");
                String page = req.getParameter("page");
                UserDto courierForContactUser = service.getCourierForContactUser(Long.parseLong(serviceId));
                req.setAttribute("page", page);
                req.setAttribute("courier", courierForContactUser);
                req.setAttribute("city", city);
                req.getRequestDispatcher("/jsp/contactWithCourierPage.jsp").forward(req, resp);
            }
        } catch (CourierServiceException | UserServiceException | ServletException | IOException e) {
            log.error("Error during get courier for contact!", e);
            throw new CommandException("Error during get courier for contact!", e);
        }
    }
}
