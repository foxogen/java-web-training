package by.training.order;

import by.training.SecurityService;
import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.user.UserDto;
import by.training.util.ValidatorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static by.training.command.ServletCommandType.ORDER_LIST_COMMAND;

@Log4j
@Bean(nameCommand = ORDER_LIST_COMMAND)
@AllArgsConstructor
public class OrderListCommand implements ServletCommand {

    private OrderService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            HttpSession session = req.getSession();
            UserDto loginUser = SecurityService.getInstance().getCurrentUser(session);

            int countPage = service.getCountPage(Math.toIntExact(loginUser.getId()));
            int countUserForOnePage = 3;
            int page;
            String currentPage = req.getParameter("page");
            if (ValidatorUtil.isDigit(currentPage)) {
                page = Integer.parseInt(currentPage);
            } else {
                page = 1;
                req.setAttribute("currentPage", 1);
            }
            int noOfPages = (int) Math.ceil(countPage * 1.0 / countUserForOnePage);
            if (page < 1 || page > noOfPages) {
                req.setAttribute("currentPage", 1);
                page = 1;
            }

            List<OrderDto> allOrder = service.getOrderByPage((page - 1) * countUserForOnePage, Math.toIntExact(loginUser.getId()));
            req.setAttribute("allOrders", allOrder);
            req.setAttribute("page", page);
            req.setAttribute("lastPage", noOfPages);

            req.getRequestDispatcher("/jsp/ordersListPage.jsp").forward(req, resp);

        } catch (IOException | OrderServiceException | ServletException e) {
            log.error("Error during get all courier services for one user!!", e);
            throw new CommandException("Error during get all courier services for one user!!", e);
        }
    }
}
