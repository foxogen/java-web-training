package by.training.order;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static by.training.command.ServletCommandType.DELETE_ORDER_COMMAND;

@Log4j
@Bean(nameCommand = DELETE_ORDER_COMMAND)
@AllArgsConstructor
public class DeleteOrderCommand implements ServletCommand {

    private OrderService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            String orderId = req.getParameter("orderId");

            OrderDto orderDto = new OrderDto();
            orderDto.setId(Long.parseLong(orderId));
            service.deleteOrder(orderDto);

            resp.sendRedirect(req.getContextPath() + "/ordersList");
        } catch (IOException | OrderServiceException e) {
            log.error("Error during delete user order!", e);
            throw new CommandException("Error during delete user order!", e);
        }
    }
}
