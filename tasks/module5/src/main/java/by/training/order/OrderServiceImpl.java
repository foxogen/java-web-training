package by.training.order;

import by.training.core.Bean;
import by.training.dao.DaoException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.util.List;

@Bean
@AllArgsConstructor
@Log4j
public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao;

    @Override
    public boolean save(OrderDto orderDto) throws OrderServiceException {
        try {
            this.orderDao.save(orderDto);
            return true;
        } catch (DaoException e) {
            log.error("Error during save new user order!", e);
            throw new OrderServiceException("Error during save new user order!", e);
        }
    }

    @Override
    public boolean updateOrder(OrderDto orderDto) throws OrderServiceException {
        try {
            return this.orderDao.update(orderDto);
        } catch (DaoException e) {
            log.error("Error during update user order!", e);
            throw new OrderServiceException("Error during update user order!", e);
        }
    }

    @Override
    public boolean deleteOrder(OrderDto orderDto) throws OrderServiceException {
        try {
            this.orderDao.delete(orderDto);
            return true;
        } catch (DaoException e) {
            log.error("Error during delete user order!", e);
            throw new OrderServiceException("Error during delete user order!", e);
        }
    }

    @Override
    public OrderDto getUserOrderById(long id) throws OrderServiceException {
        try {
            return this.orderDao.getById(id);
        } catch (DaoException e) {
            log.error("Error during get user order!", e);
            throw new OrderServiceException("Error during get user order!", e);
        }
    }

    @Override
    public int getCountPage(long id) throws OrderServiceException {
        try {
            return this.orderDao.getCountPage(id);
        } catch (DaoException e) {
            log.error("Error during get count page!", e);
            throw new OrderServiceException("Error during get count page!", e);
        }
    }

    @Override
    public int getCountPage() throws OrderServiceException {
        try {
            return this.orderDao.getCountPage();
        } catch (DaoException e) {
            log.error("Error during get all page!", e);
            throw new OrderServiceException("Error during get all page!", e);
        }
    }

    @Override
    public List<OrderDto> getOrderByPage(int page, long userId) throws OrderServiceException {
        try {
            return this.orderDao.getOrderByPage(page, userId);
        } catch (DaoException e) {
            log.error("Error during get all orders one user!", e);
            throw new OrderServiceException("Error during get all orders one user!", e);
        }
    }

    @Override
    public List<OrderDto> getOrderByPage(int page) throws OrderServiceException {
        try {
            return this.orderDao.getOrderByPage(page);
        } catch (DaoException e) {
            log.error("Error during get all orders!", e);
            throw new OrderServiceException("Error during get all orders!", e);
        }
    }

    @Override
    public boolean editOrderStatus(OrderDto orderDto) throws OrderServiceException {
        try {
            return this.orderDao.editOrderStatus(orderDto);
        } catch (DaoException e) {
            log.error("Error during edit order status!", e);
            throw new OrderServiceException("Error during edit order status!", e);
        }
    }
}
