package by.training.order;

import by.training.entity.OrderStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class OrderDto {

    private long id;
    private long userAccountId;
    private LocalDate dataOrder;
    private String exportAddress;
    private String importAddress;
    private double price;
    private int cargoWeight;
    private String mobilePhone;
    private String moreInfo;
    private String city;
    private OrderStatus status;

}
