package by.training.order;

import by.training.SecurityService;
import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.entity.OrderStatus;
import by.training.user.UserDto;
import by.training.validate.OrderValidator;
import by.training.validate.ValidationResult;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static by.training.command.ServletCommandType.CREATE_ORDER_COMMAND;

@Log4j
@Bean(nameCommand = CREATE_ORDER_COMMAND)
@AllArgsConstructor
public class CreateOrderCommand implements ServletCommand {

    private OrderService service;
    private OrderValidator validator;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                Map<String, String> parsedMap = parseParameter(req);
                ValidationResult validate = validator.validate(parsedMap);

                if (validate.isValid()) {
                    OrderDto orderDto = createUserFromParameter(req);
                    service.save(orderDto);
                    resp.sendRedirect(req.getContextPath() + "/ordersList");
                } else {
                    req.setAttribute("checkData", true);
                    req.setAttribute("createOrderError", validate.getErrors().get("createOrderError"));
                    req.getRequestDispatcher("/jsp/createOrderPage.jsp").forward(req, resp);
                }

            } else {
                req.getRequestDispatcher("/jsp/createOrderPage.jsp").forward(req, resp);
            }
        } catch (ServletException | OrderServiceException | IOException e) {
            log.error("Error during create new user order!", e);
            throw new CommandException("Error during create new user order!", e);
        }
    }

    private Map<String, String> parseParameter(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();
        map.put("orderDate", request.getParameter("orderDate"));
        map.put("exportAddress", request.getParameter("exportAddress"));
        map.put("importAddress", request.getParameter("importAddress"));
        map.put("contactPhone", request.getParameter("contactPhone"));
        map.put("cargoWeight", request.getParameter("cargoWeight"));
        map.put("price", request.getParameter("price"));
        map.put("moreInfo", request.getParameter("moreInfo"));
        map.put("city", request.getParameter("city"));

        return map;
    }

    private OrderDto createUserFromParameter(HttpServletRequest req) {
        String orderDate = req.getParameter("orderDate");
        String exportAddress = req.getParameter("exportAddress");
        String importAddress = req.getParameter("importAddress");
        String contactPhone = req.getParameter("contactPhone");
        String cargoWeight = req.getParameter("cargoWeight");
        String price = req.getParameter("price");
        String moreInfo = req.getParameter("moreInfo");
        String city = req.getParameter("city");

        UserDto currentUser = SecurityService.getInstance().getCurrentUser(req.getSession());

        OrderDto orderDto = new OrderDto();
        orderDto.setUserAccountId(currentUser.getId());
        String[] split = orderDate.split("-");
        orderDto.setDataOrder(LocalDate.of(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2])));
        orderDto.setExportAddress(exportAddress);
        orderDto.setImportAddress(importAddress);
        orderDto.setMobilePhone(contactPhone);
        orderDto.setCargoWeight(Integer.parseInt(cargoWeight));
        orderDto.setPrice(Double.parseDouble(price));
        orderDto.setMoreInfo(moreInfo);
        orderDto.setCity(city);
        orderDto.setStatus(OrderStatus.SUBMITTED);

        return orderDto;
    }
}
