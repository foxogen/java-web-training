package by.training.order;

import by.training.core.Bean;
import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.Order;
import by.training.entity.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Bean
@Log4j
@AllArgsConstructor
public class OrderDaoImpl implements OrderDao {

    private static final String INSERT_ORDER = "insert into user_order (text_order, user_account_id, data_order, export_address," +
            "import_address, price, cargo_weight, mob_phone, city, order_status_id) values (?,?,?,?,?,?,?,?,?,?)";
    private static final String SELECT_COUNT_PAGE_BY_ID = "select count(*) from user_order where user_account_id =(?)";
    private static final String SELECT_COUNT_PAGE = "select count(*) from user_order";
    private static final String SELECT_ORDERS_BY_PAGE_BY_ID = "select * from user_order where user_account_id = (?) order by id offset (?) limit 3";
    private static final String SELECT_ORDERS_BY_PAGE = "select * from user_order order by id offset (?) limit 3";
    private static final String DELETE_USER_ORDER = "delete from user_order where id =(?)";
    private static final String SELECT_ORDER_BY_ID = "select * from user_order where id = (?)";
    private static final String UPDATE_ORDER = "update user_order set text_order = (?), data_order =(?), export_address =(?)," +
            "import_address =(?), price = (?), cargo_weight =(?), mob_phone = (?), city = (?) where id =(?)";
    private static final String UPDATE_ORDER_STATUS = "update user_order set order_status_id = (?) where id =(?)";

    private ConnectionManager connectionManager;

    @Override
    public Long save(OrderDto entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_ORDER);
            Order order = fromDto(entity);
            int i = 0;
            statement.setString(++i, order.getMoreInfo());
            statement.setInt(++i, Math.toIntExact(order.getUserAccountId()));
            statement.setObject(++i, order.getDataOrder());
            statement.setString(++i, order.getExportAddress());
            statement.setString(++i, order.getImportAddress());
            statement.setDouble(++i, order.getPrice());
            statement.setInt(++i, order.getCargoWeight());
            statement.setString(++i, order.getMobilePhone());
            statement.setString(++i, order.getCity());
            statement.setInt(++i, order.getStatus().ordinal() + 1);

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
            return entity.getId();
        } catch (SQLException e) {
            log.error("Error during save new user order in DB!", e);
            throw new DaoException("Error during save new user order in DB!", e);
        }
    }

    @Override
    public boolean update(OrderDto entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_ORDER);
            Order order = fromDto(entity);
            int i = 0;
            statement.setString(++i, order.getMoreInfo());
            statement.setObject(++i, order.getDataOrder());
            statement.setString(++i, order.getExportAddress());
            statement.setString(++i, order.getImportAddress());
            statement.setDouble(++i, order.getPrice());
            statement.setInt(++i, order.getCargoWeight());
            statement.setString(++i, order.getMobilePhone());
            statement.setString(++i, order.getCity());
            statement.setInt(++i, Math.toIntExact(order.getId()));

            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            log.error("Error during update user order from DB!", e);
            throw new DaoException("Error during update user order from DB!", e);
        }
    }

    @Override
    public boolean delete(OrderDto entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_USER_ORDER);
            Order order = fromDto(entity);
            statement.setInt(1, Math.toIntExact(order.getId()));
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            log.error("Error during delete user order from DB!", e);
            throw new DaoException("Error during delete user order from DB!", e);
        }
    }

    @Override
    public OrderDto getById(Long id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ORDER_BY_ID);
            statement.setInt(1, Math.toIntExact(id));

            ResultSet resultSet = statement.executeQuery();
            Order order = new Order();
            while (resultSet.next()) {
                LocalDate orderDate = resultSet.getDate("data_order").toLocalDate();
                String moreInfo = resultSet.getString("text_order");
                String exportAddress = resultSet.getString("export_address");
                String importAddress = resultSet.getString("import_address");
                double price = resultSet.getDouble("price");
                int cargoWeight = resultSet.getInt("cargo_weight");
                String workPhone = resultSet.getString("mob_phone");
                long userAccountId = resultSet.getInt("user_account_id");
                String city = resultSet.getString("city");
                int orderStatus = resultSet.getInt("order_status_id");

                OrderStatus status = OrderStatus.values()[orderStatus - 1];

                order.setId(id);
                order.setMobilePhone(workPhone);
                order.setMoreInfo(moreInfo);
                order.setDataOrder(orderDate);
                order.setExportAddress(exportAddress);
                order.setImportAddress(importAddress);
                order.setPrice(price);
                order.setCargoWeight(cargoWeight);
                order.setUserAccountId(userAccountId);
                order.setCity(city);
                order.setStatus(status);
            }
            return fromEntity(order);
        } catch (SQLException e) {
            log.error("Error during get user order by id from DB!", e);
            throw new DaoException("Error during get user order by id from DB!", e);
        }
    }

    @Override
    public List<OrderDto> findAll() throws DaoException {
        return null;
    }

    @Override
    public int getCountPage(long id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COUNT_PAGE_BY_ID);
            statement.setInt(1, Math.toIntExact(id));
            ResultSet resultSet = statement.executeQuery();
            int countPage = 0;
            while (resultSet.next()) {
                countPage = resultSet.getInt(1);
            }
            return countPage;
        } catch (SQLException e) {
            log.error("Error during get count page!", e);
            throw new DaoException("Error during get count page!", e);
        }
    }

    @Override
    public int getCountPage() throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COUNT_PAGE);
            ResultSet resultSet = statement.executeQuery();
            int countPage = 0;
            while (resultSet.next()) {
                countPage = resultSet.getInt(1);
            }
            return countPage;
        } catch (SQLException e) {
            log.error("Error during get all page!", e);
            throw new DaoException("Error during get all page!", e);
        }
    }

    @Override
    public List<OrderDto> getOrderByPage(int page, long userId) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ORDERS_BY_PAGE_BY_ID);
            int i = 0;
            statement.setInt(++i, Math.toIntExact(userId));
            statement.setInt(++i, page);
            ResultSet resultSet = statement.executeQuery();

            List<OrderDto> list = new ArrayList<>();
            while (resultSet.next()) {
                LocalDate orderDate = resultSet.getDate("data_order").toLocalDate();
                String moreInfo = resultSet.getString("text_order");
                String exportAddress = resultSet.getString("export_address");
                String importAddress = resultSet.getString("import_address");
                double price = resultSet.getDouble("price");
                int cargoWeight = resultSet.getInt("cargo_weight");
                String workPhone = resultSet.getString("mob_phone");
                long orderId = resultSet.getInt("id");
                String city = resultSet.getString("city");
                int orderStatus = resultSet.getInt("order_status_id");

                OrderStatus status = OrderStatus.values()[orderStatus - 1];

                Order order = new Order();

                order.setId(orderId);
                order.setMobilePhone(workPhone);
                order.setMoreInfo(moreInfo);
                order.setDataOrder(orderDate);
                order.setExportAddress(exportAddress);
                order.setImportAddress(importAddress);
                order.setPrice(price);
                order.setCargoWeight(cargoWeight);
                order.setUserAccountId(userId);
                order.setCity(city);
                order.setStatus(status);

                list.add(fromEntity(order));
            }
            return list;
        } catch (SQLException e) {
            log.error("Error during get all orders one user!", e);
            throw new DaoException("Error during find all orders one user!", e);
        }
    }

    @Override
    public List<OrderDto> getOrderByPage(int page) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ORDERS_BY_PAGE);
            int i = 0;
            statement.setInt(++i, page);
            ResultSet resultSet = statement.executeQuery();

            List<OrderDto> list = new ArrayList<>();
            while (resultSet.next()) {
                LocalDate orderDate = resultSet.getDate("data_order").toLocalDate();
                String moreInfo = resultSet.getString("text_order");
                String exportAddress = resultSet.getString("export_address");
                String importAddress = resultSet.getString("import_address");
                double price = resultSet.getDouble("price");
                int cargoWeight = resultSet.getInt("cargo_weight");
                String workPhone = resultSet.getString("mob_phone");
                long orderId = resultSet.getInt("id");
                long userAccountId = resultSet.getInt("user_account_id");
                String city = resultSet.getString("city");
                int orderStatus = resultSet.getInt("order_status_id");

                OrderStatus status = OrderStatus.values()[orderStatus - 1];

                Order order = new Order();

                order.setId(orderId);
                order.setMobilePhone(workPhone);
                order.setMoreInfo(moreInfo);
                order.setDataOrder(orderDate);
                order.setExportAddress(exportAddress);
                order.setImportAddress(importAddress);
                order.setPrice(price);
                order.setCargoWeight(cargoWeight);
                order.setUserAccountId(userAccountId);
                order.setCity(city);
                order.setStatus(status);

                list.add(fromEntity(order));
            }
            return list;
        } catch (SQLException e) {
            log.error("Error during get all orders from DB!", e);
            throw new DaoException("Error during find all orders from DB!", e);
        }
    }

    @Override
    public boolean editOrderStatus(OrderDto orderDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_ORDER_STATUS);
            int i = 0;
            Order order = fromDto(orderDto);
            statement.setInt(++i, order.getStatus().ordinal() + 1);
            statement.setInt(++i, Math.toIntExact(order.getId()));

            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            log.error("Error during edit order status from DB!", e);
            throw new DaoException("Error during edit order status from DB!", e);
        }
    }

    private Order fromDto(OrderDto orderDto) {
        Order order = new Order();

        order.setId(orderDto.getId());
        order.setUserAccountId(orderDto.getUserAccountId());
        order.setCargoWeight(orderDto.getCargoWeight());
        order.setDataOrder(orderDto.getDataOrder());
        order.setExportAddress(orderDto.getExportAddress());
        order.setImportAddress(orderDto.getImportAddress());
        order.setMoreInfo(orderDto.getMoreInfo());
        order.setMobilePhone(orderDto.getMobilePhone());
        order.setPrice(orderDto.getPrice());
        order.setCity(orderDto.getCity());
        order.setStatus(orderDto.getStatus());

        return order;
    }

    private OrderDto fromEntity(Order order) {
        OrderDto orderDto = new OrderDto();

        orderDto.setId(order.getId());
        orderDto.setMoreInfo(order.getMoreInfo());
        orderDto.setDataOrder(order.getDataOrder());
        orderDto.setExportAddress(order.getExportAddress());
        orderDto.setImportAddress(order.getImportAddress());
        orderDto.setPrice(order.getPrice());
        orderDto.setCargoWeight(order.getCargoWeight());
        orderDto.setMobilePhone(order.getMobilePhone());
        orderDto.setUserAccountId(order.getUserAccountId());
        orderDto.setCity(order.getCity());
        orderDto.setStatus(order.getStatus());

        return orderDto;
    }
}
