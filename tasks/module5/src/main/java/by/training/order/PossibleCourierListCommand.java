package by.training.order;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.courier.CourierDto;
import by.training.courier.CourierService;
import by.training.courier.CourierServiceException;
import by.training.util.ValidatorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.training.command.ServletCommandType.POSSIBLE_COURIER_LIST_COMMAND;

@Log4j
@Bean(nameCommand = POSSIBLE_COURIER_LIST_COMMAND)
@AllArgsConstructor
public class PossibleCourierListCommand implements ServletCommand {

    private CourierService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            String city = req.getParameter("city");
            int countPage = service.getCountPage(city);
            int countPossibleCourierForOnePage = 3;
            int page;
            String currentPage = req.getParameter("page");
            if (ValidatorUtil.isDigit(currentPage)) {
                page = Integer.parseInt(currentPage);
            } else {
                page = 1;
                req.setAttribute("currentPage", 1);
            }
            int noOfPages = (int) Math.ceil(countPage * 1.0 / countPossibleCourierForOnePage);
            if (page < 1 || page > noOfPages) {
                page = 1;
                req.setAttribute("currentPage", 1);
            }

            List<CourierDto> allCourierByWorkRegion = service.getAllCourierByWorkRegion(city, (page - 1) * countPossibleCourierForOnePage);
            req.setAttribute("allService", allCourierByWorkRegion);
            req.setAttribute("city", city);
            req.setAttribute("page", page);
            req.setAttribute("lastPage", noOfPages);

            req.getRequestDispatcher("/jsp/possibleCourierListPage.jsp").forward(req, resp);
        } catch (CourierServiceException | ServletException | IOException e) {
            log.error("Error during get all courier services by regions!!", e);
            throw new CommandException("Error during get all courier services by regions!!", e);
        }
    }
}
