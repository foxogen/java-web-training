package by.training.order;

import by.training.dao.CRUDDao;
import by.training.dao.DaoException;

import java.util.List;

public interface OrderDao extends CRUDDao<OrderDto, Long> {
    int getCountPage(long id) throws DaoException;

    int getCountPage() throws DaoException;

    List<OrderDto> getOrderByPage(int page, long userId) throws DaoException;

    List<OrderDto> getOrderByPage(int page) throws DaoException;

    boolean editOrderStatus(OrderDto orderDto) throws DaoException;
}
