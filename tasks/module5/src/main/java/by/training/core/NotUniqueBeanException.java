package by.training.core;

public class NotUniqueBeanException extends BeanRegistrationException {
    public NotUniqueBeanException(String message) {
        super(message);
    }
}
