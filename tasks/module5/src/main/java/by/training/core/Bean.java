package by.training.core;

import by.training.command.ServletCommandType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Bean {

    ServletCommandType nameCommand() default ServletCommandType.DEFAULT_COMMAND;

    boolean proxy() default false;
}
