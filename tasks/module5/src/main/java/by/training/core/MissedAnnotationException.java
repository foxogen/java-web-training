package by.training.core;

public class MissedAnnotationException extends BeanRegistrationException {

    public MissedAnnotationException(String message) {
        super(message);
    }
}
