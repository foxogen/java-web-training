package by.training.core;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Log4j
public class BeanRegistryImpl implements BeanRegistry {

    private FactoryBean factoryBean = new FactoryBean();
    private Set<RegistryInfo> beanRegistry = new HashSet<>();

    @Override
    public <T> void registerBean(T bean) {
        RegistryInfo info = calculateRegistryInfo(bean.getClass());
        info.setConcreteBean(bean);
        addRegistryInfo(info);
        log.info("Created by Bean " + bean.getClass().getName());
    }

    @Override
    public <T> void registerBean(Class<T> beanClass) {

        RegistryInfo info = calculateRegistryInfo(beanClass);
        final Supplier<Object> factory = createFactory(info);
        info.setFactory(factory);
        addRegistryInfo(info);
        log.info("Created by Bean " + beanClass.getName());
    }

    private void addRegistryInfo(RegistryInfo info) {
        beanRegistry.stream()
                .filter(registryInfo -> registryInfo.getName().equals(info.getName()))
                .findFirst()
                .ifPresent(registryInfo -> {
                    log.error("Bean with name " + registryInfo.getName() + " already registered");
                    throw new NotUniqueBeanException("Bean with name " + registryInfo.getName() + " already registered");
                });
        beanRegistry.add(info);
    }

    private Supplier<Object> createFactory(RegistryInfo info) {

        Class<?> clazz = info.getClazz();
        Constructor<?>[] constructors = clazz.getDeclaredConstructors();
        if (constructors.length > 1) {
            log.error("More than 1 constructor is present for class " + clazz.getSimpleName());
            throw new BeanInstantiationException("More than 1 constructor is present for class " + clazz.getSimpleName());
        }

        return () -> {
            Constructor<?> constructor = constructors[0];
            if (constructor.getParameterCount() > 0) {
                Parameter[] parameters = constructor.getParameters();
                Object[] args = new Object[parameters.length];
                for (int i = 0; i < parameters.length; i++) {

                    Class<?> type = parameters[i].getType();
                    args[i] = getBean(type);

                }
                try {
                    return constructor.newInstance(args);
                } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
                    log.error("Failed to instantiate bean", e);
                    throw new BeanInstantiationException("Failed to instantiate bean", e);
                }
            } else {
                try {
                    return clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    log.error("Failed to instantiate bean", e);
                    throw new BeanInstantiationException("Failed to instantiate bean", e);
                }
            }
        };
    }

    private RegistryInfo calculateRegistryInfo(Class<?> beanClass) {

        Bean bean = beanClass.getAnnotation(Bean.class);
        if (bean == null) {
            log.error(beanClass.getName() + " doesn't have @Bean annotation");
            throw new MissedAnnotationException(beanClass.getName() + " doesn't have @Bean annotation");
        }

        RegistryInfo registryInfo = new RegistryInfo();
        registryInfo.setClazz(beanClass);

        Class<?>[] interfaces = beanClass.getInterfaces();
        registryInfo.setInterfaces(Arrays.stream(interfaces).collect(Collectors.toSet()));

        Annotation[] annotations = beanClass.getAnnotations();
        registryInfo.setAnnotations(Arrays.stream(annotations).collect(Collectors.toSet()));

        String beanName = bean.nameCommand().getName();
        if (beanName.trim().length() > 0) {
            registryInfo.setName(beanName);
        } else if (interfaces.length == 1) {
            registryInfo.setName(interfaces[0].getSimpleName());
        } else {
            registryInfo.setName(beanClass.getSimpleName());
        }

        return registryInfo;
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {

        Bean bean = beanClass.getAnnotation(Bean.class);
        String beanName = bean != null && bean.nameCommand().getName().trim().length() > 0 ? bean.nameCommand().getName().trim() : null;
        Predicate<RegistryInfo> searchBean = info -> info.getName().equals(beanName)
                || info.getClazz().equals(beanClass) || info.getInterfaces().contains(beanClass);
        return getBean(searchBean);
    }

    @Override
    public <T> T getBean(String beanName) {
        Predicate<RegistryInfo> searchBean = info -> info.getName().equals(beanName) || info.getClazz().getSimpleName().equals(beanName);
        return getBean(searchBean);
    }

    @Override
    public void destroy() {
        this.factoryBean.destroy();
        this.beanRegistry.clear();
    }

    @SuppressWarnings("unchecked")
    private <T> T getBean(Predicate<RegistryInfo> searchBean) {
        List<RegistryInfo> registryInfoList = beanRegistry
                .stream()
                .filter(searchBean)
                .collect(Collectors.toList());
        if (registryInfoList.size() > 1) {
            String multipleNames = registryInfoList.stream().map(RegistryInfo::getName).collect(Collectors.joining(", "));
            log.error("Multiple implementations found: " + multipleNames);
            throw new NotUniqueBeanException("Multiple implementations found: " + multipleNames);
        } else {
            return (T) registryInfoList.stream()
                    .map(registryInfo -> factoryBean.getBean(registryInfo))
                    .findFirst()
                    .orElse(null);
        }
    }

    @Override
    public <T> boolean removeBean(T bean) {

        RegistryInfo registryInfo = calculateRegistryInfo(bean.getClass());
        return beanRegistry.remove(registryInfo);
    }

    @Data
    @NoArgsConstructor
    @EqualsAndHashCode
    private static class RegistryInfo {

        private String name;
        private Class<?> clazz;
        private Set<Class<?>> interfaces;
        private Set<Annotation> annotations;
        private Supplier<?> factory;
        private Object concreteBean;
    }

    private static class FactoryBean {

        private Map<RegistryInfo, Object> beans = new ConcurrentHashMap<>();

        Object getBean(RegistryInfo info) {

            if (info.getConcreteBean() != null) {
                beans.put(info, info.getConcreteBean());
            } else if (!beans.containsKey(info)) {
                final Object bean = info.getFactory().get();
                beans.put(info, bean);
            }
            return beans.get(info);
        }


        void destroy() {
            beans.clear();
        }
    }
}
