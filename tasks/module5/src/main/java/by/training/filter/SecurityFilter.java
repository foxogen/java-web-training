package by.training.filter;

import by.training.SecurityService;
import by.training.entity.UserRole;
import by.training.user.UserDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@WebFilter(urlPatterns = "/*")
@Log4j
public class SecurityFilter implements Filter {
    private List<String> commands;
    private List<String> roles;
    private Map<String, List<UserRole>> commandsRolesMap = new HashMap<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("security.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);
            commands = new LinkedList<>(Arrays.asList(properties.getProperty("commands").split(", ")));
            roles = new LinkedList<>(Arrays.asList(properties.getProperty("roles").split(", ")));
            commands.forEach(command -> {
                command = command.trim();
                List<String> roles = Arrays.asList(properties.getProperty("command." + command).split(", "));
                List<UserRole> userRoleList = roles.stream()
                        .map(role -> {
                            Optional<UserRole> userRole = UserRole.fromString(role);
                            if (!userRole.isPresent()) {
                                log.error("Failed during init SecurityFilter!");
                                throw new IllegalArgumentException("Failed during init SecurityFilter!");
                            }
                            return userRole.get();
                        }).collect(Collectors.toList());
                commandsRolesMap.put(command, userRoleList);
            });
        } catch (IOException e) {
            log.error("Failed to read security.properties", e);
            throw new ServletException("Failed to read security.properties", e);
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession(false);
        String servletPath = request.getServletPath().substring(1);

        List<UserRole> userRoles = new ArrayList<>();
        if (session != null) {
            UserDto userDto = SecurityService.getInstance().getCurrentUser(session);
            if (userDto != null) {
                userRoles.add(SecurityService.getInstance().getCurrentRoles(session));
                userRoles.add(UserRole.ALL);
            } else {
                userRoles.add(UserRole.DEFAULT);
            }
        } else {
            userRoles.add(UserRole.DEFAULT);
        }

        List<UserRole> commandRoles;
        if ((commandRoles = commandsRolesMap.get(servletPath)) != null) {
            if (userRoles.stream().anyMatch(commandRoles::contains)) {
                log.info("Command " + servletPath + " was checked and completed");
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                log.info("Command " + servletPath + " wasn't completed");
                ((HttpServletResponse) servletResponse).sendRedirect(request.getContextPath());
            }
        } else {
            log.info("Command " + servletPath + " unknown");
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
        commands.clear();
        roles.clear();
        commandsRolesMap.clear();
    }
}
