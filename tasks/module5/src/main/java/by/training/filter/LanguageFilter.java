package by.training.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@WebFilter(urlPatterns = "/*")
public class LanguageFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        Cookie[] cookies = httpRequest.getCookies();

        Optional<Cookie> langCookie;
        if (cookies != null) {
            langCookie = Arrays.stream(cookies)
                    .filter(c -> c.getName().equalsIgnoreCase("lang") &&
                            (c.getValue().equalsIgnoreCase("en") ||
                                    c.getValue().equalsIgnoreCase("ru")))
                    .findFirst();

            langCookie.ifPresent(cookie -> httpRequest.setAttribute("lang", langCookie.get().getValue()));
        } else {
            ((HttpServletResponse) servletResponse).addCookie(new Cookie("lang", "en"));
            httpRequest.setAttribute("lang", "en");
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {

    }
}
