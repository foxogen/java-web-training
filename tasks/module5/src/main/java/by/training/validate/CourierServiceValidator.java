package by.training.validate;

import by.training.core.Bean;
import lombok.AllArgsConstructor;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Bean
@AllArgsConstructor
public class CourierServiceValidator {

    private static final String REGEX_REGIONS = "([a-zA-Zа-яА-Я]+)([,|.]+)?";

    public ValidationResult validate(Map<String, String> map) {
        ValidationResult validationResult = new ValidationResult();
        if (map.get("startTime").isEmpty() || map.get("endTime").isEmpty() || map.get("regions").isEmpty()
                || map.get("workingPhone").isEmpty() || map.get("transport").isEmpty() ||
                map.get("capacity").isEmpty()) {
            validationResult.addError("courierServiceError", "page.courier_service_error_all_fields");
            return validationResult;
        }

        String startTime = map.get("startTime");
        if (startTime.split(":").length != 2 || startTime.length() < 5) {
            validationResult.addError("courierServiceError", "page.courier_service_error_start_working_time");
        }

        String endTime = map.get("endTime");
        if (endTime.split(":").length != 2 || endTime.length() < 5) {
            validationResult.addError("courierServiceError", "page.courier_service_error_end_working_time");
        }

        String regions = map.get("regions");
        Pattern pattern = Pattern.compile(REGEX_REGIONS);
        Matcher matcher = pattern.matcher(regions);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        if (count < 1) {
            validationResult.addError("courierServiceError", "page.courier_service_error_regions");
        }

        String capacity = map.get("capacity");
        if (!checkInt(capacity)) {
            validationResult.addError("courierServiceError", "page.courier_service_error_capacity");
        }

        return validationResult;
    }

    private boolean checkInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
