package by.training.validate;

import by.training.core.Bean;
import by.training.user.UserDto;
import by.training.user.UserService;
import by.training.user.UserServiceException;
import lombok.AllArgsConstructor;

import java.util.Map;

@Bean
@AllArgsConstructor
public class RegisterUserValidator{

    private static final String REGEX_FOR_NAME = "^[a-zA-Zа-яА-Я'][a-zA-Zа-яА-Я-' ]+[a-zA-Zа-яА-Я']";
    private static final String REGEX_EMAIL = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
    private static final String REGEX_PHONE = "^(\\+\\d{1,3}[- ]?)?\\d{9,11}$";
    private UserService service;

    public ValidationResult validate(Map<String, String> map) throws UserServiceException {
        ValidationResult validationResult = new ValidationResult();
        if (map.get("login").isEmpty() || map.get("password").isEmpty() || map.get("firstName").isEmpty()
                || map.get("lastName").isEmpty() || map.get("email").isEmpty() || map.get("phone").isEmpty()
                || map.get("city").isEmpty()) {
            validationResult.addError("registrationError", "page.registration_all_fields");
            return validationResult;
        }

        String login = map.get("login");
        UserDto userDto = new UserDto();
        userDto.setLogin(login);
        if (service.findUserByLogin(userDto)) {
            validationResult.addError("registrationError", "page.login_busy");
        }

        String fName = map.get("firstName");
        if (!fName.matches(REGEX_FOR_NAME)) {
            validationResult.addError("registrationError", "page.registration_invalid_name");
        }

        String lName = map.get("lastName");
        if (!lName.matches(REGEX_FOR_NAME)) {
            validationResult.addError("registrationError", "page.registration_invalid_last_name");
        }

        String email = map.get("email");
        if (!email.matches(REGEX_EMAIL)) {
            validationResult.addError("registrationError", "page.registration_invalid_email");
        }

        String city = map.get("city");
        if (!city.matches(REGEX_FOR_NAME)) {
            validationResult.addError("registrationError", "page.registration_invalid_city");
        }

        String phone = map.get("phone");
        if (!phone.matches(REGEX_PHONE)) {
            validationResult.addError("registrationError", "page.registration_invalid_phone");
        }

        return validationResult;
    }

    public ValidationResult validateForEditProfile(Map<String, String> map){
        ValidationResult validationResult = new ValidationResult();
        if (map.get("login").isEmpty() || map.get("password").isEmpty() || map.get("firstName").isEmpty()
                || map.get("lastName").isEmpty() || map.get("email").isEmpty() || map.get("phone").isEmpty()
                || map.get("city").isEmpty()) {
            validationResult.addError("registrationError", "page.registration_all_fields");
            return validationResult;
        }

        String fName = map.get("firstName");
        if (!fName.matches(REGEX_FOR_NAME)) {
            validationResult.addError("registrationError", "page.registration_invalid_name");
        }

        String lName = map.get("lastName");
        if (!lName.matches(REGEX_FOR_NAME)) {
            validationResult.addError("registrationError", "page.registration_invalid_last_name");
        }

        String email = map.get("email");
        if (!email.matches(REGEX_EMAIL)) {
            validationResult.addError("registrationError", "page.registration_invalid_email");
        }

        String city = map.get("city");
        if (!city.matches(REGEX_FOR_NAME)) {
            validationResult.addError("registrationError", "page.registration_invalid_city");
        }

        String phone = map.get("phone");
        if (!phone.matches(REGEX_PHONE)) {
            validationResult.addError("registrationError", "page.registration_invalid_phone");
        }

        return validationResult;
    }
}
