package by.training.validate;

import java.util.*;

public class ValidationResult {
    private Map<String, List<String>> errors;

    public ValidationResult() {
        this.errors = new HashMap<>();
    }

    public Map<String, List<String>> getErrors() {
        return new HashMap<>(errors);
    }

    public void addError(String type, String message){
        if (errors.containsKey(type)) {
            errors.get(type).add(message);
        } else {
            List<String> messages = new ArrayList<>();
            messages.add(message);
            errors.put(type, messages);
        }
    }

    public boolean isValid(){
        return this.errors.isEmpty();
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "errors=" + errors +
                '}';
    }
}
