package by.training.validate;

import by.training.core.Bean;
import by.training.util.ValidatorUtil;
import lombok.AllArgsConstructor;

import javax.print.DocFlavor;
import java.util.Map;

@Bean
@AllArgsConstructor
public class OrderValidator {

    private static final String REGEX_PHONE = "^(\\+\\d{1,3}[- ]?)?\\d{9,11}$";

    public ValidationResult validate(Map<String, String> map){
        ValidationResult validationResult = new ValidationResult();
        if (map.get("orderDate").isEmpty() || map.get("exportAddress").isEmpty() || map.get("importAddress").isEmpty()
                || map.get("contactPhone").isEmpty() || map.get("cargoWeight").isEmpty() || map.get("price").isEmpty()
                || map.get("moreInfo").isEmpty() || map.get("city").isEmpty()) {
            validationResult.addError("createOrderError", "page.registration_all_fields");
            return validationResult;
        }

        String cargoWeight = map.get("cargoWeight");
        if (!ValidatorUtil.isDigit(cargoWeight)) {
            validationResult.addError("createOrderError", "page.user_create_invalidate_cargo_weight");
        }

        String contactPhone = map.get("contactPhone");
        if (!contactPhone.matches(REGEX_PHONE)) {
            validationResult.addError("createOrderError", "page.registration_invalid_phone");
        }

        String price = map.get("price");
        if (!ValidatorUtil.isDigitDouble(price)) {
            validationResult.addError("createOrderError", "page.user_create_invalidate_price");
        }

        return validationResult;
    }
}
