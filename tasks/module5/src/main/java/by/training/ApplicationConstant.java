package by.training;

import by.training.entity.UserRole;

public class ApplicationConstant {

    public static final SecurityService SECURITY_SERVICE = SecurityService.getInstance();
    public static final UserRole USER_ROLE = UserRole.USER;
    public static final UserRole ADMIN_ROLE = UserRole.ADMIN;
    public static final UserRole COURIER_ROLE = UserRole.COURIER;

}