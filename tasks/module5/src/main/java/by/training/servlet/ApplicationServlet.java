package by.training.servlet;

import by.training.ApplicationContext;
import by.training.command.CommandException;
import by.training.command.ServletCommand;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
@WebServlet(urlPatterns = "/", loadOnStartup = 1, name = "app")
public class ApplicationServlet extends HttpServlet {

    private static final long serialVersionUID = -5372750245293466299L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String servletPath = req.getServletPath().substring(1);
        ServletCommand command = ApplicationContext.getInstance().getBean(servletPath);
        if (command != null) {
            try {
                command.execute(req, resp);
            } catch (CommandException e) {
                log.error("Error processing request!", e);
                throw new ServletException("Error processing request!", e);
            }
        } else {
            resp.sendRedirect(req.getContextPath());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

}
