package by.training.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalTime;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Courier {

    private long id;
    private LocalTime startTime;
    private LocalTime endTime;
    private long userAccountId;
    private long typeTransportId;
    private CourierServiceStatus serviceStatus;
    private long courierRegionId;
    private String aboutMyself;
    private String mobilePhone;

}
