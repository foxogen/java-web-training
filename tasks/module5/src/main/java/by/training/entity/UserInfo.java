package by.training.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode
public class UserInfo {

    private long id;
    private String mobilePhone;
    private String city;
    private String address;
}
