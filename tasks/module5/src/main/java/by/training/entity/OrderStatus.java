package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum OrderStatus {
    SUBMITTED, APPROVED, DECLINED, PERFORMED, DONE;

    public static Optional<OrderStatus> fromString(String name) {
        return Stream.of(OrderStatus.values())
                .filter(orderStatus -> orderStatus.name().equalsIgnoreCase(name))
                .findFirst();
    }
}
