package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum UserRole {
    COURIER, USER, ADMIN, ALL, DEFAULT;

    public static Optional<UserRole> fromString(String name) {
        return Stream.of(UserRole.values())
                .filter(userRole -> userRole.name().equalsIgnoreCase(name))
                .findFirst();
    }
}
