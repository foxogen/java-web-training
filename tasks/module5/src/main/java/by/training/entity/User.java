package by.training.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class User {

    private long id;
    private String login;
    private String password;
    private UserRole role;
    private String firstName;
    private String lastName;
    private String email;
    private boolean block;
}
