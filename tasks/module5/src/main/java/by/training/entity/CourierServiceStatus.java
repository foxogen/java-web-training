package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum CourierServiceStatus {
    SUBMITTED, APPROVED, DECLINED;

    public static Optional<CourierServiceStatus> fromString(String name) {
        return Stream.of(CourierServiceStatus.values())
                .filter(courierServiceStatus -> courierServiceStatus.name().equalsIgnoreCase(name))
                .findFirst();
    }
}
