package by.training.courier;

import by.training.dao.CRUDDao;
import by.training.dao.DaoException;

import java.util.List;

public interface CourierDao extends CRUDDao<CourierDto, Long> {

    int getCountPage(int id) throws DaoException;

    int getCountPage(String city) throws DaoException;

    int getCountPage() throws DaoException;

    List<CourierDto> getServiceByPage(int page, int id) throws DaoException;

    List<CourierDto> getServiceByPage(int page) throws DaoException;

    boolean editCourierServiceStatus(CourierDto courierDto) throws DaoException;

    List<CourierDto> getAllCourierByWorkRegion(String city, int page) throws DaoException;

}
