package by.training.courier;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.entity.Region;
import by.training.entity.Transport;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static by.training.command.ServletCommandType.DELETE_COURIER_SERVICE_COMMAND;

@Log4j
@Bean(nameCommand = DELETE_COURIER_SERVICE_COMMAND)
@AllArgsConstructor
public class DeleteCourierServiceCommand implements ServletCommand {

    private CourierService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                String courierServiceId = req.getParameter("courierServiceId");
                String courierServiceTransportId = req.getParameter("courierServiceTransportId");

                CourierDto courierDto = new CourierDto();
                Transport transport = new Transport();
                Region region = new Region();
                region.setId(Long.parseLong(courierServiceId));
                List<Region> regionList = new ArrayList<>();
                regionList.add(region);
                transport.setId(Long.parseLong(courierServiceTransportId));
                courierDto.setId(Long.parseLong(courierServiceId));
                courierDto.setTransport(transport);
                courierDto.setRegions(regionList);

                service.deleteCourierService(courierDto);

                resp.sendRedirect(req.getContextPath() + "/courierServiceList");
            }
        } catch (IOException | CourierServiceException e) {
            log.error("Error during delete courier services!", e);
            throw new CommandException("Error during delete courier services!", e);
        }
    }
}
