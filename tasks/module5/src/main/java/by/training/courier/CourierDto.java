package by.training.courier;

import by.training.entity.CourierServiceStatus;
import by.training.entity.Region;
import by.training.entity.Transport;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalTime;
import java.util.List;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class CourierDto {

    private long id;
    private LocalTime startTime;
    private LocalTime endTime;
    private Transport transport;
    private CourierServiceStatus serviceStatus;
    private long userAccountId;
    private String aboutMyself;
    private String mobilePhone;
    private List<Region> regions;
}
