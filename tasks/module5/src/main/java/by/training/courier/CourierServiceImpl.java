package by.training.courier;

import by.training.core.Bean;
import by.training.dao.DaoException;
import by.training.dao.TransactionManager;
import by.training.entity.Message;
import by.training.entity.Region;
import by.training.entity.Transport;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.util.List;

@Log4j
@Bean
@AllArgsConstructor
public class CourierServiceImpl implements CourierService {

    private TransactionManager transactionManager;
    private CourierDao courierDao;
    private RegionDao regionDao;
    private TransportDao transportDao;
    private MessageDao messageDao;

    @Override
    public boolean saveCourierService(CourierDto courierDto) throws CourierServiceException {
        try {
            transactionManager.beginTransaction();
            transportDao.save(courierDto.getTransport());
            Long courierServiceId = courierDao.save(courierDto);
            for (Region region : courierDto.getRegions()) {
                region.setCourierServiceId(courierServiceId);
                regionDao.save(region);
            }
            transactionManager.commitTransaction();
            return true;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during save new courier service from DB!", e);
            throw new CourierServiceException("Error during save new courier service from DB!", e);
        }

    }

    @Override
    public boolean deleteCourierService(CourierDto courierDto) throws CourierServiceException {
        try {
            transactionManager.beginTransaction();
            regionDao.delete(courierDto.getRegions().get(0));
            courierDao.delete(courierDto);
            transportDao.delete(courierDto.getTransport());
            transactionManager.commitTransaction();
            return true;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during delete courier service from DB!", e);
            throw new CourierServiceException("Error during delete courier service from DB!", e);
        }
    }

    @Override
    public CourierDto getCourierServiceById(long id) throws CourierServiceException {
        try {
            transactionManager.beginTransaction();
            CourierDto courierDto = courierDao.getById(id);
            Transport transport = transportDao.getById(courierDto.getTransport().getId());
            courierDto.setTransport(transport);
            List<Region> allRegionsForOneCourierServiceById = regionDao.getAllRegionsForOneCourierServiceById(id);
            courierDto.setRegions(allRegionsForOneCourierServiceById);
            transactionManager.commitTransaction();
            return courierDto;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during get courier service from DB!", e);
            throw new CourierServiceException("Error during get courier service from DB!", e);
        }
    }

    @Override
    public boolean editCourierServiceInfo(CourierDto courierDto) throws CourierServiceException {
        try {
            transactionManager.beginTransaction();
            courierDao.update(courierDto);
            transportDao.update(courierDto.getTransport());
            transactionManager.commitTransaction();
            return true;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during edit courier service from DB!", e);
            throw new CourierServiceException("Error during edit courier service from DB!", e);
        }
    }

    @Override
    public int getCountPage(int id) throws CourierServiceException {
        try {
            return this.courierDao.getCountPage(id);
        } catch (DaoException e) {
            log.error("Error during get count page!", e);
            throw new CourierServiceException("Error during get count page!", e);
        }
    }

    @Override
    public int getCountPage() throws CourierServiceException {
        try {
            return this.courierDao.getCountPage();
        } catch (DaoException e) {
            log.error("Error during get count page!", e);
            throw new CourierServiceException("Error during get count page!", e);
        }
    }

    @Override
    public List<CourierDto> getServiceByPage(int page, int id) throws CourierServiceException {
        try {
            transactionManager.beginTransaction();
            List<CourierDto> serviceByPage = this.courierDao.getServiceByPage(page, id);
            for (CourierDto courierDto : serviceByPage) {
                Transport byId = transportDao.getById(courierDto.getTransport().getId());
                courierDto.setTransport(byId);
            }
            for (CourierDto courierDto : serviceByPage) {
                List<Region> allRegionsForOneCourierServiceById = regionDao.getAllRegionsForOneCourierServiceById(courierDto.getId());
                courierDto.setRegions(allRegionsForOneCourierServiceById);
            }
            transactionManager.commitTransaction();
            return serviceByPage;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during get services by page!", e);
            throw new CourierServiceException("Error during get services by page!", e);
        }
    }

    @Override
    public List<CourierDto> getServiceByPage(int page) throws CourierServiceException {
        try {
            transactionManager.beginTransaction();
            List<CourierDto> serviceByPage = this.courierDao.getServiceByPage(page);
            for (CourierDto courierDto : serviceByPage) {
                Transport byId = transportDao.getById(courierDto.getTransport().getId());
                courierDto.setTransport(byId);
            }
            for (CourierDto courierDto : serviceByPage) {
                List<Region> allRegionsForOneCourierServiceById = regionDao.getAllRegionsForOneCourierServiceById(courierDto.getId());
                courierDto.setRegions(allRegionsForOneCourierServiceById);
            }
            transactionManager.commitTransaction();
            return serviceByPage;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during get services by page!", e);
            throw new CourierServiceException("Error during get services by page!", e);
        }
    }

    @Override
    public boolean editCourierServiceStatus(CourierDto courierDto) throws CourierServiceException {
        try {
            return this.courierDao.editCourierServiceStatus(courierDto);
        } catch (DaoException e) {
            log.error("Error during update courier service status!", e);
            throw new CourierServiceException("Error during update courier service status!", e);
        }

    }

    @Override
    public List<CourierDto> getAllCourierByWorkRegion(String city, int page) throws CourierServiceException {
        try {
            transactionManager.beginTransaction();
            List<CourierDto> allCourierByWorkRegion = this.courierDao.getAllCourierByWorkRegion(city, page);
            for (CourierDto courierDto : allCourierByWorkRegion) {
                Transport byId = this.transportDao.getById(courierDto.getTransport().getId());
                courierDto.setTransport(byId);
            }
            for (CourierDto courierDto : allCourierByWorkRegion) {
                List<Region> allRegionsForOneCourierServiceCity = this.regionDao.getAllRegionsForOneCourierServiceCity(city, courierDto.getId());
                courierDto.setRegions(allRegionsForOneCourierServiceCity);
            }
            transactionManager.commitTransaction();
            return allCourierByWorkRegion;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during get all courier service status!", e);
            throw new CourierServiceException("Error during get all courier service status!", e);
        }
    }

    @Override
    public int getCountPage(String city) throws CourierServiceException {
        try {
            return this.courierDao.getCountPage(city);
        } catch (DaoException e) {
            log.error("Error during get count page!", e);
            throw new CourierServiceException("Error during get count page!", e);
        }
    }

    @Override
    public void saveMessage(Message message) throws CourierServiceException {
        try {
            this.messageDao.save(message);
        } catch (DaoException e) {
            log.error("Error during get count page!", e);
            throw new CourierServiceException("Error during get count page!", e);
        }
    }
}
