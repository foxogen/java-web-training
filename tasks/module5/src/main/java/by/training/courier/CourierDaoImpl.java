package by.training.courier;

import by.training.core.Bean;
import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.Courier;
import by.training.entity.CourierServiceStatus;
import by.training.entity.Transport;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Log4j
@Bean
@AllArgsConstructor
public class CourierDaoImpl implements CourierDao {

    private static final String INSERT_COURIER = "insert into courier_service(start_working_hour, end_working_time, user_account_id," +
            "type_transport_id, service_status_id, about_myself, mob_phone) values (?,?,?,?,?,?,?)";
    private static final String DELETE_COURIER_SERVICE = "delete from courier_service where id =(?)";
    private static final String SELECT_COURIER_SERVICE_BY_ID = "select * from courier_service where id = (?)";
    private static final String UPDATE_COURIER_SERVICE = "update courier_service set start_working_hour=(?), end_working_time=(?)," +
            " about_myself=(?), mob_phone=(?) where id=(?)";
    private static final String SELECT_COUNT_PAGE = "select count(*) from courier_service";
    private static final String SELECT_COUNT_PAGE_BY_ID = "select count(*) from courier_service where user_account_id =(?)";
    private static final String SELECT_SERVICES_BY_PAGE_BY_ID = "select * from courier_service where user_account_id = (?) order by id offset (?) limit 3";
    private static final String SELECT_SERVICES_BY_PAGE = "select * from courier_service order by id offset (?) limit 5";
    private static final String UPDATE_COURIER_SERVICE_STATUS = "update courier_service set service_status_id = (?) where id = (?)";
    private static final String SELECT_COURIER_SERVICE_BY_WORK_REGION = "select courier_service.id as courier_service_id, start_working_hour, end_working_time, " +
            "user_account_id, type_transport_id, about_myself, mob_phone, service_status_id, courier_regions.id as " +
            "courier_regions_id from courier_service right join courier_regions on courier_service.id = courier_regions.courier_service_id" +
            " where region_name = (?) offset (?) limit 3";
    private static final String SELECT_COUNT_PAGE_BY_CITY = "select count(*) from courier_service right join public.courier_regions on " +
            "courier_service.id = courier_regions.courier_service_id where region_name = (?)";

    private ConnectionManager connectionManager;

    @Override
    public Long save(CourierDto courierDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_COURIER, Statement.RETURN_GENERATED_KEYS);
            Courier courier = fromDto(courierDto);
            int i = 0;
            statement.setObject(++i, courier.getStartTime());
            statement.setObject(++i, courier.getEndTime());
            statement.setInt(++i, Math.toIntExact(courier.getUserAccountId()));
            statement.setInt(++i, Math.toIntExact(courier.getTypeTransportId()));
            statement.setInt(++i, courier.getServiceStatus().ordinal() + 1);
            statement.setString(++i, courier.getAboutMyself());
            statement.setString(++i, courier.getMobilePhone());

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                courier.setId(generatedKeys.getLong(1));
            }
            return courier.getId();
        } catch (SQLException e) {
            log.error("Error during save new courier service from DB!", e);
            throw new DaoException("Error during save new courier service from DB!", e);
        }
    }

    @Override
    public boolean update(CourierDto courierDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_COURIER_SERVICE);
            Courier courier = fromDto(courierDto);
            int i = 0;
            statement.setObject(++i, courier.getStartTime());
            statement.setObject(++i, courier.getEndTime());
            statement.setString(++i, courier.getAboutMyself());
            statement.setString(++i, courier.getMobilePhone());
            statement.setInt(++i, Math.toIntExact(courier.getId()));

            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            log.error("Error during update courier service from DB!", e);
            throw new DaoException("Error during update courier service from DB!", e);
        }
    }

    @Override
    public boolean delete(CourierDto courierDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_COURIER_SERVICE);
            Courier courier = fromDto(courierDto);
            statement.setInt(1, Math.toIntExact(courier.getId()));
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            log.error("Error during delete courier service from DB!", e);
            throw new DaoException("Error during delete courier service from DB!", e);
        }
    }

    @Override
    public CourierDto getById(Long id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COURIER_SERVICE_BY_ID);
            statement.setInt(1, Math.toIntExact(id));

            ResultSet resultSet = statement.executeQuery();
            Courier courier = new Courier();
            while (resultSet.next()) {
                LocalTime startTime = resultSet.getTime("start_working_hour").toLocalTime();
                LocalTime endTime = resultSet.getTime("end_working_time").toLocalTime();
                long typeTransportId = resultSet.getLong("type_transport_id");
                int serviceStatusId = resultSet.getInt("service_status_id");
                String aboutMyself = resultSet.getString("about_myself");
                String workPhone = resultSet.getString("mob_phone");
                long userAccountId = resultSet.getInt("user_account_id");

                CourierServiceStatus status = CourierServiceStatus.values()[serviceStatusId - 1];

                courier.setId(id);
                courier.setUserAccountId(userAccountId);
                courier.setStartTime(startTime);
                courier.setEndTime(endTime);
                courier.setTypeTransportId(typeTransportId);
                courier.setServiceStatus(status);
                courier.setAboutMyself(aboutMyself);
                courier.setMobilePhone(workPhone);
            }
            return fromEntity(courier);
        } catch (SQLException e) {
            log.error("Error during get courier service by id from DB!", e);
            throw new DaoException("Error during get courier service by id from DB!", e);
        }
    }

    @Override
    public int getCountPage(int id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COUNT_PAGE_BY_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            int countPage = 0;
            while (resultSet.next()) {
                countPage = resultSet.getInt(1);
            }
            return countPage;
        } catch (SQLException e) {
            log.error("Error during get count page!", e);
            throw new DaoException("Error during get count page!", e);
        }
    }

    @Override
    public int getCountPage(String city) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COUNT_PAGE_BY_CITY);
            statement.setString(1, city);
            ResultSet resultSet = statement.executeQuery();
            int countPage = 0;
            while (resultSet.next()) {
                countPage = resultSet.getInt(1);
            }
            return countPage;
        } catch (SQLException e) {
            log.error("Error during get count page!", e);
            throw new DaoException("Error during get count page!", e);
        }
    }

    @Override
    public int getCountPage() throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COUNT_PAGE);

            ResultSet resultSet = statement.executeQuery();
            int countPage = 0;
            while (resultSet.next()) {
                countPage = resultSet.getInt(1);
            }
            return countPage;
        } catch (SQLException e) {
            log.error("Error during get count page!", e);
            throw new DaoException("Error during get count page!", e);
        }
    }

    @Override
    public List<CourierDto> getServiceByPage(int page, int id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_SERVICES_BY_PAGE_BY_ID);
            int i = 0;
            statement.setInt(++i, id);
            statement.setInt(++i, page);
            ResultSet resultSet = statement.executeQuery();

            List<CourierDto> list = new ArrayList<>();
            while (resultSet.next()) {
                LocalTime startTime = resultSet.getTime("start_working_hour").toLocalTime();
                LocalTime endTime = resultSet.getTime("end_working_time").toLocalTime();
                long typeTransportId = resultSet.getLong("type_transport_id");
                int serviceStatusId = resultSet.getInt("service_status_id");
                String aboutMyself = resultSet.getString("about_myself");
                String workPhone = resultSet.getString("mob_phone");
                long serviceId = resultSet.getInt("id");

                Courier courier = new Courier();
                CourierServiceStatus status = CourierServiceStatus.values()[serviceStatusId - 1];

                courier.setId(serviceId);
                courier.setUserAccountId(id);
                courier.setStartTime(startTime);
                courier.setEndTime(endTime);
                courier.setTypeTransportId(typeTransportId);
                courier.setServiceStatus(status);
                courier.setAboutMyself(aboutMyself);
                courier.setMobilePhone(workPhone);

                list.add(fromEntity(courier));
            }
            return list;
        } catch (SQLException e) {
            log.error("Error during find all users from DB.", e);
            throw new DaoException("Error during find all users from DB.", e);
        }
    }

    @Override
    public List<CourierDto> getServiceByPage(int page) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_SERVICES_BY_PAGE);
            int i = 0;
            statement.setInt(++i, page);
            ResultSet resultSet = statement.executeQuery();

            List<CourierDto> list = new ArrayList<>();
            while (resultSet.next()) {
                LocalTime startTime = resultSet.getTime("start_working_hour").toLocalTime();
                LocalTime endTime = resultSet.getTime("end_working_time").toLocalTime();
                long typeTransportId = resultSet.getLong("type_transport_id");
                int serviceStatusId = resultSet.getInt("service_status_id");
                String aboutMyself = resultSet.getString("about_myself");
                String workPhone = resultSet.getString("mob_phone");
                long serviceId = resultSet.getInt("id");
                long userAccountId = resultSet.getInt("user_account_id");

                Courier courier = new Courier();
                CourierServiceStatus status = CourierServiceStatus.values()[serviceStatusId - 1];

                courier.setId(serviceId);
                courier.setUserAccountId(userAccountId);
                courier.setStartTime(startTime);
                courier.setEndTime(endTime);
                courier.setTypeTransportId(typeTransportId);
                courier.setServiceStatus(status);
                courier.setAboutMyself(aboutMyself);
                courier.setMobilePhone(workPhone);

                list.add(fromEntity(courier));
            }
            return list;
        } catch (SQLException e) {
            log.error("Error during find all users from DB.", e);
            throw new DaoException("Error during find all users from DB.", e);
        }
    }

    @Override
    public boolean editCourierServiceStatus(CourierDto courierDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_COURIER_SERVICE_STATUS);
            int i = 0;
            statement.setInt(++i, courierDto.getServiceStatus().ordinal() + 1);
            statement.setInt(++i, Math.toIntExact(courierDto.getId()));

            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            log.error("Error during update courier service status from DB!", e);
            throw new DaoException("Error during update courier service status from DB!", e);
        }
    }

    @Override
    public List<CourierDto> getAllCourierByWorkRegion(String city, int page) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COURIER_SERVICE_BY_WORK_REGION);
            int i = 0;
            statement.setString(++i, city);
            statement.setInt(++i, page);
            ResultSet resultSet = statement.executeQuery();

            List<CourierDto> list = new ArrayList<>();
            while (resultSet.next()) {
                LocalTime startTime = resultSet.getTime("start_working_hour").toLocalTime();
                LocalTime endTime = resultSet.getTime("end_working_time").toLocalTime();
                long typeTransportId = resultSet.getLong("type_transport_id");
                int serviceStatusId = resultSet.getInt("service_status_id");
                String aboutMyself = resultSet.getString("about_myself");
                String workPhone = resultSet.getString("mob_phone");
                long serviceId = resultSet.getInt("courier_service_id");
                long userAccountId = resultSet.getInt("user_account_id");
                long regionId = resultSet.getInt("courier_regions_id");


                Courier courier = new Courier();
                CourierServiceStatus status = CourierServiceStatus.values()[serviceStatusId - 1];

                courier.setId(serviceId);
                courier.setUserAccountId(userAccountId);
                courier.setStartTime(startTime);
                courier.setEndTime(endTime);
                courier.setTypeTransportId(typeTransportId);
                courier.setServiceStatus(status);
                courier.setAboutMyself(aboutMyself);
                courier.setMobilePhone(workPhone);
                courier.setCourierRegionId(regionId);

                list.add(fromEntity(courier));
            }
            return list;
        } catch (SQLException e) {
            log.error("Error during find all users from DB.", e);
            throw new DaoException("Error during find all users from DB.", e);
        }
    }

    @Override
    public List<CourierDto> findAll() throws DaoException {
        return null;
    }

    private CourierDto fromEntity(Courier courier) {

        CourierDto dto = new CourierDto();
        dto.setId(courier.getId());
        dto.setUserAccountId(courier.getUserAccountId());
        dto.setStartTime(courier.getStartTime());
        dto.setEndTime(courier.getEndTime());
        dto.setServiceStatus(courier.getServiceStatus());
        dto.setAboutMyself(courier.getAboutMyself());
        dto.setMobilePhone(courier.getMobilePhone());
        Transport transport = new Transport();
        transport.setId(courier.getTypeTransportId());
        dto.setTransport(transport);

        return dto;
    }

    private Courier fromDto(CourierDto courierDto) {

        Courier courier = new Courier();
        courier.setId(courierDto.getId());
        courier.setStartTime(courierDto.getStartTime());
        courier.setEndTime(courierDto.getEndTime());
        courier.setMobilePhone(courierDto.getMobilePhone());
        courier.setAboutMyself(courierDto.getAboutMyself());
        courier.setUserAccountId(courierDto.getUserAccountId());
        courier.setTypeTransportId(courierDto.getTransport().getId());
        courier.setServiceStatus(courierDto.getServiceStatus());

        return courier;
    }
}
