package by.training.courier;

import by.training.dao.CRUDDao;
import by.training.dao.DaoException;
import by.training.entity.Region;

import java.util.List;

public interface RegionDao extends CRUDDao<Region, Long> {
    List<Region> getAllRegionsForOneCourierServiceById(long id) throws DaoException;

    List<Region> getAllRegionsForOneCourierServiceCity(String city, long id) throws DaoException;


}
