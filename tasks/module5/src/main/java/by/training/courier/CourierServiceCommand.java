package by.training.courier;

import by.training.SecurityService;
import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.entity.CourierServiceStatus;
import by.training.entity.Region;
import by.training.entity.Transport;
import by.training.user.UserDto;
import by.training.validate.CourierServiceValidator;
import by.training.validate.ValidationResult;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static by.training.command.ServletCommandType.COURIER_SERVICE_COMMAND;

@Log4j
@Bean(nameCommand = COURIER_SERVICE_COMMAND)
@AllArgsConstructor
public class CourierServiceCommand implements ServletCommand {

    private CourierService service;
    private CourierServiceValidator validator;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                Map<String, String> parsedMap = parseParameter(req);
                ValidationResult validate = validator.validate(parsedMap);

                if (validate.isValid()) {
                    CourierDto courierDto = createCourierServiceFromParameter(req);
                    service.saveCourierService(courierDto);
                    resp.sendRedirect(req.getContextPath() + "/courierServiceList");
                } else {
                    req.setAttribute("checkData", true);
                    req.setAttribute("courierServiceError", validate.getErrors().get("courierServiceError"));
                    req.getRequestDispatcher("/jsp/courierServicePage.jsp").forward(req, resp);
                }
            } else {
                req.getRequestDispatcher("/jsp/courierServicePage.jsp").forward(req, resp);
            }
        } catch (IOException | CourierServiceException | ServletException e) {
            log.error("Error during create new courier service!", e);
            throw new CommandException("Error during create new courier service!", e);
        }
    }

    private Map<String, String> parseParameter(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();
        map.put("startTime", request.getParameter("startTime"));
        map.put("endTime", request.getParameter("endTime"));
        map.put("regions", request.getParameter("regions"));
        map.put("workingPhone", request.getParameter("workingPhone"));
        map.put("transport", request.getParameter("transport"));
        map.put("capacity", request.getParameter("capacity"));
        map.put("cargo", request.getParameter("cargo"));
        map.put("aboutMyself", request.getParameter("aboutMyself"));

        return map;
    }

    private CourierDto createCourierServiceFromParameter(HttpServletRequest req) {
        String startTime = req.getParameter("startTime");
        String endTime = req.getParameter("endTime");
        String regions = req.getParameter("regions");
        String workPhone = req.getParameter("workingPhone");
        String transport = req.getParameter("transport");
        String capacity = req.getParameter("capacity");
        String aboutMyself = req.getParameter("aboutMyself");

        Transport tr = new Transport();
        tr.setTransportName(transport);
        tr.setTransportCapacity(Integer.parseInt(capacity));

        List<Region> regionList = new ArrayList<>();
        String[] split = regions.split(", ");
        for (String s : split) {
            Region region = new Region();
            region.setNameRegion(s);
            regionList.add(region);
        }

        HttpSession session = req.getSession();

        UserDto loginUser = SecurityService.getInstance().getCurrentUser(session);

        CourierDto courierDto = new CourierDto();
        courierDto.setUserAccountId(loginUser.getId());
        courierDto.setServiceStatus(CourierServiceStatus.SUBMITTED);
        String[] stTime = startTime.split(":");
        courierDto.setStartTime(LocalTime.of(Integer.parseInt(stTime[0]), Integer.parseInt(stTime[1]), 0));
        String[] enTime = endTime.split(":");
        courierDto.setEndTime(LocalTime.of(Integer.parseInt(enTime[0]), Integer.parseInt(enTime[1]), 0));
        courierDto.setMobilePhone(workPhone);
        courierDto.setRegions(regionList);
        courierDto.setTransport(tr);
        courierDto.setAboutMyself(aboutMyself);

        return courierDto;
    }
}
