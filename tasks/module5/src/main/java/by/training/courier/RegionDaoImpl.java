package by.training.courier;

import by.training.core.Bean;
import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.Region;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Log4j
@Bean
@AllArgsConstructor
public class RegionDaoImpl implements RegionDao {

    private static final String INSERT_REGION = "insert into courier_regions(region_name, courier_service_id) values (?,?)";
    private static final String SELECT_ALL_REGIONS_BY_ID = "select * from courier_regions where courier_service_id = (?)";
    private static final String DELETE_REGIONS = "delete from courier_regions where courier_service_id = (?)";
    private static final String SELECT_REGIONS_BY_CITY_AND_ID = "select * from courier_regions where region_name = (?) and courier_service_id = (?)";

    private ConnectionManager connectionManager;

    @Override
    public Long save(Region entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_REGION, Statement.RETURN_GENERATED_KEYS);

            int i = 0;
            statement.setString(++i, entity.getNameRegion());
            statement.setInt(++i, Math.toIntExact(entity.getCourierServiceId()));

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
            return entity.getId();
        } catch (SQLException e) {
            log.error("Error during save regions for courier service from DB!", e);
            throw new DaoException("Error during save regions for courier service from DB!", e);
        }
    }

    @Override
    public List<Region> getAllRegionsForOneCourierServiceById(long id) throws DaoException {
        List<Region> regionList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_REGIONS_BY_ID);
            statement.setInt(1, Math.toIntExact(id));

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Region region = new Region();
                region.setId(resultSet.getLong("id"));
                region.setNameRegion(resultSet.getString("region_name"));
                region.setCourierServiceId(id);

                regionList.add(region);
            }
            return regionList;
        } catch (SQLException e) {
            log.error("Error during get all regions for courier service from DB!", e);
            throw new DaoException("Error during get all regions for courier service from DB!", e);
        }
    }

    @Override
    public List<Region> getAllRegionsForOneCourierServiceCity(String city, long id) throws DaoException {
        List<Region> regionList = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_REGIONS_BY_CITY_AND_ID);
            int i = 0;
            statement.setString(++i, city);
            statement.setInt(++i, Math.toIntExact(id));

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Region region = new Region();
                region.setId(resultSet.getLong("id"));
                region.setNameRegion(city);
                region.setCourierServiceId(id);

                regionList.add(region);
            }
            return regionList;
        } catch (SQLException e) {
            log.error("Error during get all regions for courier service from DB!", e);
            throw new DaoException("Error during get all regions for courier service from DB!", e);
        }
    }

    @Override
    public boolean update(Region entity) throws DaoException {
        return false;
    }

    @Override
    public boolean delete(Region entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_REGIONS);
            statement.setInt(1, Math.toIntExact(entity.getId()));
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            log.error("Error during delete regions for courier service from DB!", e);
            throw new DaoException("Error during delete regions for courier service from DB!", e);
        }
    }

    @Override
    public Region getById(Long id) throws DaoException {
        return null;
    }

    @Override
    public List<Region> findAll() throws DaoException {
        return null;
    }
}
