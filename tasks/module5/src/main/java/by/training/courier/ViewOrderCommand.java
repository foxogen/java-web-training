package by.training.courier;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.order.OrderDto;
import by.training.order.OrderService;
import by.training.order.OrderServiceException;
import by.training.util.ValidatorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.training.command.ServletCommandType.VIEW_ORDER_COMMAND;

@Log4j
@Bean(nameCommand = VIEW_ORDER_COMMAND)
@AllArgsConstructor
public class ViewOrderCommand implements ServletCommand {

    private OrderService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            int countPage = service.getCountPage();
            int countPossibleCourierForOnePage = 3;
            int page;
            String currentPage = req.getParameter("page");
            if (ValidatorUtil.isDigit(currentPage)) {
                page = Integer.parseInt(currentPage);
            } else {
                page = 1;
                req.setAttribute("currentPage", 1);
            }
            int noOfPages = (int) Math.ceil(countPage * 1.0 / countPossibleCourierForOnePage);
            if (page < 1 || page > noOfPages) {
                req.setAttribute("currentPage", 1);
                page = 1;
            }

            List<OrderDto> allOrders = service.getOrderByPage((page - 1) * countPossibleCourierForOnePage);
            req.setAttribute("allOrders", allOrders);
            req.setAttribute("page", page);
            req.setAttribute("lastPage", noOfPages);

            req.getRequestDispatcher("/jsp/viewOrdersPage.jsp").forward(req, resp);
        } catch (OrderServiceException | ServletException | IOException e) {
            log.error("Error during get approved orders!", e);
            throw new CommandException("Error during get approved orders!", e);
        }
    }
}
