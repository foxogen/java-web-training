package by.training.courier;

public class CourierServiceException extends Exception {
    public CourierServiceException(String message) {
        super(message);
    }

    public CourierServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
