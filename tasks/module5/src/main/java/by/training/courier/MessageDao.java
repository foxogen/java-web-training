package by.training.courier;

import by.training.dao.CRUDDao;
import by.training.entity.Message;

public interface MessageDao extends CRUDDao<Message, Long> {
}
