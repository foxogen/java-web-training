package by.training.courier;

import by.training.dao.CRUDDao;
import by.training.entity.Transport;

public interface TransportDao extends CRUDDao<Transport,Long> {
}
