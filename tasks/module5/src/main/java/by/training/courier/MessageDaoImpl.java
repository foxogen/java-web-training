package by.training.courier;

import by.training.core.Bean;
import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.Message;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Bean
@AllArgsConstructor
@Log4j
public class MessageDaoImpl implements MessageDao {

    private static final String INSERT_MESSAGE = "insert into courier_message(message, courier_service_id) values(?,?)";

    private ConnectionManager connectionManager;

    @Override
    public Long save(Message entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_MESSAGE);
            int i = 0;
            statement.setString(++i, entity.getMessage());
            statement.setInt(++i, Math.toIntExact(entity.getCourierServiceId()));

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
            return entity.getId();
        } catch (SQLException e) {
            log.error("Error during save message in DB!", e);
            throw new DaoException("Error during save message in DB!", e);
        }
    }

    @Override
    public boolean update(Message entity) throws DaoException {
        return false;
    }

    @Override
    public boolean delete(Message entity) throws DaoException {
        return false;
    }

    @Override
    public Message getById(Long id) throws DaoException {
        return null;
    }

    @Override
    public List<Message> findAll() throws DaoException {
        return null;
    }
}
