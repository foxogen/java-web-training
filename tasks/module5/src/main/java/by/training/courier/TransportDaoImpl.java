package by.training.courier;

import by.training.core.Bean;
import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.Transport;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.util.List;

@Log4j
@Bean
@AllArgsConstructor
public class TransportDaoImpl implements TransportDao {

    private static final String INSERT_TRANSPORT = "insert into type_transport(transport_name, capacity) values(?,?)";
    private static final String SELECT_TRANSPORT_BY_ID = "select * from type_transport where id = (?)";
    private static final String DELETE_TRANSPORT = "delete from type_transport where id = (?)";
    private static final String UPDATE_TRANSPORT = "update type_transport set transport_name =(?), capacity=(?) where id =(?)";

    private ConnectionManager connectionManager;

    @Override
    public Long save(Transport entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_TRANSPORT, Statement.RETURN_GENERATED_KEYS);

            int i = 0;
            statement.setString(++i, entity.getTransportName());
            statement.setInt(++i, entity.getTransportCapacity());

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                entity.setId(generatedKeys.getLong(1));
            }
            return entity.getId();
        } catch (SQLException e) {
            log.error("Error during save transport for courier service from DB!", e);
            throw new DaoException("Error during save transport for courier service from DB!", e);
        }
    }

    @Override
    public boolean update(Transport entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_TRANSPORT);
            int i = 0;
            statement.setString(++i, entity.getTransportName());
            statement.setInt(++i, entity.getTransportCapacity());
            statement.setInt(++i, Math.toIntExact(entity.getId()));

            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            log.error("Error during update transport for courier service from DB!", e);
            throw new DaoException("Error during update transport for courier service from DB!", e);
        }
    }

    @Override
    public boolean delete(Transport entity) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_TRANSPORT);
            statement.setInt(1, Math.toIntExact(entity.getId()));
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            log.error("Error during delete transport for courier service from DB!", e);
            throw new DaoException("Error during delete transport for courier service from DB!", e);
        }
    }

    @Override
    public Transport getById(Long id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_TRANSPORT_BY_ID);
            statement.setInt(1, Math.toIntExact(id));

            ResultSet resultSet = statement.executeQuery();
            Transport transport = new Transport();
            while (resultSet.next()) {
                transport.setId(id);
                transport.setTransportName(resultSet.getString("transport_name"));
                transport.setTransportCapacity(resultSet.getInt("capacity"));
            }
            return transport;
        } catch (SQLException e) {
            log.error("Error during get transport for courier service from DB!", e);
            throw new DaoException("Error during get transport for courier service from DB!", e);
        }
    }

    @Override
    public List<Transport> findAll() throws DaoException {
        return null;
    }
}
