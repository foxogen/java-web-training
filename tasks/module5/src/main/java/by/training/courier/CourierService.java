package by.training.courier;

import by.training.entity.Message;

import java.util.List;

public interface CourierService {
    boolean saveCourierService(CourierDto courierDto) throws CourierServiceException;

    boolean deleteCourierService(CourierDto courierDto) throws CourierServiceException;

    CourierDto getCourierServiceById(long id) throws CourierServiceException;

    boolean editCourierServiceInfo(CourierDto courierDto) throws CourierServiceException;

    int getCountPage(int id) throws CourierServiceException;

    int getCountPage() throws CourierServiceException;

    int getCountPage(String city) throws CourierServiceException;

    List<CourierDto> getServiceByPage(int page, int id) throws CourierServiceException;

    List<CourierDto> getServiceByPage(int page) throws CourierServiceException;

    boolean editCourierServiceStatus(CourierDto courierDto) throws CourierServiceException;

    List<CourierDto> getAllCourierByWorkRegion(String city, int page) throws CourierServiceException;

    void saveMessage(Message message) throws CourierServiceException;
}
