package by.training.util;

import org.apache.commons.codec.digest.DigestUtils;

public class EncoderUtil {
    public static String md5Apache(String st) {
        return DigestUtils.md5Hex(st);
    }
}
