package by.training;

import by.training.entity.UserRole;
import by.training.user.UserDto;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpSession;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Log4j
public final class SecurityService {
    private final static AtomicBoolean INITIALIZED = new AtomicBoolean(false);
    private final static Lock INITIALIZE_LOCK = new ReentrantLock();
    private static SecurityService INSTANCE;
    private ConcurrentHashMap<HttpSession, UserDto> sessionUserMap = new ConcurrentHashMap<>();

    private SecurityService() {
        if (INSTANCE != null) {
            throw new IllegalStateException("Instance was created");
        }
    }

    public static SecurityService getInstance() {
        if (!INITIALIZED.get()) {
            INITIALIZE_LOCK.lock();
            try {
                if (INSTANCE == null) {
                    INSTANCE = new SecurityService();
                    INITIALIZED.set(true);
                }
            } finally {
                INITIALIZE_LOCK.unlock();
            }
        }
        return INSTANCE;
    }

    public void createSession(HttpSession session, UserDto user) {
        sessionUserMap.put(session, user);
        log.info("Session was put to poll");
    }

    public boolean deleteSession(HttpSession session) {
        if (sessionUserMap.containsKey(session)) {
            sessionUserMap.remove(session);
            session.invalidate();
            log.info("Session was deleted from poll");
            return true;
        } else {
            return false;
        }
    }

    public UserDto getCurrentUser(HttpSession session) {
        return sessionUserMap.get(session);
    }

    public UserRole getCurrentRoles(HttpSession session) {
        UserDto user = getCurrentUser(session);
        return user.getUserRole();
    }

    public boolean isLogIn(HttpSession session) {
        return sessionUserMap.containsKey(session);
    }

    public boolean containRole(HttpSession session, UserRole role) {
        if (sessionUserMap.containsKey(session)) {
            UserRole currentRoles = getCurrentRoles(session);
            return currentRoles.equals(role);
        } else {
            return false;
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return INSTANCE;
    }
}
