package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.user.UserDto;
import by.training.user.UserService;
import by.training.user.UserServiceException;
import by.training.util.ValidatorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.training.command.ServletCommandType.VIEW_ALL_USER_COMMAND;

@Log4j
@Bean(nameCommand = VIEW_ALL_USER_COMMAND)
@AllArgsConstructor
public class ViewAllUserCommand implements ServletCommand {

    private UserService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            int countPage = service.getCountPage();
            int countUserForOnePage = 3;
            int page;
            String currentPage = req.getParameter("page");
            if (ValidatorUtil.isDigit(currentPage)) {
                page = Integer.parseInt(currentPage);
            } else {
                page = 1;
                req.setAttribute("currentPage", 1);
            }
            int noOfPages = (int) Math.ceil(countPage * 1.0 / countUserForOnePage);
            if (page < 1 || page > noOfPages) {
                req.setAttribute("currentPage", 1);
                page = 1;
            }
            List<UserDto> allUsers = service.getUserByPage((page - 1) * countUserForOnePage);
            req.setAttribute("page", page);
            req.setAttribute("lastPage", noOfPages);
            req.setAttribute("allUser", allUsers);
            req.getRequestDispatcher("/jsp/viewAllUserPage.jsp").forward(req, resp);

        } catch (IOException | UserServiceException | ServletException e) {
            log.error("Error during get all users!", e);
            throw new CommandException("Error during get all users!", e);
        }
    }
}
