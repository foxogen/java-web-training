package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.order.OrderDto;
import by.training.order.OrderService;
import by.training.order.OrderServiceException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.DELETE_USER_ORDER_COMMAND;

@Log4j
@Bean(nameCommand = DELETE_USER_ORDER_COMMAND)
@AllArgsConstructor
public class DeleteUserOrderCommand implements ServletCommand {

    private OrderService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                String orderId = req.getParameter("orderId");

                OrderDto orderDto = new OrderDto();
                orderDto.setId(Long.parseLong(orderId));

                service.deleteOrder(orderDto);

                String page = req.getParameter("page");
                resp.sendRedirect(req.getContextPath() + "/viewAllOrderUser?page=" + page);
            }
        } catch (OrderServiceException | IOException e) {
            log.error("Error during delete user order!", e);
            throw new CommandException("Error during delete user order!", e);
        }
    }
}
