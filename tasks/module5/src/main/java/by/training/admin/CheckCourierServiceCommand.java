package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.courier.CourierDto;
import by.training.courier.CourierService;
import by.training.courier.CourierServiceException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.CHECK_COURIER_SERVICE_COMMAND;

@Log4j
@Bean(nameCommand = CHECK_COURIER_SERVICE_COMMAND)
@AllArgsConstructor
public class CheckCourierServiceCommand implements ServletCommand {

    private CourierService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            String serviceId = req.getParameter("serviceId");
            String page = req.getParameter("page");

            CourierDto courierServiceById = null;
            if (serviceId != null) {
                courierServiceById = service.getCourierServiceById(Long.parseLong(serviceId));
            }
            req.setAttribute("page", page);
            req.setAttribute("serviceId", serviceId);
            req.setAttribute("courierService", courierServiceById);
            req.getRequestDispatcher("/jsp/checkCourierServicePage.jsp").forward(req, resp);
        } catch (ServletException | CourierServiceException | IOException e) {
            log.error("Error during check courier service!", e);
            throw new CommandException("Error during check courier service!", e);
        }
    }
}
