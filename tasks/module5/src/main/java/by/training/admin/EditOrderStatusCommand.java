package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.entity.OrderStatus;
import by.training.order.OrderDto;
import by.training.order.OrderService;
import by.training.order.OrderServiceException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static by.training.command.ServletCommandType.EDIT_STATUS_USER_ORDER_COMMAND;

@Log4j
@Bean(nameCommand = EDIT_STATUS_USER_ORDER_COMMAND)
@AllArgsConstructor
public class EditOrderStatusCommand implements ServletCommand {

    private OrderService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            String status = req.getParameter("status");
            Optional<OrderStatus> orderStatus = OrderStatus.fromString(status);

            if (!orderStatus.isPresent()) {
                log.error("This status invalid!");
                req.getRequestDispatcher("/jsp/viewAllOrdersPage.jsp").forward(req, resp);
                return;
            }

            OrderStatus finalStatus = orderStatus.get();
            String orderId = req.getParameter("orderId");

            OrderDto orderDto = new OrderDto();
            orderDto.setId(Long.parseLong(orderId));
            orderDto.setStatus(finalStatus);

            service.editOrderStatus(orderDto);

            resp.sendRedirect(req.getContextPath() + "/viewAllOrderUser?page=1");

        } catch (ServletException | IOException | OrderServiceException e) {
            log.error("Error during edit order status!", e);
            throw new CommandException("Error during edit order status!", e);
        }
    }
}
