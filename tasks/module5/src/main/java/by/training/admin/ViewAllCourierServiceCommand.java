package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.courier.CourierDto;
import by.training.courier.CourierService;
import by.training.courier.CourierServiceException;
import by.training.util.ValidatorUtil;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static by.training.command.ServletCommandType.VIEW_ALL_COURIER_SERVICE_COMMAND;

@Log4j
@Bean(nameCommand = VIEW_ALL_COURIER_SERVICE_COMMAND)
@AllArgsConstructor
public class ViewAllCourierServiceCommand implements ServletCommand {

    private CourierService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            int countPage = service.getCountPage();
            int countCourierServiceForOnePage = 5;
            int page;
            String currentPage = req.getParameter("page");
            if (ValidatorUtil.isDigit(currentPage)) {
                page = Integer.parseInt(currentPage);
            } else {
                page = 1;
                req.setAttribute("currentPage", 1);
            }
            int noOfPages = (int) Math.ceil(countPage * 1.0 / countCourierServiceForOnePage);
            if (page < 1 || page > noOfPages) {
                req.setAttribute("currentPage", 1);
                page = 1;
            }
            List<CourierDto> allService = service.getServiceByPage((page - 1) * countCourierServiceForOnePage);
            req.setAttribute("page", page);
            req.setAttribute("lastPage", noOfPages);
            req.setAttribute("allService", allService);
            req.getRequestDispatcher("/jsp/viewAllCourierServicePage.jsp").forward(req, resp);

        } catch (IOException | CourierServiceException | ServletException e) {
            log.error("Error during get all courier services!", e);
            throw new CommandException("Error during get all courier services!", e);
        }
    }
}

