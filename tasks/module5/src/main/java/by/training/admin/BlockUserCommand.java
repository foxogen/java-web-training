package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.user.UserDto;
import by.training.user.UserService;
import by.training.user.UserServiceException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.BLOCK_USER_COMMAND;

@Bean(nameCommand = BLOCK_USER_COMMAND)
@AllArgsConstructor
@Log4j
public class BlockUserCommand implements ServletCommand {

    private UserService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                String userId = req.getParameter("userId");
                String block = req.getParameter("block");

                UserDto userDto = new UserDto();
                userDto.setId(Long.parseLong(userId));
                if (Integer.parseInt(block) == 0) {
                    userDto.setBlock(false);
                } else {
                    userDto.setBlock(true);
                }
                service.blockOrUnblockUser(userDto);
                String page = req.getParameter("page");
                resp.sendRedirect(req.getContextPath() + "/viewAllUser?page=" + page);
            }
        } catch (IOException | UserServiceException e) {
            log.error("Error during block or unblock user!", e);
            throw new CommandException("Error during block or unblock user!", e);
        }
    }
}
