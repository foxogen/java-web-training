package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.entity.UserRole;
import by.training.user.UserDto;
import by.training.user.UserService;
import by.training.user.UserServiceException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static by.training.command.ServletCommandType.DELETE_USER_COMMAND;

@Log4j
@Bean(nameCommand = DELETE_USER_COMMAND)
@AllArgsConstructor
public class DeleteUserCommand implements ServletCommand {

    private UserService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                String userId = req.getParameter("userId");
                String userRole = req.getParameter("userRole");
                String page = req.getParameter("page");

                Optional<UserRole> role = UserRole.fromString(userRole);
                if (!role.isPresent()) {
                    resp.sendRedirect(req.getContextPath() + "/viewAllUser?page=" + page);
                    return;
                }

                UserRole uRole = role.get();

                if (uRole.equals(UserRole.ADMIN)) {
                    resp.sendRedirect(req.getContextPath() + "/viewAllUser?page=" + page);
                    return;
                }

                UserDto userDto = new UserDto();
                userDto.setId(Long.parseLong(userId));
                service.deleteUser(userDto);

                resp.sendRedirect(req.getContextPath() + "/viewAllUser?page=" + page);
            }
        } catch (IOException | UserServiceException e) {
            log.error("Error during delete user!", e);
            throw new CommandException("Error during delete user!", e);
        }
    }
}
