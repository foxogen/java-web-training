package by.training.admin;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.courier.CourierDto;
import by.training.courier.CourierService;
import by.training.courier.CourierServiceException;
import by.training.entity.CourierServiceStatus;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static by.training.command.ServletCommandType.EDIT_STATUS_COURIER_SERVICE_COMMAND;


@Log4j
@Bean(nameCommand = EDIT_STATUS_COURIER_SERVICE_COMMAND)
@AllArgsConstructor
public class EditStatusCourierServiceCommand implements ServletCommand {

    private CourierService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                String status = req.getParameter("status");

                Optional<CourierServiceStatus> courierServiceStatus = CourierServiceStatus.fromString(status);

                if (!courierServiceStatus.isPresent()) {
                    log.error("This status invalid!");
                    req.getRequestDispatcher("/jsp/viewAllCourierServicePage.jsp").forward(req, resp);
                    return;
                }

                CourierServiceStatus finalStatus = courierServiceStatus.get();
                String courierServiceId = req.getParameter("courierServiceId");

                CourierDto courierDto = new CourierDto();
                courierDto.setId(Long.parseLong(courierServiceId));
                courierDto.setServiceStatus(finalStatus);

                service.editCourierServiceStatus(courierDto);

                String page = req.getParameter("page");
                resp.sendRedirect(req.getContextPath() + "/viewAllCourierService?page=" + page);
            }
        } catch (ServletException | CourierServiceException | IOException e) {
            log.error("Error during update courier service status!", e);
            throw new CommandException("Error during update courier service status!", e);
        }
    }
}
