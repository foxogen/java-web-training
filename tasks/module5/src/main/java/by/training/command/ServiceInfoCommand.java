package by.training.command;

import by.training.core.Bean;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.SERVICE_INFO_COMMAND;

@Log4j
@Bean(nameCommand = SERVICE_INFO_COMMAND)
public class ServiceInfoCommand implements ServletCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException{
        try {
            req.getRequestDispatcher("/jsp/servicePageInfo.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            log.error("Error during forward on service info!", e);
            throw new CommandException("Error during forward on service info!", e);
        }
    }
}
