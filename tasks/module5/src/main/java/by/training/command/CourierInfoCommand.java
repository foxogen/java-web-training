package by.training.command;

import by.training.core.Bean;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.COURIER_INFO_COMMAND;

@Log4j
@Bean(nameCommand = COURIER_INFO_COMMAND)
public class CourierInfoCommand implements ServletCommand {
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException{
        try {
            req.getRequestDispatcher("/jsp/courierPageInfo.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            log.error("Error during forward on courier info!", e);
            throw new CommandException("Error during forward on courier info!", e);
        }
    }
}
