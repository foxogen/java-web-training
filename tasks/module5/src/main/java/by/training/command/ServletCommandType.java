package by.training.command;

public enum ServletCommandType {
    DEFAULT_COMMAND(""),
    REGISTER_USER_COMMAND("registerUser"),
    SERVICE_INFO_COMMAND("serviceInfo"),
    ERROR_COMMAND("error"),
    CONSUMER_INFO_COMMAND("consumerInfo"),
    COURIER_INFO_COMMAND("courierInfo"),
    USER_INFO_COMMAND("userInfo"),
    EDIT_PROFILE_COMMAND("userInfo/edit"),
    LOGIN_USER_COMMAND("loginUser"),
    LOGOUT_USER_COMMAND("logoutUser"),
    COURIER_SERVICE_COMMAND("courierService"),
    COURIER_SERVICE_LIST_COMMAND("courierServiceList"),
    DELETE_COURIER_SERVICE_COMMAND("courierServiceList/deleteService"),
    EDIT_COURIER_SERVICE_COMMAND("courierServiceList/edit"),
    VIEW_ALL_USER_COMMAND("viewAllUser"),
    DELETE_USER_COMMAND("viewAllUser/deleteUser"),
    BLOCK_USER_COMMAND("viewAllUser/blockUser"),
    VIEW_ALL_COURIER_SERVICE_COMMAND("viewAllCourierService"),
    CHECK_COURIER_SERVICE_COMMAND("viewAllCourierService/checkService"),
    DELETE_COURIER_SERVICE_ADMIN_COMMAND("viewAllCourierService/checkService/delete"),
    EDIT_STATUS_COURIER_SERVICE_COMMAND("viewAllCourierService/checkService/editStatus"),
    CREATE_ORDER_COMMAND("createOrder"),
    ORDER_LIST_COMMAND("ordersList"),
    VIEW_ORDER_COMMAND("viewOrderList"),
    POSSIBLE_COURIER_LIST_COMMAND("possibleCouriersList"),
    CONTACT_WITH_COURIER_COMMAND("possibleCouriersList/contact"),
    DELETE_ORDER_COMMAND("ordersList/deleteOrder"),
    EDIT_ORDER_COMMAND("ordersList/editOrder"),
    VIEW_ALL_USER_ORDER_COMMAND("viewAllOrderUser"),
    DELETE_USER_ORDER_COMMAND("viewAllOrderUser/delete"),
    EDIT_STATUS_USER_ORDER_COMMAND("viewAllOrderUser/editStatus");

    private String name;

    ServletCommandType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
