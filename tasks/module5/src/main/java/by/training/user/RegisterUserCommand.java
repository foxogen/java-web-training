package by.training.user;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.entity.UserRole;
import by.training.validate.RegisterUserValidator;
import by.training.validate.ValidationResult;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static by.training.command.ServletCommandType.REGISTER_USER_COMMAND;

@Log4j
@AllArgsConstructor
@Bean(nameCommand = REGISTER_USER_COMMAND)
public class RegisterUserCommand implements ServletCommand {

    private UserService service;
    private RegisterUserValidator validator;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                Map<String, String> parsedMap = parseParameter(req);
                ValidationResult validate = validator.validate(parsedMap);

                UserDto userDto = createUserFromParameter(req);

                if (validate.isValid()) {
                    service.saveUser(userDto);
                    resp.sendRedirect(req.getContextPath() + "/loginUser");
                } else {
                    req.setAttribute("checkLogin", true);
                    req.setAttribute("userHas", userDto);
                    req.setAttribute("registrationError", validate.getErrors().get("registrationError"));
                    req.getRequestDispatcher("/jsp/registrationUser.jsp").forward(req, resp);
                }

            } else {
                req.getRequestDispatcher("/jsp/registrationUser.jsp").forward(req, resp);
            }
        } catch (UserServiceException | IOException | ServletException e) {
            log.error("Error during registration new user account!", e);
            throw new CommandException("Error during registration new user account!", e);
        }
    }

    private Map<String, String> parseParameter(HttpServletRequest request) {
        Map<String, String> map = new HashMap<>();
        map.put("login", request.getParameter("login"));
        map.put("password", request.getParameter("password1"));
        map.put("firstName", request.getParameter("firstName"));
        map.put("lastName", request.getParameter("lastName"));
        map.put("email", request.getParameter("email"));
        map.put("phone", request.getParameter("phone"));
        map.put("city", request.getParameter("city"));
        map.put("address", request.getParameter("address"));
        map.put("role", request.getParameter("question"));

        return map;
    }

    private UserDto createUserFromParameter(HttpServletRequest req) {
        UserDto userDto = new UserDto();
        String login = req.getParameter("login");
        String password1 = req.getParameter("password1");
        String fName = req.getParameter("firstName");
        String lName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");
        String city = req.getParameter("city");
        String address = req.getParameter("address");
        String question = req.getParameter("question");

        UserRole userRole = UserRole.valueOf(question);

        userDto.setLogin(login);
        userDto.setPassword(password1);
        userDto.setFirstName(fName);
        userDto.setLastName(lName);
        userDto.setEmail(email);
        userDto.setMobilePhone(phone);
        userDto.setCity(city);
        userDto.setAddress(address);
        userDto.setUserRole(userRole);

        return userDto;
    }
}
