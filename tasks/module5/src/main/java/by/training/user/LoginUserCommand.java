package by.training.user;

import by.training.SecurityService;
import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.LOGIN_USER_COMMAND;

@Log4j
@AllArgsConstructor
@Bean(nameCommand = LOGIN_USER_COMMAND)
public class LoginUserCommand implements ServletCommand {

    private UserService service;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                String login = req.getParameter("login");
                String password = req.getParameter("password");

                UserDto user = service.findUserByLoginAndPass(login, password);

                if (user != null) {
                    if (user.isBlock()) {
                        req.setAttribute("error", true);
                        req.setAttribute("errorDuringLogin", "page.error_this_user_block");
                        req.getRequestDispatcher("/jsp/loginPage.jsp").forward(req, resp);
                        return;
                    }
                    SecurityService.getInstance().createSession(req.getSession(true), user);
                    resp.sendRedirect(req.getContextPath());

                } else {
                    req.setAttribute("error", true);
                    req.setAttribute("errorDuringLogin", "page.error_login_or_password");
                    req.getRequestDispatcher("/jsp/loginPage.jsp").forward(req, resp);
                }

            } else {
                req.getRequestDispatcher("/jsp/loginPage.jsp").forward(req, resp);
            }
        } catch (UserServiceException | IOException | ServletException e) {
            log.error("Error during log in user account!", e);
            throw new CommandException("Error during log in user account!", e);
        }
    }
}
