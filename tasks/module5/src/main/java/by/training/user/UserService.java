package by.training.user;

import java.util.List;

public interface UserService {
    boolean findUserByLogin(UserDto userDto) throws UserServiceException;

    UserDto findUserByLoginAndPass(String login, String pass) throws UserServiceException;

    boolean saveUser(UserDto user) throws UserServiceException;

    boolean editUserInfo(UserDto user) throws UserServiceException;

    boolean deleteUser(UserDto userDto) throws UserServiceException;

    boolean blockOrUnblockUser(UserDto userDto) throws UserServiceException;

    int getCountPage() throws UserServiceException;

    List<UserDto> getUserByPage(int page) throws UserServiceException;

    UserDto getCourierForContactUser(long id) throws UserServiceException;
}
