package by.training.user;

import by.training.SecurityService;
import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.LOGOUT_USER_COMMAND;

@Log4j
@Bean(nameCommand = LOGOUT_USER_COMMAND)
@AllArgsConstructor
public class LogoutUserCommand implements ServletCommand {

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            SecurityService.getInstance().deleteSession(req.getSession());
            resp.sendRedirect(req.getContextPath());
        } catch (IOException e) {
            log.error("Error during log out user account!", e);
            throw new CommandException("Error during log out user account!", e);
        }

    }
}
