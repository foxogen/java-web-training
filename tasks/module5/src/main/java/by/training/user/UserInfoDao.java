package by.training.user;

import by.training.dao.CRUDDao;

public interface UserInfoDao extends CRUDDao<UserDto, Long> {
}
