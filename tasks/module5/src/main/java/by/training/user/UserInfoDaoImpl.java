package by.training.user;

import by.training.core.Bean;
import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.UserInfo;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.util.List;

@Log4j
@Bean
@AllArgsConstructor
public class UserInfoDaoImpl implements UserInfoDao {

    private static final String INSERT_USER_INFO = "insert into user_info(id, mob_phone, city, address) values(?,?,?,?)";
    private static final String SELECT_USER_INFO = "select * from user_info where id = (?)";
    private static final String DELETE_USER = "delete from user_info where id = (?)";
    private static final String UPDATE_USER = "update user_info set mob_phone=(?), city=(?), address=(?) where id=(?)";

    private ConnectionManager connectionManager;

    @Override
    public Long save(UserDto user) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_USER_INFO, Statement.RETURN_GENERATED_KEYS);
            UserInfo userInfo = fromDto(user);
            int i = 0;
            statement.setInt(++i, Math.toIntExact(userInfo.getId()));
            statement.setString(++i, userInfo.getMobilePhone());
            statement.setString(++i, userInfo.getCity());
            statement.setString(++i, userInfo.getAddress());

            statement.executeUpdate();
            return userInfo.getId();
        } catch (SQLException e) {
            log.error("Error during save new user info from DB.", e);
            throw new DaoException("Error during save new user info from DB.", e);
        }
    }

    @Override
    public boolean update(UserDto user) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_USER);
            UserInfo userInfo = fromDto(user);
            int i = 0;
            statement.setString(++i, userInfo.getMobilePhone());
            statement.setString(++i, userInfo.getCity());
            statement.setString(++i, userInfo.getAddress());
            statement.setInt(++i, Math.toIntExact(userInfo.getId()));

            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            log.error("Error during update user info from DB.", e);
            throw new DaoException("Error during update user info from DB.", e);
        }
    }

    @Override
    public boolean delete(UserDto user) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_USER);

            UserInfo userInfo = fromDto(user);
            statement.setInt(1, Math.toIntExact(userInfo.getId()));
            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            log.error("Error during delete user info from DB.", e);
            throw new DaoException("Error during delete user info from DB.", e);
        }
    }

    @Override
    public UserDto getById(Long id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_USER_INFO);
            statement.setInt(1, Math.toIntExact(id));

            ResultSet resultSet = statement.executeQuery();

            UserInfo userInfo = new UserInfo();
            while (resultSet.next()) {
                String phone = resultSet.getString("mob_phone");
                String city = resultSet.getString("city");
                String address = resultSet.getString("address");

                userInfo.setId(id);
                userInfo.setMobilePhone(phone);
                userInfo.setCity(city);
                userInfo.setAddress(address);
            }
            return fromEntity(userInfo);
        } catch (SQLException e) {
            log.error("Error during get user info by id from DB.", e);
            throw new DaoException("Error during get user info by id from DB.", e);
        }
    }

    @Override
    public List<UserDto> findAll() throws DaoException {
        return null;
    }

    private UserDto fromEntity(UserInfo userInfo) {

        UserDto dto = new UserDto();
        dto.setId(userInfo.getId());
        dto.setMobilePhone(userInfo.getMobilePhone());
        dto.setCity(userInfo.getCity());
        dto.setAddress(userInfo.getAddress());

        return dto;
    }

    private UserInfo fromDto(UserDto userDto) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(userDto.getId());
        userInfo.setAddress(userDto.getAddress());
        userInfo.setCity(userDto.getCity());
        userInfo.setMobilePhone(userDto.getMobilePhone());
        return userInfo;
    }
}
