package by.training.user;

import by.training.command.CommandException;
import by.training.core.Bean;
import by.training.dao.DaoException;
import by.training.dao.TransactionManager;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import java.util.List;
import java.util.Optional;

@Log4j
@Bean
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private TransactionManager transactionManager;
    private UserDao userDao;
    private UserInfoDao userInfoDao;

    @Override
    public boolean findUserByLogin(UserDto userDto) throws UserServiceException {
        Optional<UserDto> byLogin;
        try {
            byLogin = this.userDao.findByLogin(userDto.getLogin());
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during find user by login from DB.", e);
            throw new UserServiceException("Error during find user by login from DB.", e);
        }
        return byLogin.filter(dto -> dto.getLogin().equals(userDto.getLogin())).isPresent();
    }

    @Override
    public UserDto findUserByLoginAndPass(String login, String pass) throws UserServiceException {
        Optional<UserDto> userByLoginAndPass;
        try {
            transactionManager.beginTransaction();
            userByLoginAndPass = this.userDao.findByLoginAndPass(login, pass);
            if (!userByLoginAndPass.isPresent()) {
                log.info("User not found!");
                return null;
            }
            UserDto userDto = userByLoginAndPass.get();
            UserDto userInfo = this.userInfoDao.getById(userDto.getId());
            if (userDto.getId() == userInfo.getId()) {
                userDto.setMobilePhone(userInfo.getMobilePhone());
                userDto.setCity(userInfo.getCity());
                userDto.setAddress(userInfo.getAddress());
            }
            transactionManager.commitTransaction();
            return userDto;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during find user by login and password from DB.", e);
            throw new UserServiceException("Error during find user by login and password from DB.", e);
        }
    }

    @Override
    public boolean saveUser(UserDto user) throws UserServiceException {
        try {
            transactionManager.beginTransaction();
            this.userDao.save(user);
            this.userInfoDao.save(user);
            transactionManager.commitTransaction();
            return true;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during save new user from DB.", e);
            throw new UserServiceException("Error during save new user from DB.", e);
        }
    }

    @Override
    public boolean deleteUser(UserDto userDto) throws UserServiceException {
        try {
            transactionManager.beginTransaction();
            this.userInfoDao.delete(userDto);
            this.userDao.delete(userDto);
            transactionManager.commitTransaction();
            return true;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during delete user from DB.", e);
            throw new UserServiceException("Error during delete user from DB.", e);
        }
    }

    @Override
    public boolean editUserInfo(UserDto user) throws UserServiceException {
        try {
            transactionManager.beginTransaction();
            this.userDao.update(user);
            this.userInfoDao.update(user);
            transactionManager.commitTransaction();
            return true;
        } catch (DaoException e) {
            transactionManager.rollbackTransaction();
            log.error("Error during update user from DB.", e);
            throw new UserServiceException("Error during update user from DB.", e);
        }
    }

    @Override
    public boolean blockOrUnblockUser(UserDto userDto) throws UserServiceException {
        try {
            this.userDao.blockOrUnblockUser(userDto);
            return true;
        } catch (DaoException e) {
            log.error("Error during block or unblock user from DB.", e);
            throw new UserServiceException("Error during block or unblock user from DB.", e);
        }
    }

    @Override
    public int getCountPage() throws UserServiceException {
        try {
            return this.userDao.getCountPage();
        } catch (DaoException e) {
            log.error("Error during get count pages", e);
            throw new UserServiceException("Error during get count pages", e);
        }
    }

    @Override
    public List<UserDto> getUserByPage(int page) throws UserServiceException {
        try {
            return this.userDao.getUserByPage(page);
        } catch (DaoException e) {
            log.error("Error during get users by page!", e);
            throw new UserServiceException("Error during get users by page!", e);
        }
    }

    @Override
    public UserDto getCourierForContactUser(long id) throws UserServiceException {
        try {
            return this.userDao.getCourierForContactUser(id);
        } catch (DaoException e) {
            log.error("Error during get courier for contact!", e);
            throw new UserServiceException("Error during get courier for contact!", e);
        }
    }
}
