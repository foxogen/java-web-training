package by.training.user;

import by.training.dao.CRUDDao;
import by.training.dao.DaoException;

import java.util.List;
import java.util.Optional;

public interface UserDao extends CRUDDao<UserDto, Long> {

    Optional<UserDto> findByLogin(String login) throws DaoException;

    Optional<UserDto> findByLoginAndPass(String login, String pass) throws DaoException;

    boolean blockOrUnblockUser(UserDto userDto) throws DaoException;

    int getCountPage() throws DaoException;

    List<UserDto> getUserByPage(int page) throws DaoException;

    UserDto getCourierForContactUser(long id) throws DaoException;

}
