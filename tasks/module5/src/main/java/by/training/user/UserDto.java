package by.training.user;

import by.training.entity.UserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class UserDto {

    private long id;
    private String login;
    private String password;
    private UserRole userRole;
    private String firstName;
    private String lastName;
    private String mobilePhone;
    private String email;
    private String city;
    private String address;
    private boolean block;
}
