package by.training.user;

import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static by.training.command.ServletCommandType.USER_INFO_COMMAND;

@Log4j
@Bean(nameCommand = USER_INFO_COMMAND)
public class UserInfoCommand implements ServletCommand {
    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException{
        try {
            req.getRequestDispatcher("/jsp/userInfo.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            log.error("Error during forward user info page!", e);
            throw new CommandException("Error during forward user info page!", e);
        }
    }
}
