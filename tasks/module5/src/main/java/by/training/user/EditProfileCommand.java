package by.training.user;

import by.training.SecurityService;
import by.training.command.CommandException;
import by.training.command.ServletCommand;
import by.training.core.Bean;
import by.training.validate.RegisterUserValidator;
import by.training.validate.ValidationResult;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static by.training.command.ServletCommandType.EDIT_PROFILE_COMMAND;

@Log4j
@Bean(nameCommand = EDIT_PROFILE_COMMAND)
@AllArgsConstructor
public class EditProfileCommand implements ServletCommand {

    private UserService service;
    private RegisterUserValidator validator;

    @Override
    public void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException {
        try {
            if (req.getMethod().equalsIgnoreCase("post")) {
                Map<String, String> parsedMap = parseParameter(req);
                ValidationResult validate = validator.validateForEditProfile(parsedMap);
                if (validate.isValid()) {
                    UserDto userDto = createUserFromParameter(req);

                    service.editUserInfo(userDto);

                    resp.sendRedirect(req.getContextPath() + "/userInfo");
                } else {
                    req.setAttribute("editProfileError", true);
                    req.setAttribute("registrationError", validate.getErrors().get("registrationError"));
                    req.getRequestDispatcher("/jsp/editProfilePage.jsp").forward(req, resp);
                }

            } else {
                req.getRequestDispatcher("/jsp/editProfilePage.jsp").forward(req, resp);
            }
        } catch (ServletException | IOException | UserServiceException e) {
            log.error("Error during edit user info!", e);
            throw new CommandException("Error during edit user info!", e);
        }
    }

    private Map<String, String> parseParameter(HttpServletRequest request) {
        HttpSession session = request.getSession();
        UserDto userDto = SecurityService.getInstance().getCurrentUser(session);
        Map<String, String> map = new HashMap<>();
        map.put("login", userDto.getLogin());
        map.put("password", userDto.getPassword());
        map.put("firstName", request.getParameter("firstName"));
        map.put("lastName", request.getParameter("lastName"));
        map.put("email", request.getParameter("email"));
        map.put("phone", request.getParameter("phone"));
        map.put("city", request.getParameter("city"));
        map.put("address", request.getParameter("address"));

        return map;
    }

    private UserDto createUserFromParameter(HttpServletRequest req) {
        HttpSession session = req.getSession();
        UserDto userDto = SecurityService.getInstance().getCurrentUser(session);
        String fName = req.getParameter("firstName");
        String lName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String phone = req.getParameter("phone");
        String city = req.getParameter("city");
        String address = req.getParameter("address");

        userDto.setFirstName(fName);
        userDto.setLastName(lName);
        userDto.setEmail(email);
        userDto.setMobilePhone(phone);
        userDto.setCity(city);
        userDto.setAddress(address);

        return userDto;
    }
}
