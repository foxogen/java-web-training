package by.training.user;

import by.training.core.Bean;
import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.User;
import by.training.entity.UserRole;
import by.training.util.EncoderUtil;
import lombok.extern.log4j.Log4j;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j
@Bean
public class UserDaoImpl implements UserDao {

    private static final String INSERT_USER = "insert into user_account(login, password, user_role_id, first_name, last_name," +
            " email, block_account) values(?,?,?,?,?,?,?)";
    private static final String SELECT_BY_LOGIN_AND_PASS = "select * from user_account where login = (?) and password =(?)";
    private static final String SELECT_BY_LOGIN = "select login from user_account where login = (?)";
    private static final String DELETE_USER = "delete from user_account where id = (?)";
    private static final String UPDATE_USER = "update user_account set first_name=(?), last_name=(?), email=(?) where id=(?)";
    private static final String GET_ALL_USERS = "select * from user_account right join user_info on user_account.id=user_info.id";
    private static final String BLOCK_USER = "update user_account set block_account = (?) where id =(?)";
    private static final String SELECT_COUNT_PAGE = "select count(*) from user_account";
    private static final String SELECT_USERS_BY_PAGE = "select * from user_account right join user_info on user_account.id = user_info.id order by login offset (?) limit 3";
    private static final String SELECT_COURIER_FOR_CONTACT = "select user_account_id, mob_phone, first_name," +
            " last_name from courier_service right join user_account on" +
            "    courier_service.user_account_id=user_account.id where courier_service.id = (?)";

    private ConnectionManager connectionManager;

    public UserDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public Optional<UserDto> findByLogin(String login) throws DaoException {
        List<User> users = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_BY_LOGIN);
            statement.setString(1, login);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                String loginUse = resultSet.getString("login");

                User user = new User();
                user.setLogin(loginUse);

                users.add(user);
            }
            return users.stream().map(this::fromEntity).findFirst();
        } catch (SQLException e) {
            log.error("Error during find user by login from DB.", e);
            throw new DaoException("Error during find user by login from DB.", e);
        }
    }

    @Override
    public Optional<UserDto> findByLoginAndPass(String login, String pass) throws DaoException {
        List<User> users = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_BY_LOGIN_AND_PASS);
            int i = 0;
            statement.setString(++i, login);
            statement.setString(++i, EncoderUtil.md5Apache(pass));

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                long entityId = resultSet.getLong("id");
                String email = resultSet.getString("email");
                int userRoleId = resultSet.getInt("user_role_id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                int block = resultSet.getInt("block_account");

                UserRole userRole = UserRole.values()[userRoleId - 1];

                User user = new User();
                user.setId(entityId);
                user.setLogin(login);
                user.setPassword(pass);
                user.setRole(userRole);
                user.setEmail(email);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                if (block == 0) {
                    user.setBlock(false);
                } else {
                    user.setBlock(true);
                }

                users.add(user);
            }
            return users.stream().map(this::fromEntity).findFirst();
        } catch (SQLException e) {
            log.error("Error during find user by login and password from DB.", e);
            throw new DaoException("Error during find user by login and password from DB.", e);
        }
    }

    @Override
    public Long save(UserDto user) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            User userEntity = fromDto(user);
            int i = 0;
            statement.setString(++i, userEntity.getLogin());
            statement.setString(++i, EncoderUtil.md5Apache(userEntity.getPassword()));
            statement.setInt(++i, userEntity.getRole().ordinal() + 1);
            statement.setString(++i, userEntity.getFirstName());
            statement.setString(++i, userEntity.getLastName());
            statement.setString(++i, userEntity.getEmail());
            statement.setInt(++i, 0);

            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            while (generatedKeys.next()) {
                user.setId(generatedKeys.getLong(1));
            }
            return user.getId();
        } catch (SQLException e) {
            log.error("Error during save new user from DB.", e);
            throw new DaoException("Error during save new user from DB.", e);
        }
    }

    @Override
    public boolean update(UserDto user) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(UPDATE_USER);
            User userEntity = fromDto(user);
            int i = 0;
            statement.setString(++i, userEntity.getFirstName());
            statement.setString(++i, userEntity.getLastName());
            statement.setString(++i, userEntity.getEmail());
            statement.setInt(++i, Math.toIntExact(userEntity.getId()));

            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            log.error("Error during update user from DB.", e);
            throw new DaoException("Error during update user from DB.", e);
        }
    }

    @Override
    public boolean delete(UserDto user) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(DELETE_USER);
            User userEntity = fromDto(user);

            statement.setInt(1, Math.toIntExact(userEntity.getId()));

            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            log.error("Error during delete user from DB.", e);
            throw new DaoException("Error during delete user from DB.", e);
        }
    }

    @Override
    public UserDto getById(Long id) throws DaoException {
        return null;
    }

    @Override
    public List<UserDto> findAll() throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(GET_ALL_USERS);
            ResultSet resultSet = statement.executeQuery();

            List<UserDto> list = new ArrayList<>();
            while (resultSet.next()) {
                long entityId = resultSet.getLong("id");
                String email = resultSet.getString("email");
                String login = resultSet.getString("login");
                int userRoleId = resultSet.getInt("user_role_id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String phone = resultSet.getString("mob_phone");
                String city = resultSet.getString("city");
                String address = resultSet.getString("address");
                int block = resultSet.getInt("block_account");

                UserRole userRole = UserRole.values()[userRoleId - 1];

                UserDto dto = new UserDto();
                dto.setEmail(email);
                dto.setFirstName(firstName);
                dto.setId(entityId);
                dto.setUserRole(userRole);
                dto.setLastName(lastName);
                dto.setLogin(login);
                dto.setMobilePhone(phone);
                dto.setCity(city);
                dto.setAddress(address);
                if (block == 0) {
                    dto.setBlock(false);
                } else {
                    dto.setBlock(true);
                }

                list.add(dto);
            }
            return list;
        } catch (SQLException e) {
            log.error("Error during find all users from DB.", e);
            throw new DaoException("Error during find all users from DB.", e);
        }
    }

    @Override
    public boolean blockOrUnblockUser(UserDto userDto) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(BLOCK_USER);
            User userEntity = fromDto(userDto);

            int i = 0;
            if (userEntity.isBlock()) {
                statement.setInt(++i, 1);
            } else {
                statement.setInt(++i, 0);
            }
            statement.setInt(++i, Math.toIntExact(userEntity.getId()));

            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            log.error("Error during block or unblock user from DB.", e);
            throw new DaoException("Error during block or unblock user from DB.", e);
        }
    }

    @Override
    public int getCountPage() throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COUNT_PAGE);
            ResultSet resultSet = statement.executeQuery();
            int countPage = 0;
            while (resultSet.next()) {
                countPage = resultSet.getInt(1);
            }
            return countPage;
        } catch (SQLException e) {
            log.error("Error during get count pages", e);
            throw new DaoException("Error during get count pages", e);
        }

    }

    @Override
    public List<UserDto> getUserByPage(int page) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_USERS_BY_PAGE);
            statement.setInt(1, page);
            ResultSet resultSet = statement.executeQuery();

            List<UserDto> list = new ArrayList<>();
            while (resultSet.next()) {
                long entityId = resultSet.getLong("id");
                String email = resultSet.getString("email");
                String login = resultSet.getString("login");
                int userRoleId = resultSet.getInt("user_role_id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String phone = resultSet.getString("mob_phone");
                String city = resultSet.getString("city");
                String address = resultSet.getString("address");

                int block = resultSet.getInt("block_account");

                UserRole userRole = UserRole.values()[userRoleId - 1];

                UserDto dto = new UserDto();
                dto.setEmail(email);
                dto.setFirstName(firstName);
                dto.setMobilePhone(phone);
                dto.setCity(city);
                dto.setAddress(address);
                dto.setId(entityId);
                dto.setUserRole(userRole);
                dto.setLastName(lastName);
                dto.setLogin(login);
                if (block == 0) {
                    dto.setBlock(false);
                } else {
                    dto.setBlock(true);
                }

                list.add(dto);
            }
            return list;
        } catch (SQLException e) {
            log.error("Error during find all users from DB.", e);
            throw new DaoException("Error during find all users from DB.", e);
        }
    }

    @Override
    public UserDto getCourierForContactUser(long id) throws DaoException {
        try (Connection connection = connectionManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SELECT_COURIER_FOR_CONTACT);
            statement.setInt(1, Math.toIntExact(id));

            ResultSet resultSet = statement.executeQuery();
            UserDto userDto = new UserDto();
            while (resultSet.next()) {
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String workPhone = resultSet.getString("mob_phone");
                long userAccountId = resultSet.getInt("user_account_id");

                userDto.setFirstName(firstName);
                userDto.setId(userAccountId);
                userDto.setLastName(lastName);
                userDto.setMobilePhone(workPhone);
            }
            return userDto;
        } catch (SQLException e) {
            log.error("Error during get courier service for contact with user from DB!", e);
            throw new DaoException("Error during get courier service  for contact with user from DB!", e);
        }
    }

    private UserDto fromEntity(User user) {

        UserDto dto = new UserDto();
        dto.setEmail(user.getEmail());
        dto.setFirstName(user.getFirstName());
        dto.setId(user.getId());
        dto.setUserRole(user.getRole());
        dto.setLastName(user.getLastName());
        dto.setLogin(user.getLogin());
        dto.setPassword(user.getPassword());
        dto.setBlock(user.isBlock());

        return dto;
    }

    private User fromDto(UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setPassword(userDto.getPassword());
        user.setRole(userDto.getUserRole());
        user.setEmail(userDto.getEmail());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setBlock(userDto.isBlock());
        return user;
    }
}
