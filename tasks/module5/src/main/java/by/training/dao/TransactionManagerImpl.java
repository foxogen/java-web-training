package by.training.dao;

import by.training.core.Bean;
import lombok.extern.log4j.Log4j;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

@Bean
@Log4j
public class TransactionManagerImpl implements TransactionManager {

    private DataSource dataSource;
    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    public TransactionManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void beginTransaction() throws TransactionManagerException {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            log.error("Error during begin transaction!", e);
            throw new TransactionManagerException("Error during begin transaction!", e);
        }
        localConnection.set(connection);
    }

    @Override
    public void commitTransaction() throws TransactionManagerException {
        Connection connection = localConnection.get();
        if (connection != null) {
            try {
                connection.commit();
                connection.close();
            } catch (SQLException e) {
                log.error("Error during commit transaction!", e);
                throw new TransactionManagerException("Error during commit transaction!", e);
            }
        }
        localConnection.remove();
    }

    @Override
    public void rollbackTransaction() throws TransactionManagerException {
        Connection connection = localConnection.get();
        if (connection != null) {
            try {
                connection.rollback();
                connection.close();
            } catch (SQLException e) {
                log.error("Error during rollback transaction!", e);
                throw new TransactionManagerException("Error during rollback transaction!", e);
            }
        }
        localConnection.remove();
    }

    @Override
    public Connection getConnection() {
        if (localConnection.get() != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if (method.getName().equals("close")) {
                            return null;
                        } else {
                            Connection realConnection = localConnection.get();
                            return method.invoke(realConnection, args);
                        }
                    });
        } else {
            return dataSource.getConnection();
        }
    }

}
