package by.training.dao;

class ConnectionPoolException extends RuntimeException {
    ConnectionPoolException(String message) {
        super(message);
    }

    ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }
}
