package by.training.dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionPool {
    Connection getConnection();

    void releaseConnection(Connection connection);

    void shutdown() throws SQLException;
}
