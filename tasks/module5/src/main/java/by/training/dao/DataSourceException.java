package by.training.dao;

public class DataSourceException extends ConnectionPoolException {

    public DataSourceException(String message, Throwable cause) {
        super(message, cause);
    }
}
