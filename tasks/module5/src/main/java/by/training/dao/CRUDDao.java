package by.training.dao;

import java.util.List;

public interface CRUDDao<E, K> {

    K save(E entity) throws DaoException;

    boolean update(E entity) throws DaoException;

    boolean delete(E entity) throws DaoException;

    E getById(K id) throws DaoException;

    List<E> findAll() throws DaoException;
}
