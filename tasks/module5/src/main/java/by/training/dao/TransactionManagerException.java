package by.training.dao;

public class TransactionManagerException extends RuntimeException {
    public TransactionManagerException(String message, Throwable cause) {
        super(message, cause);
    }
}
