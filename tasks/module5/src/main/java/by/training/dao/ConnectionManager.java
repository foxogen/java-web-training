package by.training.dao;

import java.sql.Connection;

public interface ConnectionManager {

    Connection getConnection();

}
