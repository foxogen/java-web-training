package by.training.dao;

import by.training.core.Bean;

import java.sql.Connection;
import java.sql.SQLException;

@Bean
public class DataSourceImpl implements DataSource {

    private final ConnectionPool connectionPool;

    public DataSourceImpl() {
        connectionPool = ConnectionPoolImpl.create();
    }

    @Override
    public Connection getConnection(){
        return connectionPool.getConnection();
    }

    @Override
    public void close() {
        try {
            this.connectionPool.shutdown();
        } catch (SQLException e) {
            throw new DataSourceException("Failed close connection pool!", e);
        }
    }
}
