package by.training.dao;

import by.training.core.Bean;
import lombok.NoArgsConstructor;

import java.sql.Connection;

@Bean
@NoArgsConstructor
public class ConnectionManagerImpl implements ConnectionManager {

    private TransactionManager transactionManager;
    private DataSource dataSource;

    public ConnectionManagerImpl(TransactionManager transactionManager, DataSource dataSource) {
        this.transactionManager = transactionManager;
        this.dataSource = dataSource;
    }

    @Override
    public Connection getConnection() {
        Connection managerConnection = transactionManager.getConnection();
        return managerConnection != null ? managerConnection : dataSource.getConnection();
    }

}
