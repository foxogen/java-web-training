package by.training.dao;

import java.sql.Connection;

public interface DataSource {

    Connection getConnection();

    void close();
}
