package by.training.dao;

import by.training.core.Bean;
import lombok.extern.log4j.Log4j;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@Bean
@Log4j
public class ConnectionPoolImpl implements ConnectionPool {

    private static final int INITIAL_POOL_SIZE = 10;
    private static ReentrantLock reentrantLock = new ReentrantLock(true);
    private static Condition condition = reentrantLock.newCondition();
    private static ConnectionPoolImpl instance;
    private BlockingQueue<Connection> usedConnections;
    private BlockingQueue<Connection> connectionPool;
    private String url;
    private String user;
    private String password;
    private Driver driver;

    private ConnectionPoolImpl() {
        Properties resource = new Properties();
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            resource.load(new FileReader(Objects.requireNonNull(classLoader.getResource("db.properties")).getFile()));

            this.url = resource.getProperty("url_db");
            this.user = resource.getProperty("user_db");
            this.password = resource.getProperty("password_db");

        } catch (IOException e) {
            throw new ConnectionPoolException("File not found! ", e);
        }

        try {
            Class driverClass = Class.forName("org.postgresql.Driver");
            this.driver = (Driver) driverClass.newInstance();
            DriverManager.registerDriver(this.driver);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
            log.error("Failed to connect driver! ", e);
            throw new ConnectionPoolException("Failed to connect driver! ", e);
        }
        connectionPool = new ArrayBlockingQueue<>(INITIAL_POOL_SIZE);
        usedConnections = new ArrayBlockingQueue<>(INITIAL_POOL_SIZE);

        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            connectionPool.add(createConnection(url, user, password));
        }
        log.info("Connection pool for 10 connections was created!");
    }

    public static ConnectionPoolImpl create() {
        if (instance == null) {
            try {
                reentrantLock.lock();
                if (instance == null) {
                    instance = new ConnectionPoolImpl();
                }
            } finally {
                reentrantLock.unlock();
            }
        }
        return instance;
    }

    private static Connection createConnection(String url, String user, String password) {
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            log.error("Failed get connection! ", e);
            throw new ConnectionPoolException("Failed get connection! ", e);
        }
    }

    @Override
    public Connection getConnection() {
        reentrantLock.lock();
        try {
            while (connectionPool.isEmpty()) {
                condition.await();
            }
            Connection connection = connectionPool.poll();
            usedConnections.add(connection);
            return createProxyConnection(connection);
        } catch (InterruptedException e) {
            log.error("Error getting connection! ", e);
            throw new ConnectionPoolException("Error getting connection! ", e);
        } finally {
            reentrantLock.unlock();
        }
    }

    private Connection createProxyConnection(Connection connection) {
        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if ("close".equals(method.getName())) {
                        releaseConnection(connection);
                        return null;
                    } else if ("hashCode".equals(method.getName())) {
                        return connection.hashCode();
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    @Override
    public void releaseConnection(Connection connection) {
        reentrantLock.lock();
        try {
            if (!usedConnections.contains(connection)) {
                log.error("This connection is not in use!");
                throw new ConnectionPoolException("This connection is not in use!");
            }
            usedConnections.remove(connection);
            connectionPool.add(connection);
            condition.signal();
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override
    public void shutdown() throws SQLException {
        Connection connection;
        while (connectionPool.size() > 0) {
            connection = connectionPool.poll();
            connection.close();
        }

        while (usedConnections.size() > 0) {
            connection = usedConnections.poll();
            connection.close();
        }

        DriverManager.deregisterDriver(driver);
        log.info("Connection pool is closed!");
    }
}
