package by.training.user;

import by.training.dao.ConnectionManager;
import by.training.dao.DaoException;
import by.training.entity.UserRole;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

@RunWith(JUnit4.class)
public class UserDaoTest {
    private static Connection connection;
    private UserDao userDao;
    private UserDto userDto;

    @BeforeClass
    public static void initConnection() throws SQLException {
        connection =
                DriverManager.getConnection("jdbc:postgresql://localhost:5432/courier_test", "courier", "qwert123");
    }

    @AfterClass
    public static void closeConnection() throws SQLException {
        connection.close();
    }

    @Before
    public void createTable() throws SQLException {
        ConnectionManager mockConnectionManager = Mockito.mock(ConnectionManager.class);
        userDao = new UserDaoImpl(mockConnectionManager);
            Mockito.when(mockConnectionManager.getConnection()).thenReturn(createConnectionProxy(connection));

        userDto = new UserDto();
        userDto.setLogin("test5");
        userDto.setPassword("test5");
        userDto.setUserRole(UserRole.USER);
        userDto.setFirstName("test");
        userDto.setLastName("test");
        userDto.setEmail("test");
        userDto.setBlock(false);

        String sql = "create table user_account\n" +
                "(\n" +
                "\tid serial not null\n" +
                "\t\tconstraint user_account_pk\n" +
                "\t\t\tprimary key,\n" +
                "\tlogin varchar(40),\n" +
                "\tpassword varchar(40) not null,\n" +
                "\tuser_role_id int not null,\n" +
                "\tfirst_name varchar(20),\n" +
                "\tlast_name varchar(20),\n" +
                "\temail varchar(50),\n" +
                "\tblock_account int\n" +
                ")";
        executeSql(sql);
    }

    @Test
    public void shouldInsertUser() throws DaoException {
        userDao.save(userDto);
        Optional<UserDto> afterInsert = userDao.findByLogin("test5");
        Assert.assertEquals("test5", afterInsert.get().getLogin());
    }

    @Test
    public void shouldFindByLogin() throws DaoException {
        Optional<UserDto> foundUser = userDao.findByLogin("test5");
        Assert.assertFalse(foundUser.isPresent());
    }

    @Test
    public void shouldDeleteUser() throws DaoException {
        userDao.delete(userDto);
        Optional<UserDto> afterDelete = userDao.findByLogin("test5");
        Assert.assertFalse(afterDelete.isPresent());
    }

    private Connection createConnectionProxy(Connection realConnection) {
        return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if (method.getName().equals("close")) {
                        return null;
                    } else {
                        return method.invoke(realConnection, args);
                    }
                });
    }

    @After
    public void dropTable() throws SQLException {
        String sql = "drop table user_account";
        executeSql(sql);
    }

    private void executeSql(String sql) throws SQLException {
        PreparedStatement createTableStatement = connection.prepareStatement(sql);
        createTableStatement.executeUpdate();
        createTableStatement.close();
    }
}