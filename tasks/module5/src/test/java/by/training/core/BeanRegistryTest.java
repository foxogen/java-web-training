package by.training.core;

import by.training.command.TestCommand1;
import by.training.command.TestCommand2;
import by.training.user.UserService;
import by.training.user.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class BeanRegistryTest {

    @Test
    public void shouldRegisterAndReturnServiceByInterface() {

        BeanRegistry provider = new BeanRegistryImpl();
        UserService serviceImpl = new UserServiceImpl(null, null, null);
        provider.registerBean(serviceImpl);
        UserService serviceFound = provider.getBean(UserService.class);
        Assert.assertEquals(serviceImpl, serviceFound);
    }

    @Test
    public void shouldRegisterAndReturnServiceByName() {

        BeanRegistry provider = new BeanRegistryImpl();
        UserService serviceImpl = new UserServiceImpl(null, null, null);
        provider.registerBean(serviceImpl);
        UserService serviceFound = provider.getBean("UserService");
        Assert.assertEquals(serviceImpl, serviceFound);
    }

    @Test(expected = NotUniqueBeanException.class)
    public void shouldThrowExceptionOnDuplicateName() {
        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(TestCommand1.class);
        provider.registerBean(TestCommand1.class);
    }

    @Test(expected = MissedAnnotationException.class)
    public void shouldThrowExceptionMissedAnnotationBean() {
        BeanRegistry provider = new BeanRegistryImpl();
        provider.registerBean(TestCommand2.class);

    }
}