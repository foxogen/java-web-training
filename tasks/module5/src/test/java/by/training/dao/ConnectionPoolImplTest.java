package by.training.dao;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@RunWith(JUnit4.class)
public class ConnectionPoolImplTest {
    private static ConnectionPool connectionPool;

    @BeforeClass
    public static void setUp() {
        connectionPool = ConnectionPoolImpl.create();
    }

    @Test(expected = ConnectionPoolException.class)
    public void shouldThrowExceptionWhenReleaseWrongConnection() throws SQLException {
        Connection connection =
                DriverManager.getConnection("jdbc:postgresql://localhost:5432/courier_test", "courier", "qwert123");
        connectionPool.releaseConnection(connection);
        Assert.fail("Test should throw ConnectionPoolException");
    }

    @AfterClass
    public static void destroy() throws SQLException {
        connectionPool.shutdown();
    }
}