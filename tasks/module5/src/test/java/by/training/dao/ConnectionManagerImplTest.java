package by.training.dao;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.SQLException;

@RunWith(JUnit4.class)
public class ConnectionManagerImplTest {
    private static ConnectionPool connectionPool;
    private ConnectionManager connectionManager;

    @BeforeClass
    public static void initConnectionPool(){
        connectionPool = ConnectionPoolImpl.create();
    }

    @Before
    public void initConnectionManager(){
        DataSource dataSource = new DataSourceImpl();
        TransactionManager transactionManager = new TransactionManagerImpl(dataSource);
        connectionManager = new ConnectionManagerImpl(transactionManager, dataSource);
    }

    @Test
    public void getConnection() throws SQLException {
        Connection connection = connectionManager.getConnection();
        Assert.assertNotNull(connection);
        connection.close();
    }

    @AfterClass
    public static void closeConnectionPool() throws SQLException {
        connectionPool.shutdown();
    }
}