create table if not exists user_role
(
	id serial not null
		constraint user_role_pk
			primary key,
	role_name varchar(15) not null
);

alter table user_role owner to courier;

create table if not exists user_account
(
	id serial not null
		constraint user_account_pk
			primary key,
	login varchar(15) not null,
	password varchar(100) not null,
	user_role_id integer
		constraint user_account_user_role_id_fk
			references user_role,
	first_name varchar(25),
	last_name varchar(25),
	email varchar(50),
	block_account integer
);

alter table user_account owner to courier;

create unique index if not exists user_account_login_uindex
	on user_account (login);

create unique index if not exists user_role_name_uindex
	on user_role (role_name);

create table if not exists user_info
(
	id serial not null
		constraint user_info_pk
			primary key
		constraint user_info_user_account_id_fk
			references user_account,
	mob_phone varchar(16),
	city varchar(20),
	address varchar(50)
);

alter table user_info owner to courier;

create table if not exists type_transport
(
	id serial not null
		constraint type_transport_pk
			primary key,
	transport_name varchar(30) not null,
	capacity integer
);

alter table type_transport owner to courier;

create table if not exists service_status
(
	id serial not null
		constraint service_status_pk
			primary key,
	status_name varchar(20) not null
);

alter table service_status owner to courier;

create table if not exists courier_service
(
	id serial not null
		constraint courier_service_pk
			primary key,
	start_working_hour time,
	end_working_time time,
	user_account_id integer not null
		constraint courier_service_user_account_id_fk
			references user_account,
	type_transport_id integer not null
		constraint courier_service_type_transport_id_fk
			references type_transport,
	about_myself varchar(2000),
	mob_phone varchar(20) not null,
	service_status_id integer not null
		constraint courier_service_service_status_id_fk
			references service_status
);

alter table courier_service owner to courier;

create unique index if not exists service_status_status_name_uindex
	on service_status (status_name);

create table if not exists user_comment
(
	id serial not null
		constraint user_comment_pk
			primary key,
	date date not null,
	comment_text varchar(500),
	courier_service_id integer not null
		constraint user_comment_courier_service_id_fk
			references courier_service
);

alter table user_comment owner to courier;

create table if not exists courier_regions
(
	id serial not null
		constraint courier_regions_pk
			primary key,
	region_name varchar(50) not null,
	courier_service_id integer not null
		constraint courier_regions_courier_service_id_fk
			references courier_service
);

alter table courier_regions owner to courier;

create table if not exists order_status
(
	id serial not null
		constraint order_status_pk
			primary key,
	status_name varchar(20) not null
);

alter table order_status owner to courier;

create table if not exists user_order
(
	id serial not null
		constraint user_order_pk
			primary key,
	text_order varchar(500),
	user_account_id integer not null
		constraint user_order_user_account_id_fk
			references user_account,
	data_order date not null,
	export_address varchar(100) not null,
	import_address varchar(100) not null,
	price numeric(10,2),
	cargo_weight integer,
	mob_phone varchar(20) not null,
	city varchar(30) not null,
	order_status_id integer not null
		constraint user_order_order_status_id_fk
			references order_status
);

alter table user_order owner to courier;

create table if not exists user_check
(
	id serial not null
		constraint user_check_pk
			primary key
		constraint user_check_user_order_id_fk
			references user_order,
	order_check integer
);

alter table user_check owner to courier;

create unique index if not exists order_status_status_name_uindex
	on order_status (status_name);

create table if not exists courier_message
(
	id serial not null
		constraint user_message_pk
			primary key,
	message varchar(500),
	courier_service_id integer not null
		constraint courier_message_user_account_id_fk
			references user_account
);

alter table courier_message owner to courier;

