<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 02.12.2019
  Time: 21:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>Your Services</title>
</head>
<body>
<jsp:include page="/jsp/patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<h3 class="title has-text-black" style="text-align: center"><fmt:message key="page.courier_service_list"/></h3>
<hr class="login-hr">

<c:forEach items="${requestScope.allCourierService}" var="service">
    <div class="column is-5 is-offset-4">
        <div class="box" style="align-items: center;">
            <ul>
                <li>
                    <label class="label"><fmt:message key="page.courier_service_info_start_work_time"/>
                            ${service.startTime}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.courier_service_info_end_work_time"/>
                            ${service.endTime}
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.courier_service_transport"/>
                            ${service.transport.transportName}
                    </label>
                </li>
                <li>
                <li>
                    <label class="label"><fmt:message key="page.courier_service_transport_capacity"/>
                            ${service.transport.transportCapacity} <fmt:message
                                key="page.courier_service_transport_kilo"/>
                    </label>
                </li>
                <li>
                    <label class="label"><fmt:message key="page.courier_service_working_phone"/>
                            ${service.mobilePhone}
                    </label>

                </li>
                <li>
                    <label class="label"><fmt:message key="page.courier_service_region"/> </label>
                    <c:forEach items="${service.regions}" var="region">
                        <label class="label">
                                ${region.nameRegion}
                        </label>
                    </c:forEach>
                </li>
                <li>
                    <c:choose>
                        <c:when test="${service.serviceStatus == 'SUBMITTED'}">
                            <label class="label"><fmt:message key="page.courier_service_info_status"/>
                                <fmt:message key="page.courier_service_status_submitted"/>
                            </label>
                        </c:when>
                        <c:when test="${service.serviceStatus == 'APPROVED'}">
                            <label class="label"><fmt:message key="page.courier_service_info_status"/>
                                <fmt:message key="page.courier_service_status_approved"/>
                            </label>
                        </c:when>
                        <c:when test="${service.serviceStatus == 'DECLINED'}">
                            <label class="label"><fmt:message key="page.courier_service_info_status"/>
                                <fmt:message key="page.courier_service_status_"/>
                            </label>
                        </c:when>
                    </c:choose>

                </li>
            </ul>
            <br>
            <form action="${pageContext.request.contextPath}/courierServiceList/edit" method="get"
                  style="display: inline-block">
                <input type="hidden" name="courierServiceId" value="${service.id}">
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                key="page.button_edit_profile"/></button>
                    </div>
                </div>
            </form>
            <form action="${pageContext.request.contextPath}/courierServiceList/deleteService" method="post"
                  style="display: inline-block">
                <input type="hidden" name="courierServiceId" value="${service.id}">
                <input type="hidden" name="courierServiceTransportId" value="${service.transport.id}">
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                key="page.admin_delete"/></button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</c:forEach>
<form action="${pageContext.request.contextPath}/courierServiceList" method="post">
    <nav class="pagination is-centered" role="navigation" aria-label="pagination">
        <ul class="pagination-list">
            <c:forEach begin="1" end="${lastPage}" var="i">
                <li><a class="pagination-link"
                       href="${pageContext.request.contextPath}/courierServiceList?page=${i}">${i}</a>
                </li>
            </c:forEach>
        </ul>
    </nav>
</form>


<jsp:include page="/jsp/patterns/_footer.jsp"/>
</body>
</html>
