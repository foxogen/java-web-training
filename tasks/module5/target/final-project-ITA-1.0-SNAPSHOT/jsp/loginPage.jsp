<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 18.11.2019
  Time: 19:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>

<html>
<head>
    <title>Login</title>
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<section class="hero">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">
                <h3 class="title has-text-black"><fmt:message key="page.login"/></h3>
                <hr class="login-hr">
                <p class="subtitle has-text-black"><fmt:message key="page.login_text"/></p>
                <form method="post" action="${pageContext.request.contextPath}/loginUser">
                    <div class="box">
                        <form>
                            <c:if test="${error == 'true'}">
                                <label class="label" style="color: red"><fmt:message key="${errorDuringLogin}"/></label>
                            </c:if>
                            <div class="field">
                                <div class="control">
                                    <input class="input is-large" type="login" name="login"
                                           placeholder="<fmt:message key="page.login"/>" autofocus="">
                                </div>
                            </div>

                            <div class="field">
                                <div class="control">
                                    <input class="input is-large" type="password" name="password"
                                           placeholder="<fmt:message key="page.password"/>">
                                </div>
                            </div>
                            <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                    key="page.button_login"/><i class="fa fa-sign-in"
                                                                aria-hidden="true"></i>
                            </button>
                        </form>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</body>
</html>
<jsp:include page="patterns/_footer.jsp"/>
</body>
</html>
