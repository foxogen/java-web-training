<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 28.11.2019
  Time: 14:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="by.training.ApplicationConstant" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>

<html>
<head>
    <title>Edit Profile</title>
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<form method="post" action="${pageContext.request.contextPath}/userInfo/edit">
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <form method="post" action="${pageContext.request.contextPath}/registerUser">
                    <div class="column is-4 is-offset-4">
                        <h3 class="title has-text-black"><fmt:message key="page.button_edit_profile"/></h3>
                        <hr class="login-hr">
                        <div class="box">

                            <div class="field">
                                <c:if test="${editProfileError == true}">
                                    <c:forEach items="${requestScope.registrationError}" var="error">
                                        <label class="label" style="color: red"><fmt:message key="${error}"/></label>
                                    </c:forEach>
                                </c:if>
                                <label class="label"><fmt:message key="page.first_name"/></label>
                                <div class="control">
                                    <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>"
                                           name="firstName"
                                           value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).firstName}">
                                </div>
                            </div>

                            <div class="field">
                                <label class="label"><fmt:message key="page.last_name"/></label>
                                <div class="control">
                                    <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>"
                                           name="lastName"
                                           value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).lastName}">
                                </div>
                            </div>

                            <div class="field">
                                <label class="label"><fmt:message key="page.email"/></label>
                                <div class="control">
                                    <input class="input" type="email" placeholder="<fmt:message key="page.text_input"/>"
                                           name="email"
                                           value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).email}">
                                </div>
                            </div>

                            <div class="field">
                                <label class="label"><fmt:message key="page.phone"/></label>
                                <div class="control">
                                    <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>"
                                           name="phone"
                                           value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).mobilePhone}">
                                </div>
                            </div>

                            <div class="field">
                                <label class="label"><fmt:message key="page.city"/></label>
                                <div class="control">
                                    <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>"
                                           name="city"
                                           value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).city}">
                                </div>
                            </div>

                            <div class="field">
                                <label class="label"><fmt:message key="page.address"/></label>
                                <div class="control">
                                    <input class="input" type="text" placeholder="<fmt:message key="page.text_input"/>"
                                           name="address"
                                           value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).address}">
                                </div>
                            </div>

                            <div class="field is-grouped">
                                <div class="control">
                                    <button class="button is-block is-info is-large is-fullwidth"><fmt:message
                                            key="page.button_edit_profile"/>
                                        <i class="fa fa-sign-in"
                                           aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</form>
<jsp:include page="patterns/_footer.jsp"/>
</body>
</html>
