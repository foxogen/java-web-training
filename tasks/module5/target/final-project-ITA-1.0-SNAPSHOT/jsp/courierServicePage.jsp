<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 01.12.2019
  Time: 19:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="by.training.ApplicationConstant" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <title>Your service</title>
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<section class="hero">
    <div class="hero-body is-medium">
        <div class="container has-text-centered">
            <div class="column is-offset-4" style="margin: auto">
                <h3 class="title has-text-black"><fmt:message key="page.courier_service"/></h3>
                <hr class="login-hr">
                <form action="${pageContext.request.contextPath}/courierService" method="post">
                    <div class="box">
                        <div class="field">
                            <c:if test="${checkData == true}">
                                <c:forEach items="${requestScope.courierServiceError}" var="error">
                                    <label class="label" style="color: red"><fmt:message key="${error}"/></label>
                                </c:forEach>
                            </c:if>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.courier_service_working_time"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded ">
                                        <input class="input" type="text" name="startTime"
                                               placeholder="<fmt:message key="page.courier_service_working_time_format"/> 10:00">
                                    </p>
                                </div>
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input" type="text" name="endTime"
                                               placeholder="<fmt:message key="page.courier_service_working_time_format"/> 20:00">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.courier_service_region"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="text" name="regions"
                                               placeholder="<fmt:message key="page.courier_service_region_format"/> ">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.courier_service_working_phone"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <input class="input" type="text" name="workingPhone"
                                               value="${ApplicationConstant.SECURITY_SERVICE.getCurrentUser(pageContext.request.session).mobilePhone}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.courier_service_transport"/></label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control is-expanded ">
                                        <input class="input" type="text" name="transport">
                                    </p>
                                </div>
                                <div class="field-label is-normal">
                                    <label class="label"><fmt:message
                                            key="page.courier_service_transport_capacity"/></label>
                                </div>
                                <div class="field">
                                    <p class="control is-expanded">
                                        <input class="input" type="text" name="capacity"
                                               placeholder="<fmt:message key="page.courier_service_transport_kilo"/>">
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label is-normal">
                                <label class="label"><fmt:message key="page.courier_service_about_myself"/> </label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <textarea class="textarea" name="aboutMyself"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <div class="control">
                                        <button class="button is-primary">
                                           <fmt:message key="page.courier_service_submit"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<jsp:include page="patterns/_footer.jsp"/>
</body>
</html>
