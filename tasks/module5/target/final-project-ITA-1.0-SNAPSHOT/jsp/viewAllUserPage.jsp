<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 26.11.2019
  Time: 17:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n/courier" scope="application"/>
<html>
<head>
    <meta charset="UTF-8">
    <title>All users</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
</head>
<body>
<jsp:include page="patterns/_header.jsp"/>
<jsp:include page="/jsp/patterns/_menu.jsp"/>
<h3 class="title has-text-black" style="text-align: center"><fmt:message key="page.nav_menu_view_all_users"/></h3>
<hr class="login-hr">
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th><fmt:message key="page.login"/></th>
            <th><fmt:message key="page.user_role"/></th>
            <th><fmt:message key="page.first_name"/></th>
            <th><fmt:message key="page.last_name"/></th>
            <th><fmt:message key="page.email"/></th>
            <th><fmt:message key="page.city"/></th>
            <th><fmt:message key="page.address"/></th>
            <th><fmt:message key="page.phone"/></th>
            <th><fmt:message key="page.admin_delete"/></th>
            <th><fmt:message key="page.admin_block"/></th>

        </tr>
        </thead>
        <c:forEach items="${requestScope.allUser}" var="user">
            <tbody>
            <tr>
                <th>${user.id}</th>
                <td>${user.login}</td>
                <td>${user.userRole}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.email}</td>
                <td>${user.city}</td>
                <td>${user.address}</td>
                <td>${user.mobilePhone}</td>
                <td>
                    <form action="${pageContext.request.contextPath}/viewAllUser/deleteUser" method="post">
                        <input type="hidden" name="userId" value="${user.id}">
                        <input type="hidden" name="userRole" value="${user.userRole}">
                        <input type="hidden" name="page" value="${page}">
                        <input class="button is-small" type="submit" value="<fmt:message key="page.admin_delete"/>"/>
                    </form>
                </td>
                <c:if test="${user.block == 'true'}">
                    <td>
                        <form action="${pageContext.request.contextPath}/viewAllUser/blockUser" method="post">
                            <input type="hidden" name="userId" value="${user.id}">
                            <input type="hidden" name="block" value="0">
                            <input type="hidden" name="page" value="${page}">
                            <input class="button is-small" type="submit"
                                   value="<fmt:message key="page.admin_unblock"/>"/>
                        </form>
                    </td>
                </c:if>
                <c:if test="${user.block == 'false'}">
                    <td>
                        <form action="${pageContext.request.contextPath}/viewAllUser/blockUser" method="post">
                            <input type="hidden" name="userId" value="${user.id}">
                            <input type="hidden" name="page" value="${page}">
                            <input type="hidden" name="block" value="1">
                            <input class="button is-small" type="submit" value="<fmt:message key="page.admin_block"/>"/>
                        </form>
                    </td>
                </c:if>
            </tr>

            </tbody>
        </c:forEach>
    </table>
    <form action="${pageContext.request.contextPath}/viewAllUser" method="post">
        <nav class="pagination is-centered" role="navigation" aria-label="pagination">
            <ul class="pagination-list">
                <c:forEach begin="1" end="${lastPage}" var="i">
                    <li><a class="pagination-link"
                           href="${pageContext.request.contextPath}/viewAllUser?page=${i}">${i}</a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </form>
</div>
<jsp:include page="patterns/_footer.jsp"/>
</body>
</html>
