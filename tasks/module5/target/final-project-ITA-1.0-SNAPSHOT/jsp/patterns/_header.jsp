<%--
  Created by IntelliJ IDEA.
  User: Илья
  Date: 21.08.2019
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="by.training.ApplicationConstant" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${requestScope.get('lang')}"/>
<fmt:setBundle basename="i18n/courier" scope="application"/>

<html>
<head>
    <meta charset="UTF-8">
    <title>Courier exchange</title>
    <link rel="stylesheet" href="<c:url value="/static/style/bulma.css"/>">
</head>
<body>
<section class="hero is-info">
    <div class="control">
        <div class="hero-head has-background-primary">
            <nav class="navbar">
                <div class="container">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="${pageContext.request.contextPath}/">
                            <h1><fmt:message key="page.title"/></h1>
                        </a>
                    </div>
                    <div class="navbar-brand">
                        <a class="navbar-item" href="#" onclick="changeLang('ru')">
                            <h1><fmt:message key="page.lang_ru"/></h1>
                        </a>
                    </div>
                    <div class="navbar-brand">
                        <a class="navbar-item" href="#" onclick="changeLang('en')">
                            <h1><fmt:message key="page.lang_en"/></h1>
                        </a>
                    </div>
                    <div id="navbarMenu" class="navbar-menu">
                        <div class="navbar-end">
                            <c:if test="${ApplicationConstant.SECURITY_SERVICE.isLogIn(pageContext.request.session) == 'false'}">
                                <span class="navbar-item">

    <a class="button is-white is-outlined" href="${pageContext.request.contextPath}/registerUser">
    <span class="is-small">
    </span>
    <span><fmt:message key="page.button_registration"/></span>
    </a>
    </span>
                                <span class="navbar-item">
    <a class="button is-white is-outlined" href="${pageContext.request.contextPath}/loginUser">
    <span class="is-small">
    </span>
    <span><fmt:message key="page.button_login"/></span>
    </a>
    </span>
                            </c:if>
                            <c:if test="${ApplicationConstant.SECURITY_SERVICE.isLogIn(pageContext.request.session)}">
                            <span class="navbar-item">
    <a class="button is-white is-outlined" href="${pageContext.request.contextPath}/logoutUser">
    <span class="is-small">
    </span>
    <span><fmt:message key="page.button_logout"/></span>
    </a>
    </span>
                                <span class="navbar-item">
                        <div class="control">
                            <div class="select">
                                <select class="is-small"
                                        onchange="window.location.href=this.options[this.selectedIndex].value">
                                    <c:choose>
                                        <c:when test="${ApplicationConstant.SECURITY_SERVICE.containRole(pageContext.request.session,
                                         ApplicationConstant.USER_ROLE)}">
                                            <option selected value="${pageContext.request.contextPath}/"><fmt:message
                                                    key="page.nav_menu"/></option>
                                            <option value="${pageContext.request.contextPath}/userInfo"><fmt:message
                                                    key="page.nav_menu_profile"/></option>
                                            <option value="${pageContext.request.contextPath}/createOrder"><fmt:message
                                                    key="page.user_create_order"/></option>
                                            <option value="${pageContext.request.contextPath}/ordersList?page=1"><fmt:message
                                                    key="page.user_orders"/></option>
                                        </c:when>
                                        <c:when test="${ApplicationConstant.SECURITY_SERVICE.containRole(pageContext.request.session,
                                         ApplicationConstant.ADMIN_ROLE)}">
                                            <option selected value="${pageContext.request.contextPath}/"><fmt:message
                                                    key="page.nav_menu"/></option>
                                            <option value="${pageContext.request.contextPath}/userInfo"><fmt:message
                                                    key="page.nav_menu_profile"/></option>
                                            <option value="${pageContext.request.contextPath}/viewAllUser?page=1"><fmt:message
                                                    key="page.nav_menu_view_all_users"/></option>
                                            <option value="${pageContext.request.contextPath}/viewAllCourierService?page=1"><fmt:message
                                                    key="page.nan_menu_view_all_courier_services"/></option>
                                            <option value="${pageContext.request.contextPath}/viewAllOrderUser?page=1"><fmt:message
                                                    key="page.admin_view_all_orders"/></option>
                                        </c:when>
                                        <c:when test="${ApplicationConstant.SECURITY_SERVICE.containRole(pageContext.request.session,
                                         ApplicationConstant.COURIER_ROLE)}">
                                            <option selected value="${pageContext.request.contextPath}/"><fmt:message
                                                    key="page.nav_menu"/></option>
                                            <option value="${pageContext.request.contextPath}/userInfo"><fmt:message
                                                    key="page.nav_menu_profile"/></option>
                                            <option value="${pageContext.request.contextPath}/courierService"><fmt:message
                                                    key="page.courier_service"/></option>
                                            <option value="${pageContext.request.contextPath}/courierServiceList?page=1"><fmt:message
                                                    key="page.courier_service_list"/></option>
                                            <option value="${pageContext.request.contextPath}/viewOrderList?page=1"><fmt:message
                                                    key="page.courier_view_orders"/></option>
                                        </c:when>
                                    </c:choose>
                                </select>
                            </div>
                        </div>
                        </span>
                            </c:if>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</section>
</body>
</html>


<script type="text/javascript">
    function changeLang(name) {
        document.cookie = "lang=" + name;
        window.location.reload(true);
    }
</script>

