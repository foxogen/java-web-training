package by.training.validator;

import by.training.entity.TypeToy;
import org.apache.log4j.Logger;

public class DataValidatorFactory {

    private static final Logger LOGGER = Logger.getLogger(DataValidatorFactory.class);

    public AbstractValidator getDataValidator(TypeToy typeToy) {
        switch (typeToy) {
            case CAR:
                return new DataValidatorCar();
            case BALL:
                return new DataValidatorBall();
            case DOLL:
                return new DataValidatorDoll();
            case CUBE:
                return new DataValidatorCube();
            default:
                LOGGER.fatal("Fatal ERROR, It is not possible to create a validation object!");
                throw new RuntimeException("It is not possible to create a validation object!");
        }

    }
}
