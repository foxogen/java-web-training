package by.training.validator;

import java.util.Optional;
import java.util.stream.Stream;

public enum DefaultFieldEnum {
    ID, NAME, PRICE, AGETOY, TYPETOY, CARTYPE, BALLMATERIALTYPE, CUBEMATERIALTYPE, DOLLWEIGHT, BRAND;

    public static Optional<DefaultFieldEnum> fromString(String name){

        return Stream.of(DefaultFieldEnum.values())
                .filter(t -> t.name().equalsIgnoreCase(name))
                .findFirst();
    }
}
