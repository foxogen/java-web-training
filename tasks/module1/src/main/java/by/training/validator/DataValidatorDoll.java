package by.training.validator;

import by.training.util.DataValidatorUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataValidatorDoll extends AbstractValidator {

    private static final String CODE_EXEPTION = "5";

    @Override
    public ValidatorResult checkDataValidate(String key, String value) {

        Map<String, List<String>> errorMap = new HashMap<>();
        List<String> error = new ArrayList<>();

        if (!DataValidatorUtil.isStringCheck(key) && !DataValidatorUtil.isStringCheck(value)) {
            error.add(DataValidatorDoll.class + " This key or value isEmpty!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }

            switch (key) {
                case "dollWeight":
                    if (DataValidatorUtil.isDoubleNum(value))
                        break;
                case "brand":
                    if (DataValidatorUtil.isStringCheck(value))
                        break;
                default:
                    error.add(DataValidatorDoll.class + " There is no such type of data!");
                    errorMap.put(CODE_EXEPTION, error);
            }

        return new ValidatorResult(errorMap);
    }


}
