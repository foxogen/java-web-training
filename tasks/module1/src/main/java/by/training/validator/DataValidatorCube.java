package by.training.validator;

import by.training.entity.CubeMaterialType;
import by.training.util.DataValidatorUtil;

import java.util.*;

public class DataValidatorCube extends AbstractValidator {

    private static final String CODE_EXEPTION = "5";

    @Override
    public ValidatorResult checkDataValidate(String key, String value) {

        Map<String, List<String>> errorMap = new HashMap<>();
        List<String> error = new ArrayList<>();

        if (!DataValidatorUtil.isStringCheck(key) && !DataValidatorUtil.isStringCheck(value)) {
            error.add(DataValidatorCube.class + " This key or value isEmpty!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }

        Optional<CubeMaterialType> cubeMaterialType = CubeMaterialType.fromString(value);

        if (!cubeMaterialType.isPresent()) {
            error.add(DataValidatorCube.class + " There is no type of CubeMaterialType in the line!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }

        CubeMaterialType type = CubeMaterialType.valueOf(value.toUpperCase());
        switch (type) {
            case IRON:
                break;
            case WOOD:
                break;
            case PAPER:
                break;
            case RUBBER:
                break;
            case PLASTIC:
                break;
            default:
                error.add(DataValidatorCube.class + " There is no such type of data!");
                errorMap.put(CODE_EXEPTION, error);
        }

        return new ValidatorResult(errorMap);
    }

}
