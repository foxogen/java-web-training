package by.training.validator;

import by.training.entity.TypeToy;

import java.util.*;

public class DataValidator {

    private static final String CODE_TYPE_INVALID = "3";
    private Map<String, List<String>> errorMap = new HashMap<>();
    private List<String> error = new ArrayList<>();

    public ValidatorResult dataValidate(Map<String, String> map) {

        String typeStr = map.get("TypeToy");

        Optional<TypeToy> toy = TypeToy.fromString(typeStr);
        if (!toy.isPresent()) {
            error.add("There is no type of toy in the line!");
            errorMap.put(CODE_TYPE_INVALID, error);
            return new ValidatorResult(errorMap);
        }

        TypeToy typeToy = TypeToy.valueOf(map.get("TypeToy"));
        DataValidatorFactory factory = new DataValidatorFactory();
        AbstractValidator validator = factory.getDataValidator(typeToy);

        ValidatorResult validatorResult = validator.checkDefaultField(map);

        return validatorResult;
    }
}
