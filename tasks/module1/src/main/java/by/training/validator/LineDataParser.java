package by.training.validator;


import java.util.*;

public class LineDataParser {

    private static final String CODE_STR_INVALID = "2";
    private LinkedHashMap<String, String> map;

    public LineDataParser(LinkedHashMap<String, String> map) {
        this.map = map;
    }

    public ValidatorResult lineParse(String line) {
        List<String> error = new ArrayList<>();
        Map<String, List<String>> errorMap = new HashMap<>();

        map.clear();

        List<String> list = new ArrayList<>(Arrays.asList(line.split(",")));

        for (String s : list) {
            if(s.isEmpty()){

                error.add("This str invalid!");
                errorMap.put(CODE_STR_INVALID, error);
                return new ValidatorResult(errorMap);
            }
        }

        String[] strSplit;
        for (String s : list) {
            strSplit = s.split(":");

            if(strSplit.length > 2){
                error.add("This str invalid!");
                errorMap.put(CODE_STR_INVALID, error);
                return new ValidatorResult(errorMap);
            }

            map.put(strSplit[0], strSplit[1]);
        }

        return new ValidatorResult(errorMap);
    }
}
