package by.training.validator;

import by.training.util.DataValidatorUtil;

import java.util.*;

public abstract class AbstractValidator {

    private static final String CODE_EXEPTION = "4";
    private ValidatorResult validatorResult;

    public abstract ValidatorResult checkDataValidate(String key, String value);

    public ValidatorResult checkDefaultField(Map<String, String> map) {

        Map<String, List<String>> errorMap = new HashMap<>();
        List<String> error = new ArrayList<>();

        if(map.size() < 6){
            error.add(DataValidatorCar.class + " This map invalid!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }

        for(Map.Entry<String, String> entry : map.entrySet()) {

            Optional<DefaultFieldEnum> defaultField = DefaultFieldEnum.fromString(entry.getKey());

            if (!defaultField.isPresent()) {
                error.add("This field " + entry.getKey() + " invalid");
                errorMap.put(CODE_EXEPTION, error);
                return new ValidatorResult(errorMap);
            }

            DefaultFieldEnum defaultFieldEnum = DefaultFieldEnum.valueOf(entry.getKey().toUpperCase());
            switch (defaultFieldEnum) {
                case ID:
                    if (DataValidatorUtil.isIntNum(entry.getValue()))
                        break;
                case NAME:
                    if (DataValidatorUtil.isStringCheck(entry.getValue()))
                        break;
                case PRICE:
                    if (DataValidatorUtil.isDoubleNum(entry.getValue()))
                        break;
                case AGETOY:
                    if (DataValidatorUtil.isIntNum(entry.getValue()))
                        break;
                case TYPETOY:
                    if (DataValidatorUtil.isStringCheck(entry.getValue()))
                        break;
                default:
                    validatorResult = checkDataValidate(entry.getKey(), entry.getValue());
                    if (!validatorResult.isValid()) return validatorResult;

            }
        }
        return new ValidatorResult(errorMap);
    }
}
