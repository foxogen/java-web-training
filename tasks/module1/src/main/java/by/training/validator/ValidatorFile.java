package by.training.validator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidatorFile {

    private static final String CODE_FILE_NOT_FOUND = "1";

    public ValidatorResult validFile(String path) {
        Map<String, List<String>> errorMap = new HashMap<>();
        List<String> error = new ArrayList<>();

        File file = new File(path);

        if (!file.isFile()) {
            error.add("File not found for this path: " + path);
            errorMap.put(CODE_FILE_NOT_FOUND, error);
            return new ValidatorResult(errorMap);
        }

        if (file.length() == 0) {
            error.add("The file on this path is empty: " + path);
            errorMap.put(CODE_FILE_NOT_FOUND, error);
            return new ValidatorResult(errorMap);
        }

        return new ValidatorResult(errorMap);
    }
}
