package by.training.validator;

import by.training.entity.BallMaterialType;
import by.training.util.DataValidatorUtil;

import java.util.*;

public class DataValidatorBall extends AbstractValidator {

    private static final String CODE_EXEPTION = "5";

    @Override
    public ValidatorResult checkDataValidate(String key, String value) {

        Map<String, List<String>> errorMap = new HashMap<>();
        List<String> error = new ArrayList<>();

        if (!DataValidatorUtil.isStringCheck(key) && !DataValidatorUtil.isStringCheck(value)) {
            error.add(DataValidatorBall.class + " This key or value isEmpty!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }

        Optional<BallMaterialType> ballMaterialType = BallMaterialType.fromString(value);

        if (!ballMaterialType.isPresent()) {
            error.add(DataValidatorBall.class + " There is no type of BallMaterialType in the line!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }

        BallMaterialType type = BallMaterialType.valueOf(value);
        switch (type) {
            case LEATHER:
                break;
            case RESIDOR:
                break;
            case POLYESTER:
                break;
            default:
                error.add(DataValidatorBall.class + " There is no such type of data!");
                errorMap.put(CODE_EXEPTION, error);
        }

        return new ValidatorResult(errorMap);
    }


}
