package by.training.validator;

import by.training.entity.CarType;
import by.training.util.DataValidatorUtil;

import java.util.*;

public class DataValidatorCar extends AbstractValidator {

    private static final String CODE_EXEPTION = "5";

    @Override
    public ValidatorResult checkDataValidate(String key, String value) {

        Map<String, List<String>> errorMap = new HashMap<>();
        List<String> error = new ArrayList<>();

        if (!DataValidatorUtil.isStringCheck(key) && !DataValidatorUtil.isStringCheck(value)) {
            error.add(DataValidatorCar.class + " This key or value isEmpty!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }

        Optional<CarType> carType = CarType.fromString(value);

        if (!carType.isPresent()) {
            error.add(DataValidatorCar.class + " There is no type of CarType in the line!");
            errorMap.put(CODE_EXEPTION, error);
            return new ValidatorResult(errorMap);
        }
        CarType type = CarType.valueOf(value.toUpperCase());
            switch (type) {
                case SMALL_CAR:
                    break;
                case LARGE_CAR:
                    break;
                case MEDIUM_CAR:
                    break;
                default:
                    error.add(DataValidatorCar.class + " There is no such type of data!");
                    errorMap.put(CODE_EXEPTION, error);

        }
        return new ValidatorResult(errorMap);
    }


}
