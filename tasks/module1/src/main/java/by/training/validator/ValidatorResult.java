package by.training.validator;

import java.util.List;
import java.util.Map;

public class ValidatorResult {

    private Map<String, List<String>> errorMap;

    public ValidatorResult(Map<String, List<String>> errorMap) {
        this.errorMap = errorMap;
    }

    public Map<String, List<String>> getError(){
        return errorMap;
    }

    public List<String> getErrorByKey(String key){
        return errorMap.get(key);
    }

    public boolean isValid() {
        return this.errorMap.isEmpty();
    }
}
