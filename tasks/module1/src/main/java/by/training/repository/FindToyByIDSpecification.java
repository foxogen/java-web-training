package by.training.repository;

import by.training.entity.Toy;

public class FindToyByIDSpecification implements Specification<Toy> {

    private int id;

    public FindToyByIDSpecification(int id) {
        this.id = id;
    }

    @Override
    public boolean match(Toy entity) {
        return entity.getId() == id;
    }
}
