package by.training.repository;

import by.training.entity.Toy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ToyRepository implements Repository<Toy> {

    private List<Toy> toyList = new ArrayList<>();

    @Override
    public void add(Toy toy) {
        toyList.add(toy);
    }

    @Override
    public List<Toy> getAllToy(){
        return toyList;
    }

    @Override
    public void delete(Toy toy) {
        toyList.remove(toy);
    }

    @Override
    public List<Toy> find(Specification<Toy> spec) {
        List<Toy> list = toyList.stream()
                .filter(toy -> spec.match(toy))
                .collect(Collectors.toList());

        return list;
    }
}
