package by.training.repository;

import java.util.List;

public interface Repository<T> {

    void add(T t);

    List<T> getAllToy();

    void delete(T t);

    List<T> find(Specification<T> spec);
}
