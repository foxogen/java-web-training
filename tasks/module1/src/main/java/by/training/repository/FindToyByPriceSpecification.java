package by.training.repository;

import by.training.entity.Toy;

public class FindToyByPriceSpecification implements Specification<Toy> {

    private double price;

    public FindToyByPriceSpecification(double price) {
        this.price = price;
    }

    @Override
    public boolean match(Toy entity) {
        return entity.getPrice() == price;
    }
}
