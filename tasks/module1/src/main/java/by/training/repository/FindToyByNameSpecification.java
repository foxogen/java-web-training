package by.training.repository;

import by.training.entity.Toy;

public class FindToyByNameSpecification implements Specification<Toy> {

    private String name;

    public FindToyByNameSpecification(String name) {
        this.name = name;
    }

    @Override
    public boolean match(Toy entity) {
        return entity.getName().equals(name);
    }
}
