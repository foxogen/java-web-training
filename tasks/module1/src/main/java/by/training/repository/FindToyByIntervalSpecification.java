package by.training.repository;

import by.training.entity.Toy;

public class FindToyByIntervalSpecification implements Specification<Toy> {

    private int initValue;
    private int finalValue;

    public FindToyByIntervalSpecification(int initValue, int finalValue) {
        this.initValue = initValue;
        this.finalValue = finalValue;
    }

    @Override
    public boolean match(Toy entity) {
        return entity.getId()> initValue && entity.getId() < finalValue;
    }
}
