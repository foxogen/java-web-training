package by.training.service;

import by.training.entity.Toy;
import by.training.repository.Specification;

import java.util.List;

public interface ToyService {
    void addToy(Toy toy);

    List<Toy> sortToyByName();

    List<Toy> sortToyByNameAndPrice();

    List<Toy> findToyByName(Specification<Toy> spec);

    List<Toy> findToyByNameAndPrice(Specification<Toy> spec);

}
