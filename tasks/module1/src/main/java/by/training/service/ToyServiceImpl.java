package by.training.service;

import by.training.entity.Toy;
import by.training.repository.Repository;
import by.training.repository.Specification;
import org.apache.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ToyServiceImpl implements ToyService {

    private static final Logger LOGGER = Logger.getLogger(ToyServiceImpl.class);

    private List<Toy> toyList;
    private Repository<Toy> repository;

    public ToyServiceImpl(Repository<Toy> repository) {
        this.repository = repository;
    }

    public void addToy(Toy toy) {
        if (toy != null) {
            repository.add(toy);
        } else {
            throw new IllegalArgumentException("Problem with object Toy!");
        }
    }

    public List<Toy> sortToyByName() {
        toyList = repository.getAllToy();
        List<Toy> list = toyList.stream()
                .sorted(Comparator.comparing(Toy::getName))
                .collect(Collectors.toList());

        LOGGER.info("Sort toys by name!");
        return list;
    }

    public List<Toy> sortToyByNameAndPrice() {
        toyList = repository.getAllToy();
        List<Toy> list = toyList.stream()
                .sorted(Comparator.comparing(Toy::getName).thenComparing(Toy::getPrice))
                .collect(Collectors.toList());

        LOGGER.info("Sort toys by name and price!");
        return list;
    }


    public List<Toy> findToyByName(Specification<Toy> spec) {
        toyList = repository.getAllToy();
        List<Toy> list = toyList.stream()
                .filter(toy -> spec.match(toy))
                .collect(Collectors.toList());

        LOGGER.info("Find toy by name!");
        return list;
    }

    public List<Toy> findToyByNameAndPrice(Specification<Toy> spec) {
        toyList = repository.getAllToy();
        List<Toy> list = toyList.stream()
                .filter(toy -> spec.match(toy))
                .collect(Collectors.toList());

        LOGGER.info("Find toy by name and price!");
        return list;
    }

}
