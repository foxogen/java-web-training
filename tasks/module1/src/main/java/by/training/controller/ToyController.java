package by.training.controller;

import by.training.builder.DataBuilder;
import by.training.builder.FactoryBuilder;
import by.training.validator.DataValidator;
import by.training.validator.LineDataReader;
import by.training.validator.ValidatorFile;
import by.training.validator.ValidatorResult;
import by.training.entity.Toy;
import by.training.entity.TypeToy;
import by.training.validator.LineDataParser;
import by.training.service.ToyServiceImpl;
import org.apache.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.List;

public class ToyController {

    private static final Logger LOGGER = Logger.getLogger(ToyController.class);
    private ToyServiceImpl service;
    private ValidatorFile validatorFile;
    private LineDataReader lineDataReader;
    private DataValidator dataValidator;
    private FactoryBuilder factoryBuilder;

    public ToyController(ToyServiceImpl service, ValidatorFile validatorFile, LineDataReader lineDataReader,
                         DataValidator dataValidator, FactoryBuilder factoryBuilder) {
        this.service = service;
        this.validatorFile = validatorFile;
        this.lineDataReader = lineDataReader;
        this.dataValidator = dataValidator;
        this.factoryBuilder = factoryBuilder;
    }


    public boolean readInfo(String path) {
        List<String> listData = null;
        LinkedHashMap<String, String> mapParse = new LinkedHashMap<String, String>();

        ValidatorResult validatorResult = validatorFile.validFile(path);

        if (!validatorResult.isValid()) {
            LOGGER.error(validatorResult.getError());
            return false;
        }

        listData = lineDataReader.dataReader(path);

        LineDataParser parser = new LineDataParser(mapParse);
        for (String list : listData) {
            validatorResult = parser.lineParse(list);

            if (validatorResult.isValid()) {
                validatorResult = dataValidator.dataValidate(mapParse);
            } else {
                LOGGER.error(validatorResult.getError());
                return false;
            }

            if (validatorResult.isValid()) {
                DataBuilder dataBuilder = factoryBuilder.getBuilder(TypeToy.valueOf(mapParse.get("TypeToy")));

                Toy toy = dataBuilder.getToy(mapParse);

                service.addToy(toy);
            } else {
                LOGGER.error(validatorResult.getError());
                return false;
            }
        }

        return true;
    }
}
