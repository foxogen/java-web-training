package by.training.util;

import java.util.Map;

public class DataValidatorUtil {

    public static boolean isIntNum(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("This value "+ num +" is not valid!");
        }
    }

    public static boolean isDoubleNum(String num) {
        try {
            Double.parseDouble(num);
            return true;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("This value "+ num +" is not valid!");
        }
    }

    public static boolean isStringCheck(String str) {
        if (!str.isEmpty()) return true;

        throw new IllegalArgumentException("This value "+ str +" is not valid!");
    }

    public static boolean checkMapisEmpty(Map<String, String> map){
        return map.isEmpty();
    }
}
