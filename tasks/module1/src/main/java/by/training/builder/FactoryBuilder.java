package by.training.builder;

import by.training.entity.TypeToy;
import org.apache.log4j.Logger;

public class FactoryBuilder {

    private static final Logger LOGGER = Logger.getLogger(FactoryBuilder.class);

    public DataBuilder getBuilder(TypeToy typeToy) {
        switch (typeToy) {
            case CAR:
                return new DataBuilderCar();
            case BALL:
                return new DataBuilderBall();
            case DOLL:
                return new DataBuilderDoll();
            case CUBE:
                return new DataBuilderCube();
            default:
                LOGGER.fatal("Fatal ERROR, It is not possible to create a toy object!");
                throw new  RuntimeException("It is not possible to create a toy object!");
        }
    }
}
