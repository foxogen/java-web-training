package by.training.builder;

import by.training.entity.Cube;
import by.training.entity.CubeMaterialType;
import by.training.entity.TypeToy;

import java.util.Map;

public class DataBuilderCube implements DataBuilder {
    @Override
    public Cube getToy(Map<String, String> map) {
        return new Cube(Integer.parseInt(map.get("id")),
                map.get("name"),
                Double.parseDouble(map.get("price")),
                Integer.parseInt(map.get("ageToy")),
                TypeToy.valueOf(map.get("TypeToy")),
                CubeMaterialType.valueOf(map.get("CubeMaterialType")));
    }
}
