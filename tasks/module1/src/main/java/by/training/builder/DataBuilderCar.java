package by.training.builder;

import by.training.entity.Car;
import by.training.entity.CarType;
import by.training.entity.TypeToy;

import java.util.Map;

public class DataBuilderCar implements DataBuilder {

    @Override
    public Car getToy(Map<String, String> map) {
        return new Car(Integer.parseInt(map.get("id")),
                map.get("name"),
                Double.parseDouble(map.get("price")),
                Integer.parseInt(map.get("ageToy")),
                TypeToy.valueOf(map.get("TypeToy")),
                CarType.valueOf(map.get("CarType")));
    }
}
