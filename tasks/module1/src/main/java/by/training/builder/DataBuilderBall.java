package by.training.builder;

import by.training.entity.Ball;
import by.training.entity.BallMaterialType;
import by.training.entity.TypeToy;

import java.util.Map;

public class DataBuilderBall implements DataBuilder{
    @Override
    public Ball getToy(Map<String, String> map) {
        return new Ball(Integer.parseInt(map.get("id")),
                map.get("name"),
                Double.parseDouble(map.get("price")),
                Integer.parseInt(map.get("ageToy")),
                TypeToy.valueOf(map.get("TypeToy")),
                BallMaterialType.valueOf(map.get("BallMaterialType")));
    }
}
