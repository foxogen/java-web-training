package by.training.builder;

import by.training.entity.Doll;
import by.training.entity.TypeToy;

import java.util.Map;

public class DataBuilderDoll implements DataBuilder {
    @Override
    public Doll getToy(Map<String, String> map) {
        return new Doll(Integer.parseInt(map.get("id")),
                map.get("name"),
                Double.parseDouble(map.get("price")),
                Integer.parseInt(map.get("ageToy")),
                TypeToy.valueOf(map.get("TypeToy")),
                Double.parseDouble(map.get("dollWeight")),
                map.get("brand"));
    }
}
