package by.training.builder;

import by.training.entity.Toy;

import java.util.Map;

public interface DataBuilder {
    Toy getToy(Map<String, String> map);
}
