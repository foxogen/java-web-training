package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum CubeMaterialType {

    WOOD, PLASTIC, IRON, PAPER, RUBBER;

    public static Optional<CubeMaterialType> fromString(String type) {

        return Stream.of(CubeMaterialType.values())
                .filter(t -> t.name().equalsIgnoreCase(type))
                .findFirst();
    }
}
