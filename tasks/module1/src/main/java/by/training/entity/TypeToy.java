package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum TypeToy {
    CAR, DOLL, BALL, CUBE;

    public static Optional<TypeToy> fromString(String type) {

        return Stream.of(TypeToy.values())
                .filter(t -> t.name().equalsIgnoreCase(type))
                .findFirst();
    }
}
