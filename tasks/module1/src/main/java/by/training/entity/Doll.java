package by.training.entity;

import java.util.Objects;

public class Doll extends Toy {

    private double dollWeight;
    private String brand;

    public Doll(int id, String name, double price, int ageToy, TypeToy typeToy) {
        super(id, name, price, ageToy, typeToy);
    }

    public Doll(int id, String name, double price, int ageToy, TypeToy typeToy, double dollWeight, String brand) {
        super(id, name, price, ageToy, typeToy);
        this.dollWeight = dollWeight;
        this.brand = brand;
    }

    public double getDollWeight() {
        return dollWeight;
    }

    public void setDollWeight(double dollWeight) {
        this.dollWeight = dollWeight;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Doll doll = (Doll) o;
        return Double.compare(doll.dollWeight, dollWeight) == 0 &&
                Objects.equals(brand, doll.brand);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dollWeight, brand);
    }

    @Override
    public String toString() {
        return "Doll{" +
                "dollWeight=" + dollWeight +
                ", brand='" + brand + '\'' +
                '}';
    }
}
