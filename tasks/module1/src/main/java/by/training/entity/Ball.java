package by.training.entity;

import java.util.Objects;

public class Ball extends Toy {

    private BallMaterialType ballMaterial;

    public Ball(int id, String name, double price, int ageToy, TypeToy typeToy) {
        super(id, name, price, ageToy, typeToy);
    }

    public Ball(int id, String name, double price, int ageToy, TypeToy typeToy, BallMaterialType ballMaterial) {
        super(id, name, price, ageToy, typeToy);
        this.ballMaterial = ballMaterial;
    }

    public BallMaterialType getBallMaterial() {
        return ballMaterial;
    }

    public void setBallMaterial(BallMaterialType ballMaterial) {
        this.ballMaterial = ballMaterial;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Ball ball = (Ball) o;
        return ballMaterial == ball.ballMaterial;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ballMaterial);
    }

    @Override
    public String toString() {
        return "Ball{" +
                "ballMaterial=" + ballMaterial +
                '}';
    }
}
