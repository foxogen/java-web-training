package by.training.entity;

import java.util.Objects;

public class Car extends Toy {

    private CarType carType;

    public Car(int id, String name, double price, int ageToy, TypeToy typeToy) {
        super(id, name, price, ageToy, typeToy);
    }

    public Car(int id, String name, double price, int ageToy, TypeToy typeToy, CarType carType) {
        super(id, name, price, ageToy, typeToy);
        this.carType = carType;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Car car = (Car) o;
        return carType == car.carType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), carType);
    }

    @Override
    public String toString() {
        return "Car{" +
                "carType=" + carType +
                '}';
    }
}
