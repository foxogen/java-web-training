package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum BallMaterialType {

    LEATHER, RESIDOR, POLYESTER;

    public static Optional<BallMaterialType> fromString(String type) {

        return Stream.of(BallMaterialType.values())
                .filter(t -> t.name().equalsIgnoreCase(type))
                .findFirst();
    }
}
