package by.training.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum CarType {

    SMALL_CAR, MEDIUM_CAR, LARGE_CAR;

    public static Optional<CarType> fromString(String name) {

        return Stream.of(CarType.values())
                .filter(t -> t.name().equalsIgnoreCase(name))
                .findFirst();
    }
}
