package by.training.entity;

import java.util.Objects;

public class Cube extends Toy {

    private CubeMaterialType type;

    public Cube(int id, String name, double price, int ageToy, TypeToy typeToy) {
        super(id, name, price, ageToy, typeToy);
    }

    public Cube(int id, String name, double price, int ageToy, TypeToy typeToy, CubeMaterialType type) {
        super(id, name, price, ageToy, typeToy);
        this.type = type;
    }

    public CubeMaterialType getType() {
        return type;
    }

    public void setType(CubeMaterialType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Cube cube = (Cube) o;
        return type == cube.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type);
    }

    @Override
    public String toString() {
        return "Cube{" +
                "type=" + type +
                '}';
    }
}
