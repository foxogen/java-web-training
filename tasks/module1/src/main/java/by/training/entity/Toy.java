package by.training.entity;

import java.util.Objects;

public abstract class Toy {
    private int id;
    private String name;
    private double price;
    private int ageToy;
    private TypeToy typeToy;

    public Toy(int id, String name, double price, int ageToy, TypeToy typeToy) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.ageToy = ageToy;
        this.typeToy = typeToy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAgeToy() {
        return ageToy;
    }

    public void setAgeToy(int ageToy) {
        this.ageToy = ageToy;
    }

    public TypeToy getTypeToy() {
        return typeToy;
    }

    public void setTypeToy(TypeToy typeToy) {
        this.typeToy = typeToy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Toy toy = (Toy) o;
        return id == toy.id &&
                price == toy.price &&
                ageToy == toy.ageToy &&
                Objects.equals(name, toy.name) &&
                typeToy == toy.typeToy;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, ageToy, typeToy);
    }

}
