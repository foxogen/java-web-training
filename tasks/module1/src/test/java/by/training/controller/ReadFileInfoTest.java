package by.training.controller;

import by.training.builder.FactoryBuilder;
import by.training.validator.DataValidator;
import by.training.validator.LineDataReader;
import by.training.validator.ValidatorFile;
import by.training.repository.Repository;
import by.training.repository.ToyRepository;
import by.training.service.ToyServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

@RunWith(JUnit4.class)
public class ReadFileInfoTest {

    private Repository repository = new ToyRepository();
    private ToyServiceImpl service;
    private ValidatorFile validatorFile;
    private LineDataReader lineDataReader;
    private DataValidator dataValidator;
    private FactoryBuilder factoryBuilder;
    private ToyController readFileInfo;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void loadingInfo() {
        service = new ToyServiceImpl(repository);
        validatorFile = new ValidatorFile();
        lineDataReader = new LineDataReader();
        dataValidator = new DataValidator();
        factoryBuilder = new FactoryBuilder();
        readFileInfo = new ToyController(service, validatorFile, lineDataReader, dataValidator, factoryBuilder);
    }

    @Test
    public void readFirstValidFile() {
        File file = new File(classLoader.getResource("ValidTest.txt").getFile());
        readFileInfo.readInfo(file.getAbsolutePath());

        Assert.assertTrue(true);
    }


    @Test
    public void readSecondValidFile() {
        File file = new File(classLoader.getResource("SecondValidFile.txt").getFile());
        readFileInfo.readInfo(file.getAbsolutePath());

        Assert.assertTrue(true);
    }

    @Test
    public void readEmptyFile() {
        File file = new File(classLoader.getResource("EmptyFile.txt").getFile());
        readFileInfo.readInfo(file.getAbsolutePath());

        Assert.assertFalse(false);
    }

    @Test
    public void readFirstNotValidFile() {
        File file = new File(classLoader.getResource("FirstNotValidFile.txt").getFile());
        readFileInfo.readInfo(file.getAbsolutePath());

        Assert.assertFalse(false);
    }

    @Test
    public void readSecondNotValidFile() {
        File file = new File(classLoader.getResource("SecondNotValidFile.txt").getFile());
        readFileInfo.readInfo(file.getAbsolutePath());
        Assert.assertFalse(false);
    }
}