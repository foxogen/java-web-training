package by.training.repository;

import by.training.controller.ToyController;
import by.training.builder.FactoryBuilder;
import by.training.validator.DataValidator;
import by.training.validator.LineDataReader;
import by.training.validator.ValidatorFile;
import by.training.entity.Car;
import by.training.entity.CarType;
import by.training.entity.Toy;
import by.training.entity.TypeToy;
import by.training.service.ToyServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;

@RunWith(JUnit4.class)
public class ToyRepositoryTest {

    private Toy TOY_FOR_DELETE = new Car(1, "Jeep", 50.0, 2, TypeToy.CAR, CarType.MEDIUM_CAR);
    private Repository repository = new ToyRepository();
    private ToyServiceImpl service;
    private ValidatorFile validatorFile;
    private LineDataReader lineDataReader;
    private DataValidator dataValidator;
    private FactoryBuilder factoryBuilder;
    private ToyController readFileInfo;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void loadingInfo(){
        service = new ToyServiceImpl(repository);
        validatorFile = new ValidatorFile();
        lineDataReader = new LineDataReader();
        dataValidator = new DataValidator();
        factoryBuilder = new FactoryBuilder();
        readFileInfo = new ToyController(service, validatorFile, lineDataReader, dataValidator, factoryBuilder);

        File file = new File(classLoader.getResource("ValidTest.txt").getFile());
        readFileInfo.readInfo(file.getAbsolutePath());
    }

    @Test
    public void findById() {
        service = new ToyServiceImpl(repository);

        Specification<Toy> specification = new FindToyByIDSpecification(1);

        List<Toy> toy = repository.find(specification);

        Assert.assertEquals(toy.get(0).getName(),"Jeep");
    }

    @Test
    public void findByInterval() {
        service = new ToyServiceImpl(repository);

        Specification<Toy> specification = new FindToyByIntervalSpecification(1,4);

        List<Toy> toyList = repository.find(specification);

        Assert.assertEquals(toyList.get(0).getName(), "Football");
    }

    @Test
    public void findByName() {
        service = new ToyServiceImpl(repository);

        Specification<Toy> specification = new FindToyByNameSpecification("Barbi");

        List<Toy> toyList = repository.find(specification);

        Assert.assertEquals(toyList.get(0).getName(), "Barbi");
    }

    @Test
    public void delete() {
        service = new ToyServiceImpl(repository);

        List<Toy> toyList = repository.getAllToy();

        Assert.assertEquals(toyList.size(), 7);

        repository.delete(TOY_FOR_DELETE);

        Assert.assertEquals(toyList.size(), 6);
    }
}