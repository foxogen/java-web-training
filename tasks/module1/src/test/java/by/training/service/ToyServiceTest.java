package by.training.service;

import by.training.controller.ToyController;
import by.training.builder.FactoryBuilder;
import by.training.validator.DataValidator;
import by.training.validator.LineDataReader;
import by.training.validator.ValidatorFile;
import by.training.entity.*;
import by.training.repository.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.util.List;

@RunWith(JUnit4.class)
public class ToyServiceTest {

    private Toy TOY_JEEP = new Car(1, "Jeep", 50.0, 2, TypeToy.CAR, CarType.MEDIUM_CAR);
    private Toy TOY_BARBI_ONE = new Doll(6, "Barbi", 27.4, 2, TypeToy.DOLL, 0.9, "DG");
    private Toy TOY_BARBI_TWO = new Doll(3, "Barbi", 45.7, 1, TypeToy.DOLL, 1.2, "gucci");
    private Repository repository = new ToyRepository();
    private ToyServiceImpl service;
    private ValidatorFile validatorFile;
    private LineDataReader lineDataReader;
    private DataValidator dataValidator;
    private FactoryBuilder factoryBuilder;
    private ToyController readFileInfo;
    private ClassLoader classLoader = getClass().getClassLoader();

    @Before
    public void loadInfo(){
        service = new ToyServiceImpl(repository);
        validatorFile = new ValidatorFile();
        lineDataReader = new LineDataReader();
        dataValidator = new DataValidator();
        factoryBuilder = new FactoryBuilder();
        readFileInfo = new ToyController(service, validatorFile, lineDataReader, dataValidator, factoryBuilder);

        File file = new File(classLoader.getResource("ValidTest.txt").getFile());
        readFileInfo.readInfo(file.getAbsolutePath());
    }

    @Test
    public void sortToyByName() {
        service = new ToyServiceImpl(repository);

        List<Toy> toyList = service.sortToyByName();

        Assert.assertEquals(toyList.get(3), TOY_JEEP);
    }

    @Test
    public void sortToyByNameAndPrice() {
        service = new ToyServiceImpl(repository);

        List<Toy> toyList = service.sortToyByNameAndPrice();

        Assert.assertEquals(toyList.get(0), TOY_BARBI_ONE);
        Assert.assertEquals(toyList.get(1), TOY_BARBI_TWO);
    }

    @Test
    public void findToyByName() {
        service = new ToyServiceImpl(repository);

        Specification<Toy> spec = new FindToyByNameSpecification("Jeep");
        List<Toy> toyList = service.findToyByName(spec);

        Assert.assertEquals(toyList.get(0), TOY_JEEP);
    }

    @Test
    public void findToyByNameAndPrice() {
        service= new ToyServiceImpl(repository);

        Specification<Toy> spec1 = new FindToyByNameSpecification("Barbi");
        Specification<Toy> spec2 = new FindToyByPriceSpecification(27.4);
        List<Toy> toyList = service.findToyByNameAndPrice(spec1.and(spec2));

        Assert.assertEquals(toyList.get(0), TOY_BARBI_ONE);
    }
}